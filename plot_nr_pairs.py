from analogSimulatorBenchmarkingFunctions import *
import structureFunctions


filename = 'numericalData/17Simulation/circuitBandDiagHframePotentials.txt'
structureFunctions.plotDataFromtxtFile(filename,
									   'N',
									   'length',
									   showPlot = False,
									   yscale = 'log',
									   gkdesignIndex = 3,
									   boxplot = True,
									   noNewFigure = True,
									   addToLabel = ', 4 design, one set',
									   plotColor = 120,
									   averageColor = 40,
									   gxLabel = 'nr of pairs',
									   gxArray = [10**2, 100**2, 1000**2, 10000 ** 2] )





filename = 'numericalData/16Simulation/circuitBandDiagHframePotentials.txt'

structureFunctions.plotDataFromtxtFile(filename,
									   'N',
									   'length',
									   showPlot = False,
									   yscale = 'log',
									   gkdesignIndex = 3,
									   boxplot = True,
									   noNewFigure = True,
									   addToLabel = ', 4 design, two sets',
									   plotColor = 120,
									   averageColor = 40,
									   gxLabel = 'nr of pairs',
									   gxArray = [5**2, 50**2, 500**2, 5000 ** 2])


filename = 'numericalData/15Simulation/circuitBandDiagHframePotentials.txt'
plt.tight_layout()
structureFunctions.plotDataFromtxtFile(filename,
									   'N',
									   'length',
									   showPlot = True,
									   yscale = 'log',
									   gkdesignIndex = 3,
									   boxplot = True,
									   noNewFigure = True,
									   addToLabel = ', 4 design, pairs',
									   plotColor = 120,
									   averageColor = 40,
									   gxLabel = 'nr of pairs',
									   gxArray = [5, 50, 500, 5000])







