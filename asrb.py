import numpy as np
import structure_functions
import datetime
# import matplotlib.pyplot as plt
import pdb
from scipy.special import binom
from itertools import permutations
from copy import deepcopy


###############################################################################
############################## Spectral Gap ###################################
###############################################################################


def spectral_norm_hist(gmatrices):
    """ This function takes an array of matrices and computes the spectral norm
    of every single matrix. This values are than displayed in an histogram.
    Input:
        np.ndarray:gmatrices, the array of matrices for which the spectral
                              norm is calculated and displayed.
    """
    # Calculate the spectral norm of every matrix, ord = 2 means the spectral
    # norm, axis = (1,2) denotes the single matrices.
    norm_array = np.linalg.norm(gmatrices, axis = (1,2), ord = 2)
    # Make a histogram out of it.
    plt.hist(norm_array)
    # The x axis displays the spectral norm.
    plt.xlabel('spectral norm')
    # The y axis how many of this value is given in the array.
    plt.ylabel('amount')
    plt.show()


def calculate_spectral_gap(gunitaries, k, *args, **kwargs):
    """ This function takes a set of unitaries and calculates the spectral gap
    according to:
    M*S*N^dag=SVD[1/N*sum_j^N(U_j^(tensor k) tensor (U_j^dag)^(tensor k))],
    where N is the amount of unitaries in the given set,
    U_j with j in {1, ..., N} and SVD[] denotes a
    Singular Value Decomposition. In this case S contains the sorted singular
    values on the diagonal, starting with the biggest value on the left.
    Input:
        np.ndarray:gunitaries, the set of unitaries which are summed over.
    Output:
        np.ndarray, the singular values stored in a 1D array. 
    """
    # Extract the shape of np.ndarray:gunitaries.
    shape_tuple = gunitaries.shape
    # Initialize the matrix used for summing over all computed matrices.
    summed_matrix = np.zeros((shape_tuple[1]**(2*k), shape_tuple[1]**(2*k)))
    # Iterate over every single unitary in the set.
    for counter in range(shape_tuple[0]):
        # Extract the wanted unitary from the set.
        unitary = gunitaries[counter]
        # Compute its conjugated version.
        unitary_conj = unitary.conj()
        # Compute from left to right the equation from above.
        conj_tensorproduct_right = unitary_conj
        tensorproduct_left = unitary
        for kk in range(k-1):
            # Calculate the right hand side of the formula given above.
            conj_tensorproduct_right = np.kron(unitary_conj,
                                               conj_tensorproduct_right)
            # Calculate the left hand side.
            tensorproduct_left = np.kron(unitary, tensorproduct_left)
        # Take the tensor product of left and right hand side.
        tensorproduct_matrix = np.kron(tensorproduct_left,
                                       conj_tensorproduct_right)
        # Sum everything up.
        summed_matrix = summed_matrix + tensorproduct_matrix
    # Divide the sum through the amount of matrices in the set.
    summed_matrix = summed_matrix/shape_tuple[0]
    # Carry out the singular value decomposition.
    _, singValues, _ = np.linalg.svd(summed_matrix)
    # Return only the singular values.
    return singValues

###############################################################################
############################### Check For ####################################
###############################################################################

def check_hermitian(gmatrices):
    """ This function computes the daggered version of the input and calculates
    the distance of each entry from the not daggered version. If all entries
    are near to zero, the input matrices are hermitian.
    Input:
        np.ndarray:gmatrices, with shape (int:N, int:dim, int:dim)
    Output:
        np.ndarray:distanceMatrix,   with shape (int:N, int:dim, int:dim)
    """
    multiple_matrices_dag = np.transpose(gmatrices, (0, 2, 1)).conj()
    distance_matrix = multiple_matrices_dag - gmatrices
    return distance_matrix

###############################################################################
####################### Distributions to draw from ############################
###############################################################################

def normal_distribution(amount, dim, *args, loc=0.0, scale=1.0, **kwargs):
    """ Returns an array of size (forFactor * N * (dim - dimMinus)) with entries
    drawn from a normal distribution.
    Input:
        int:amount,
        int:dim
    Output:
        np.ndarray:entries, every entry is drawn from the normal distribution,
            there are int:number_entries many entries in the array.
    """
    # Draw the entries.
    entries = np.random.normal(size=amount*dim, loc=loc, scale=scale)
    return entries


def uniform_distribution(amount, dim, *args, low=0.0, high=1.0, **kwargs):
    """ Returns an array of size (forFactor * N * (dim - dimMinus)) with entries
    drawn from a normal distribution.
    Input:
        int:amount,
        int:dim
    Output:
         np.ndarray:entries, every entry is drawn from the uniform distribution,
            there are int:number_entries many entries in the array. 
    """
    # Draw the entries from the uniform distribution.
    entries = np.random.uniform(size=amount*dim, low=low, high=high)
    return entries


def repeated_distribution(amount, dim, *args,
                     distribution=normal_distribution, **kwargs):
    """ This function returns an array repeated int:N times. 
    Input:
        int:amount,
        int:dim,
    Output:
        np.ndarray
    """
    # Draw the entries which are supposed to be the real and imaginary entries.
    a1 = distribution(1, dim, *args, **kwargs)
    a2 = distribution(1, dim, *args, **kwargs)
    # When building the hamiltonian, the the supposed to be imaginary part
    # is taken times 1j, meaning that if it is already -1j it will be real again.
    if kwargs.get('not_imaginary'):
        a2 = a2*(-1j)
    # Repeat the entries, such that they match the drawn in.
    # Since there are two arrays draw, they have to only be repeated by half
    # of the int:amount.
    repeated1, repeated2 = np.tile(a1, int(amount/2)), np.tile(a2, int(amount/2))
    # Append them!
    repeated_values = np.append(repeated1, repeated2)
    # Return the flat array.
    return repeated_values


def only_number(amount, dim, *args,
        fixed_value=1., loc=0.0, scale=1.0, low=0.0, high=1.0, **kwargs):
    """ This function returns an array filled with int:amount*int*dim values
    which yields int:fixed_value in the end after being used in the function
    func:unitaries_from_hamiltonians.
    Input:
        int:amount,
        int:dim
    Output:
        np.ndarray an array with dimension int:amount*int:dim filled
            with float:fixed_value
    """ 
    # Check if the one repeated entry should be gauss random.
    if kwargs.get('random_entry_normal'):
        # Draw the normal entries.
        fixed_value = np.random.normal(loc=loc, scale=scale)
    # Check if the one repeated entry should be uniform random.
    elif kwargs.get('random_entry_uniform'):
        fixed_value = np.random.uniform(high=high, low=low)
    return np.ones(amount*dim)*fixed_value

###############################################################################
######################## Patterns for Hamiltonians #############################
###############################################################################

def draw_diag(amount, dim, *args, take_every=None,
              diag_func=normal_distribution, **kwargs):
    """ Takes the function which has to be used to draw the entries,
    amount of matrices and dimension of the single matrices.
    Returns the entries and the corresponding indices of a flatten matrix.
    Input:
        int:numberEntries, the amount of entries.
        func:diag_func, the given function with which
            the entries are drawn from.
    """
    # Generate indices for diagonal entries.
    diag_indices = np.array([np.arange(x*dim**2, (x+1)*dim**2, dim+1) \
        for x in range(0, amount)])
    # Check if only a part of the entries is needed.
    if take_every != None:
        # Take only the given entries.
        diag_indices = diag_indices[:,::take_every].flatten()
        # Draw as many entries as needed * 2 from the given distribution.
        # The other half is needed for the imaginary part.
        diag_values = diag_func(2*amount, dim//take_every,
                                          *args, **kwargs)
        half = len(diag_indices)
    else:
        # Draw all the diagonal int:amount*int:dim non zero entries from
        # the given distribution and additionally int:amount*int:dim entries
        # which will be the imaginary part
        diag_values = diag_func(2*amount, dim, *args, **kwargs)
        half = amount*dim
        diag_indices = diag_indices.flatten()
    # Make them imaginary.
    diag_values = diag_values[:half] + 1j * diag_values[half:]
    return diag_indices, diag_values


def draw_band(amount, dim, bandweight, *args,
              band_func=normal_distribution, which_band=1,
              leaveout_every=None, **kwargs):
    """ Takes the function which has to be used to draw the entries,
    amount of matrices and dimension of the single matrices.
    Returns the band indices and entries.
    Input:
        int:amount, the amount of matrices
            (when reshaped into (int:amount,int:dim,int:dim)).
        int:dim, the dimension of the single matrices
            (when reshaped into (int:amount,int:dim,int:dim)).
        flot:bandweight, the number with which the band entries are multiplied.
        func:band_func, the given function with which
            the entries are drawn from.
        int:which_band, describes the amount of entries needed, the first band
            needs more then the third band.
        int:leaveout_every, for lattices, some entries have to still be zero.
    Output:
        np.ndarray:sideBandIndices,
        np.ndarray:sideBandEntries
    
    """
    # Generate the non zero entry indices of side bands, which_band denotes
    # the distance between the diagonal entries and the band entries.
    upperband_indices = [np.arange(
        x*dim**2+which_band, (x+1)*dim**2 - which_band*dim, dim + 1) \
        for x in range(0, amount)]
    lowerband_indices = [np.arange(x*dim**2 + which_band*dim, (x + 1)*dim**2,
        dim + 1) for x in range(0, amount)]
    # Possibly, there are entries which have to be left out, for example for the
    # 2D hopping Hamiltonian term.
    if leaveout_every != (None and 0):
        # Generate the entries which should not be taken out. 
        indices = [x-1 for x in range(0,dim) if x%leaveout_every!=0]
        # This indices leave every np.sqrt(dim)th value out.
        upperband_indices = np.array(upperband_indices)[:,indices]
        lowerband_indices = np.array(lowerband_indices)[:,indices]
        # Draw the band entries, take into account for which band the entries 
        # are drawn and if entries are left out. Multiply them by the factor
        # float:bandweight. 
        # Half of that drawn numbers will be used for the imaginary part.
        band_entries = bandweight*band_func(
            4*amount, (dim-which_band-int((dim-which_band)/leaveout_every)),
            *args, **kwargs)
        # Calculate where the first half ends and the second half starts.
        half = int(len(band_entries)/2)
    else:
        # Draw the band entries, in total 4*int:amount*(int:dim-which_band), from
        # given distribution and multiply them by the factor float:bandweight.
        # Half of that drawn numbers will be used for the imaginary part
        band_entries = bandweight*band_func(
            4*amount, (dim-which_band), *args, **kwargs)
        # Calculate where the first half ends and the second half starts.
        half = 2*amount*(dim-which_band)
    # Put them together since the entries of the side bands are drawn from
    # the same distribution.
    sideband_indices = np.append(upperband_indices, lowerband_indices)
    # Make the entries imaginary.
    band_entries = band_entries[:half] + 1j * band_entries[half:]
    return sideband_indices, band_entries


def interaction_1d(amount, dim, *args, pattern="same_indices", **kwargs):
    """ Draws the entries and indices according to the given description.
    Input:
        int:amount,
        int:dim,
        str:pattern
    Output:
        np.ndarray:
        np.ndarray:
    """
    # Check what pattern is required and draw the indices accordingly.
    if pattern == 'same_indices':
        # Pretend like the entries are drawn for the hopping term.
        # Then take every int:dimth entry, meaning only take
        # V_{11,11}, V_{22,22}, ...
        indices, values = draw_diag(amount, dim**2, *args, take_every=dim,
                                    diag_func=kwargs['diag_func_int'], **kwargs)
    return indices, values


def interaction_2d(amount, dim, *args, pattern='same_indices', **kwargs):
    """ Draws the entries and indices according to the given description.
    Input:
        int:amount,
        int:dim,
        str:pattern
    Output:
        np.ndarray:
        np.ndarray:
    """
    # Check what pattern is required and draw the indices accordingly.
    if pattern == 'same_indices':
        # Pretend like the entries are drawn for the hopping term.
        # Then take every int:dimth entry, meaning only take
        # V_{11,11}, V_{22,22}, ...
        indices, values = draw_diag(amount, dim**2, *args, take_every=dim,
                                    diag_func=kwargs['diag_func_int'], **kwargs)
    return indices, values

###############################################################################
####################### Functions for Hamiltonians ############################
###############################################################################

def make_hermitian(gmatrices, *args, **kwargs):
    """
    Takes a not necessary hermitian matrix (np.ndarray) with shape (N, dim, dim), 
    creates the conjugate transpose of the singel matrices in the array, adds it 
    to the given array of matrices and divides it by two.
    Input:
        np.ndarray:gmatrices, array of matrices with shape (N, dim, dim)
                                 which will be made hermitian
    Output:
        np.ndarray: hermitian matrices inside the array, same shape as input.
    """
    # Transpose the matrices inside the array.
    gmatrices_T = np.transpose(gmatrices, (0,2,1))
    # Add the given matrices with with conjugate transpose.
    hermitian_matrices = (gmatrices + gmatrices_T.conj())/2.
    return hermitian_matrices


def extract_repeated_indices(unravel_indices, *args, **kwargs):
    """ Gets the entries of indices of the first matrix.
    First sorts, then extracts.
    Input:
        np.ndarray:unravel_indices, the multi layer array with array of
            indices.
    Output:
        np.ndarray:first_layer_sorted_ind, the indices of the sorted first layer.
        np.ndarray:first_matrix_indices, the entries of the first matrix.
        
    """
    # Since each of the int:amount matrix should be drawn equally, the second
    # and third layer of the np.ndarray:unraveled_hopping_indices should be
    # the same for each matrix.
    first_layer_sorted_ind = np.argsort(unravel_indices[0])
    # Sort the indices and values of the hopping.
    sorted_entries = unravel_indices[:,first_layer_sorted_ind]
    # Get the index where the indexing for the second matrix starts.
    first_until = np.argwhere(sorted_entries[0]>0)[0,0]
    # Get the indices of the hopping of the first (and then of all) matrices.
    first_matrix_indices = sorted_entries[:,:first_until][1:]
    return first_layer_sorted_ind, first_matrix_indices


def flip_operator(gvectors, indices_plus, indices_minus, 
                  *args, tiled=False, **kwargs):
    """ Takes an np.ndarray:gvectors filled with vectors and the instructions
    how to manipulate the given vectors. Meaning that in the
    np.ndarray:indices_plus array each entry stands for the index which is
    indexing the entry of every single vector in gvectors which is taken + 1.
    For the np.ndarray:indices_minus everything is the same except the entries
    are taken - 1.
    In the end, there are as many copies of the gvectors array as there are
    indices in indices_plus (or indices_minus) and in every copy another entry
    is altered according to the entries in indices_plus and indices_minus.
    Input:
        np.ndarray:gvectors, an array filled with arrays which are altered.
        np.ndarray:indices_plus, an array containing as entries the indices
            which are increased.
        np.ndarray:indices_minus, an array containing as entries the indices
            which are decreased.
        bool:tiled, if this is true, the gvectors already has the right
            shape and does not have to be tiled, again.
    Output:
        np.ndarray:vectors, an array of shape (len(indices_plus),gvectors.shape)
    """
    if tiled:
        # Make a deep copy as to not alter the given array.
        vectors = deepcopy(gvectors)
        dim_basis = len(vectors[0])
    else:
        # Make int:len(indices_plus) many copies of np.ndarray:gvectors such that
        # they are tiled up individually with shape
        # (len(indices_plus),gvectors.shape).
        vectors = np.tile(gvectors, (len(indices_plus),1,1))
        dim_basis = len(gvectors)

    # To alter the entries of this new array np.ndarray:vectors, the right
    # indices have to be chosen.
    # First, the very outer indices of which copy is accessed.
    indices_outer_vectors = np.arange(len(indices_plus))
    # Second, the second layer, which vector in the copy.
    indices_inner_vectors = np.arange(dim_basis)
    # For matching the outer and inner indices, the outer have to be  repeated, meaning that
    # every number is repeated individually, for example [1,2] -> [1,1,2,2].
    repeated_outer = np.repeat(indices_outer_vectors, dim_basis)
    # And the inner indices have to be tiled,
    # meaning that the sequence is repeated itself, for example [1,2]->[1,2,1,2].
    tiled_inner = np.tile(indices_inner_vectors, len(indices_plus))
    # Also, the swapping indices have to be repeated accordingly.
    repeated_swapping_plus = np.repeat(indices_plus, dim_basis)
    repeated_swapping_minus = np.repeat(indices_minus, dim_basis)
    # For indexing the 3 dimensional np.ndarray:vectors, the arrays have to be converted to
    # lists and appended to each other. From outer to inner indices to what to take minus or plus.
    take_plus, take_minus = [list(repeated_outer)], [list(repeated_outer)]
    take_plus.append(list(tiled_inner)), take_minus.append(list(tiled_inner))
    take_plus.append(list(repeated_swapping_plus))
    take_minus.append(list(repeated_swapping_minus))
    # Now the actual work, the take_minus entries index the values which are decreased.
    vectors[tuple(take_minus)] -= 1
    # Now all vector entries which are now negative, meaning -1, are set to -2, 
    # with this it is ensured that this vectors are not contributing any more 
    # in the overlap calculation.
    vectors[vectors<0] = -2
    # And the take_plus entries index the values which are increased.
    vectors[tuple(take_plus)] += 1
    return vectors


def generate_fock_vectors(number_particles, number_nodes, *args, **kwargs):
    """ This function generates the Fock Basis for int:number_particles many
    particles in int:number_nodes many nodes in the lattice or chain.
    For example a 3 node chain with 2 particles yields:
    [[2, 0, 0], [1, 1, 0], [1, 0, 1], [0, 2, 0], [0, 1, 1], [0, 0, 2]]
    Input:
        int:number_particles
        int:number_nodes
    Output:
        np.ndarray:stats, array filled with fock basis arrays.
    """
    # Initialize the amount of arrays needed and fill with zeros. 
    states = np.zeros((int(binom(number_particles+number_nodes-1,
        number_particles)), number_nodes), dtype=int)
    # First entry in first array is the amount of particles.
    states[0, 0]=number_particles
    # Initialize for loop variable.
    count = 0
    # Loop over all arrays in states.
    for i in range(1, states.shape[0]):
        states[i,:number_nodes-1] = states[i-1, :number_nodes-1]
        states[i,count] -= 1
        states[i,count+1] += 1+states[i-1, number_nodes-1]
        if count >= number_nodes-2:
            if np.any(states[i, :number_nodes-1]):
                count = np.nonzero(states[i, :number_nodes-1])[0][-1]
        else:
            count += 1
    return states

###############################################################################
############################## Hamiltonians ###################################
###############################################################################

def draw_H_noninteracting(amount, dim, *args, dimension='1D', bandweight=1., **kwargs):
    # TODO build in if it is only one particle, the fock space does not have to be used!
    """Puts together an array filled with  matrices, with the 
    shape (int:amount,int:dim,int:dim). Since there can be more than one particle,
    the basis has to be changed to the Fock basis. Like in the draw_H_interacting function.
    Each matrix is drawn from a given distribution.
    1D:
        The diagonal and off diagonal entries next to the diagonal are non-zeros,
        other entries stay zero.
    2D:
        Non-zero entries:
            - diagonal,
            - bands of diagonal where index%sqrt(dim)!= 0,
            - sqrt(dim)th band.
    Input:
        int:amount, the number of matrices in the array.
        int:dim, the dimension of the single matrices in the array.
        float:bandweight, multiply the band entries with this value.
    Output:
        np.ndarray:hamiltonians, the array of hermitian matrices
            with the shape (int:amount,int:dim,int:dim).
    """
    # Extract how many particles there are in the system. Important for the Fock basis.
    # If nothing is given, take number_particles = 1.
    number_particles = kwargs.get('number_particles', 1)
    # Check if there is only 1 particle. Then the Fock space transision is not needed.
    if number_particles == 1:
        hamiltonian_fock = draw_H_noninteracting_OLD(
            amount, dim, *args, dimension=dimension, bandweight=bandweight, **kwargs)
    # But if there is more then 1 particle, the change of basis has to happen to
    # the Fock basis.
    else:
        # Generate the fock basis vectors. The basis of the hopping
        # term will be changed from spatial to the Fock basis.
        fock_basis = generate_fock_vectors(number_particles, dim)
        fock_dim = len(fock_basis)
        if dimension == '1D':
            # Get the diagonal indices and entries.
            diag_indices, diag_values = draw_diag(
                amount, dim, *args, diag_func=kwargs['diag_func_hop'], **kwargs)
            # Get the band entries.
            band_indices, band_values = draw_band(
                amount, dim, bandweight, *args, which_band=1,
                band_func=kwargs['band_func_hop'], **kwargs)
            # Put together the indices and values.
            hopping_indices = np.append(diag_indices, band_indices)
            hopping_values = np.append(diag_values, band_values)
        elif dimension == '2D':
            # Get the diagonal indices and entries.
            diag_indices, diag_values = draw_diag(
                amount, dim, *args, diag_func=kwargs['diag_func_hop'], **kwargs)
            # Calculate the length of one side of the lattice.
            sqrtdim = int(np.sqrt(dim))
            # Get the band indices and entries next to the diagonal.
            band_indices, band_values = draw_band(
                amount, dim, bandweight, *args, which_band=1, leaveout_every=sqrtdim,
                band_func=kwargs['band_func_hop'], **kwargs)
            # Get the second band indices and entries.
            second_band_indices, second_band_values = draw_band(
                amount, dim, bandweight, *args, which_band=sqrtdim, 
                band_func=kwargs['band_func_hop'], **kwargs)
            # Put together the indices and values.
            hopping_indices = np.append(diag_indices, [band_indices, second_band_indices])
            hopping_values = np.append(diag_values, [band_values, second_band_values])
        # Now the indices and values are set. The change of basis can be performed.
        # The indices array are indexing the flattened hamiltonian matrices.  But
        # now the indices for the reshaped matrices are needed to denote the
        # swapping (or flipping).
        unraveled_indices = np.array(np.unravel_index(hopping_indices, (amount, dim, dim)))
        # To avoid redundancy, the indices of the first matrix are extracted. Since the 
        # Hamiltonians indices are all drawn the same, take the first set.
        sorted_indices, hopping_entries = extract_repeated_indices(unraveled_indices, *args, **kwargs)
        # Sort the values, too.
        sorted_hopping_values = hopping_values[sorted_indices]
        # Calculate the swapped (according to the hopping indices) fock vectors.
        # Meaning that here the action of the Hamiltonian on the fock basis is calculated, 
        # since in the end only the overlaps are important.
        flipped_hop_vectors = flip_operator(fock_basis,
                                            hopping_entries[0],
                                            hopping_entries[1], *args, **kwargs)
        # Initialize the list which, when filled, are the Hamiltonians in the fock
        # basis.
        hamiltonian_fock = []
        # For each matrix in int:amount matrices given, calculate the Hamiltonian
        # in fock basis.
        for matrix in range(amount):
            # Go through all vectors in the fock_basis with a for loop. Calculate
            # the overlap of a vector from the fock basis and a vector flipped 
            # through the hopping matrix.
            for row in range(fock_dim):
                # For every vector in the fock basis calculate individually the 
                # overlap of each fock vector which was altered by the hopping terms.
                for column in range(fock_dim):
                    # Count the amount of trues, meaning that the entries are the
                    # same as in fock_basis[row].
                    # Since the fock_basis is an orthonormal basis, only multiplying
                    # the same vectors gives 1, else 0.
                    count_trues_hop = np.sum(
                        flipped_hop_vectors[:,column] == fock_basis[row], axis=1)
                    # This yields the indices where the hopping flipping gives the
                    # same fock vector.
                    overlap_indices_hop = np.argwhere(count_trues_hop == dim)
                    # If there is no overlap, append 0.
                    if len(overlap_indices_hop) == 0:
                        overlap_hop = 0
                    # If there is an overlap, take the values at this points and
                    # sum them up.
                    else:
                        # Sum over all non-zero overlaps with the related values and 
                        # append this value to the list:hamiltonian_fock. Since there are multiple
                        # matrices, take the indices plus the amount of matrices we are right now.
                        overlap_hop = np.sum(
                            sorted_hopping_values[overlap_indices_hop
                            + matrix*len(hopping_entries[0])])
                    # Append the overlap values. 
                    hamiltonian_fock.append(overlap_hop)
        # Reshape the list into the new hamiltonian with the fock basis as basis!
        hamiltonian_fock = np.array(hamiltonian_fock).reshape(amount, 
                                                              fock_dim, fock_dim)
        # Make each of them hermitian.
        hamiltonian_fock = make_hermitian(hamiltonian_fock)
    return hamiltonian_fock


def change_to_fockbasis(hopping_values, hopping_indices, amount, dim, *args, **kwargs):
    """ Takes values and the respective indices of the matrice(s) (there are int:amount many)
    in the spatial basis ( o-o-o would be three nodes and means a 3x3 hopping matrix) and turnes
    them into values in the fock basis which is dependent on how many particles there are.
    Input:
        np.ndarray:hopping_values, 1 dimensional array of length amount*something
        np.ndarray:hopping_indices, 1 dimensional array of length amount*something
            the values and indices are building int:amount many Hamiltonians.
        int:amount, how many Hamiltonians there are
        int:dim, which dimension the Hamiltonians have (and how many nodes there are).
    Output:
        np.ndarray, the Hamiltonians in Fock base.
    """
    # Generate the fock basis vectors. The basis of the hopping
    # term will be changed from spatial to the Fock basis.
    fock_basis = generate_fock_vectors(kwargs.get('number_particles'), dim)
    fock_dim = len(fock_basis)
    # The indices array are indexing the flattened hamiltonian matrices.  But
    # now the indices for the reshaped matrices are needed to denote the
    # swapping (or flipping).
    unraveled_indices = np.array(np.unravel_index(hopping_indices, (amount, dim, dim)))
    # If there is more than one hamiltonian, extract the same indices.
    if amount != 1:
        # To avoid redundancy, the indices of the first matrix are extracted. Since the 
        # Hamiltonians indices are all drawn the same, take the first set.
        sorted_indices, hopping_entries = extract_repeated_indices(unraveled_indices, *args, **kwargs)
    else:
        # Assume here, that the entries are already sorted!
        sorted_indices, hopping_entries = hopping_indices, unraveled_indices[[1,2]]
    # Sort the values, too.
    sorted_hopping_values = hopping_values[sorted_indices]
    # Calculate the swapped (according to the hopping indices) fock vectors.
    # Meaning that here the action of the Hamiltonian on the fock basis is calculated, 
    # since in the end only the overlaps are important.
    flipped_hop_vectors = flip_operator(fock_basis,
                                        hopping_entries[0],
                                        hopping_entries[1], *args, **kwargs)
    # Initialize the list which, when filled, are the Hamiltonians in the fock
    # basis.
    hopping_fock = []
    # For each matrix in int:amount matrices given, calculate the Hamiltonian
    # in fock basis.
    for matrix in range(amount):
        # Go through all vectors in the fock_basis with a for loop. Calculate
        # the overlap of a vector from the fock basis and a vector flipped 
        # through the hopping matrix.
        for row in range(fock_dim):
            # For every vector in the fock basis calculate individually the 
            # overlap of each fock vector which was altered by the hopping terms.
            for column in range(fock_dim):
                # Count the amount of trues, meaning that the entries are the
                # same as in fock_basis[row].
                # Since the fock_basis is an orthonormal basis, only multiplying
                # the same vectors gives 1, else 0.
                count_trues_hop = np.sum(
                    flipped_hop_vectors[:,column] == fock_basis[row], axis=1)
                # This yields the indices where the hopping flipping gives the
                # same fock vector.
                overlap_indices_hop = np.argwhere(count_trues_hop == dim)
                # If there is no overlap, append 0.
                if len(overlap_indices_hop) == 0:
                    overlap_hop = 0
                # If there is an overlap, take the values at this points and
                # sum them up.
                else:
                    # Sum over all non-zero overlaps with the related values and 
                    # append this value to the list:hopping_fock. Since there are multiple
                    # matrices, take the indices plus the amount of matrices we are right now.
                    overlap_hop = np.sum(
                        sorted_hopping_values[overlap_indices_hop
                        + matrix*len(hopping_entries[0])])
                # Append the overlap values. 
                hopping_fock.append(overlap_hop)
    # Reshape the list into the new hamiltonian with the fock basis as basis!
    hopping_fock = np.array(hopping_fock).reshape(amount, fock_dim, fock_dim)
    return hopping_fock


def change_to_particlebasis(gmatrix, dim, **kwargs):
    """ Takes a matrix in 'node' basis to particle basis using the Fock basis states.
    newmatrix_ij = P_<i|gmatrix|j>_P where |i>_P and |j>_P are state vectors of
    the particle space.
    They are constructed via the Fock basis states, this is described in the function
    generate_particlebasis().
    Input:
        np.ndarray:gmatrix of shape (int:dim**2, int:dim**2).
        int:dim, the number of nodes.
        int:kwargs['number_particles'], the number of particles
    Output:
        np.ndarray of shape (int:fockdim, int:fockdim), where int:fockdim is the 
        number of vectors (or dimension) of the corresponding Fock space (or particle space).
    """
    # Generate the basis vectors of the particle space.
    particle_basis = generate_particlebasis(dim, kwargs.get('number_particles'))
    # The shape of the new matrix is given by the dimension of the new basis.
    basisdim = len(particle_basis)
    # Initiate the new matrix with zeros to fill them in the easiest way: through a
    # for loop over each basis vector pair and the overlap P_<i|gmatrix|j>_P. 
    newmatrix = np.zeros((basisdim, basisdim), dtype = np.complex128)
    # Calculate the overlaps.
    for count_column in range(basisdim):
        # Extract the jth basis vector.
        basisvector_j = particle_basis[count_column]
        # Calculate gmatrix|j>_P.
        overlap_vector = gmatrix.dot(basisvector_j)
        # Take this vector and calculate the scalar product of all other vectors
        for count_row in range(basisdim):
            # The entry (newmatrix)_ij is calculated.
            newmatrix[count_column, count_row] = particle_basis[count_row].dot(overlap_vector)
    return newmatrix


def generate_particlebasis(dim, number_particles):
    """ 
    Takes the Fock basis and turns it into the particle basis.
    For example the Fock basis state |2,0,0>_F (two particles are in the first node)
    translated to |1,1>_P, the first particle is in the first node and the second particle
    is in the first node, too.
    |1,0,1>_F translates to 1/sqrt(2)*(|1,3>_P + |3,1>_P).
    The implementation of the particle base happens in bitstrings.
    Meaning that for example |1,1>_P = ([1,0,0]kron[1,0,0]),
    or |1,3>_P = 1/sqrt(2)*([1,0,0]kron[0,0,1] + [0,0,1]kron[1,0,0]).
    Input:
        int:dim, how many nodes there are.
        int:number_particles, how many particles there are.
    Output:
        list of vectors spanning the symmetrized particle basis (bosonic).
    """
    # Initialize the empty list where the generated basis vectors will be stored.
    all_basis_vectors = []
    # Generate the fock states. int:dim is the number of nodes.
    fock_basis = generate_fock_vectors(number_particles, dim)
    # Go through every vector and of the fock basis and generate the corresponding
    # particle vector.
    for fockvector in fock_basis:
        # The basis vectors consists of many parts which are tensor producted
        # togehter. In list:parts_list they will be stored.
        parts_list = []
        # The maximal number of particles in a node determins how long this for 
        # loop will go through the fock vector. 
        # In general the fock vector has to be decomposed into the single bit vectors.
        # Which seems trivial but I couldn't find a nicer way then using this for loop.
        # Decomposition like for example [2,0,1] = [1,0,0] + [1,0,0] + [0,0,1].
        for count_particle in range(np.max(fockvector)):
            # Check where there are entires of only one particle in a node.
            only_ones = np.array(fockvector==1)+0 
            # If this is more than one entry, they have to be splitted. More than one entry
            # means that the sum is greater than one.
            amount = sum(only_ones)
            if amount != 1: 
                # Generate a tuple of indices. In the first entry of the tuple the index goes
                # from 0 to how many entries there are. In the second entry the inices of where
                # the ones are are stored.
                indices = (np.arange(0, amount), np.argwhere(only_ones==1).flatten())
                # With this tuple the entries of a matrix filled with zeros can be set to
                # one, one by one vector, such that each row is a vector with only one number one 
                # in it.
                only_ones = np.zeros((amount, dim))
                only_ones[indices] = 1
            # Depending on where the for loop is, the np.ndarray:only_ones is appended to list:parts_list.
            # If in this node there are more than one particle, the vector has to be appended according to how
            # many particles are in this node. 
            parts_list.append(np.tile(only_ones, count_particle+1).reshape((count_particle+1)*amount, dim)) 
            # The count_particle goes one up, and the entries in the fock vector go down. So in the next
            # loop the nodes with 2 particles, 3 particles, ... are detected as one entry, but in int:count_particle
            # it is stored, how many particle where there.
            fockvector -= 1
        # Make an array out of array out of the list of matrices of different dimensions.
        parts_array = np.vstack(parts_list)
        # Since every combination of the parts vectors have to be tensore producted together, all
        # possible combination of the first, second, ... parts vector has to be calculated.
        perms = list(permutations(np.arange(0, number_particles)))
        # Initiate the list where the arrays which are already tensor producted together are stored.
        final = []
        # Go through every permutation of the indices (combination of the parts vectors).
        for count in range(len(perms)):
            permuted_parts = parts_array[list(perms[count])]
            # Take the tensor product of all of them, in the right order of course! 
            kron_product = permuted_parts[0]
            # Ther is not yet a multi kron function, so a for loop must be used.
            for tensorcount in range(1, len(parts_array)):
                kron_product = np.kron(kron_product, permuted_parts[tensorcount])
            # Append this tensor product of permutations of the parts array.
            final.append(kron_product)
        # Get rid of dublicate which appear when there is more than one particle in a node.
        sum_this = np.unique(np.array(final), axis=0)
        # The last step is to sum the tensor producted permutations of the parts together and
        # normalize by how many unique vectors are used to do so.
        basisvector = np.sum(sum_this, axis=0)/np.sqrt(len(sum_this))
        all_basis_vectors.append(basisvector)
    return all_basis_vectors



def draw_H_noninteracting_OLD(amount, dim, *args, dimension='1D', bandweight=1., **kwargs):
    """Puts together an array filled with  matrices, with the 
    shape (int:amount,int:dim,int:dim).
    Each matrix is drawn from a given distribution.
    1D:
        The diagonal and off diagonal entries next to the diagonal are non-zeros,
        other entries stay zero.
    2D:
        Non-zero entries:
            - diagonal,
            - bands of diagonal where index%sqrt(dim)!= 0,
            - sqrt(dim)th band.
    Input:
        int:amount, the number of matrices in the array.
        int:dim, the dimension of the single matrices in the array.
        float:bandweight, multiply the band entries with this value.
    Output:
        np.ndarray:hamiltonians, the array of hermitian matrices
            with the shape (int:amount,int:dim,int:dim).
    """
    # Initialize the amountxdimxdim matrix, do not reshape it into the final
    # shape, drawing an putting the indices and entries is easier like this.
    # But the data type must include complex numbers, otherwise the imaginary
    # part is mapped onto the real part.
    matrices = np.zeros(dim**2*amount, dtype = np.complex128)
    if dimension == '1D':
        # Get the diagonal indices and entries.
        diag_indices, diag_values = draw_diag(
            amount, dim, *args, diag_func=kwargs['diag_func_hop'], **kwargs)
        # Get the band entries.
        band_indices, band_values = draw_band(
            amount, dim, bandweight, *args, which_band=1,
            band_func=kwargs['band_func_hop'], **kwargs)
        # Put together the indices and values.
        indices = np.append(diag_indices, band_indices)
        values = np.append(diag_values, band_values)
    elif dimension == '2D':
        # Get the diagonal indices and entries.
        diag_indices, diag_values = draw_diag(
            amount, dim, *args, diag_func=kwargs['diag_func_hop'], **kwargs)
        # Calculate the length of one side of the lattice.
        sqrtdim = int(np.sqrt(dim))
        # Get the band indices and entries next to the diagonal.
        band_indices, band_values = draw_band(
            amount, dim, bandweight, *args, which_band=1, leaveout_every=sqrtdim,
            band_func=kwargs['band_func_hop'], **kwargs)
        # Get the second band indices and entries.
        second_band_indices, second_band_values = draw_band(
            amount, dim, bandweight, *args, which_band=sqrtdim, 
            band_func=kwargs['band_func_hop'], **kwargs)
        # Put together the indices and values.
        indices = np.append(diag_indices, [band_indices, second_band_indices])
        values = np.append(diag_values, [band_values, second_band_values])
    # Put the diagonal entries in the matrices array.
    matrices[indices] = values
    band_diagonal_matrices = matrices.reshape(amount, dim, dim)
    # Make the single matrices hermitian.
    hamiltonians = make_hermitian(band_diagonal_matrices)
    return hamiltonians


def draw_1d_noninteracting(amount, dim, *args, bandweight=1., **kwargs):
    """ Puts together an array filled with matrices, with the shape
    (int:amount,int:dim,int:dim). Each matrix is drawn from a given distribution.
    The diagonal and off diagonal entries next to the diagonal are non-zeros,
    other entries stay zero.
    Input:
        int:amount, the number of matrices in the array.
        int:dim, the dimension of the single matrices in the array.
        float:bandweight, multiply the band entries with this value.
    Output:
        np.ndarray:hamiltonians, the array of hermitian matrices
            with the shape (int:amount,int:dim,int:dim).
    """
    hamiltonians = draw_H_noninteracting(amount, dim, *args,
        bandweight=bandweight, dimension='1D', **kwargs)
    return hamiltonians


def draw_2d_noninteracting(amount, dim, *args, bandweight=1., **kwargs):
    """ Puts together an array filled with matrices, with the shape
    (int:amount,int:dim,int:dim). Each matrix is drawn from a given distribution.
    The diagonal and off diagonal entries next to the diagonal are non-zeros,
    other entries stay zero.
    Input:
        int:amount, the number of matrices in the array.
        int:dim, the dimension of the single matrices in the array.
        float:bandweight, multiply the band entries with this value.
    Output:
        np.ndarray:hamiltonians, the array of hermitian matrices
            with the shape (int:amount,int:dim,int:dim).
    """
    hamiltonians = draw_H_noninteracting(amount, dim, *args,
        bandweight=bandweight, dimension='2D', **kwargs)
    return hamiltonians

    


def draw_H_interacting(amount, dim, *args, dimension='1D', bandweight=1., **kwargs):
    """This function puts together the hopping matrix and the interaction 
    matrix, like they have to be added after calculating the time evolution.
    Then the change of basis is done, meaning that the Hamiltonian is written
    in the Fock-basis now.
    First the hopping entries are drawn, then the interaction entries.
    The basis of this drawn matrices are spatial, meaning that h_11 represents the
    hopping entry from the first node to the first node (or just staying there).
    The basis to change to is the Fock-basis.
    Input:
        int:amount, the number of single Hamiltonians.
        int:dim, the number of nodes and dimension of the drawn hopping and
            interactin term.
    Needed entries in kwargs:
        func:diag_func_hop, func:band_func_hop
    Output:
        np.ndarray:hamiltonian_fock with shape (amount, fock_dim, fock_dim) where
            fock_dim = (dim+2-1)!/dim!(2-1)!, the number of new basis elements.
    """
    # Extract how many particles there are in the system. Important for the Fock basis.
    # If nothing is given, take number_particles = 2.
    number_particles = kwargs.get('number_particles', 2)
    # Generate the fock basis vectors. The basis of the hopping and interacting
    # term will be changed from spatial to the Fock basis.
    fock_basis = generate_fock_vectors(number_particles, dim)
    fock_dim = len(fock_basis)
    if dimension == '1D':
        # Get the hopping indices and entries.
        diag_indices, diag_values = draw_diag(
            amount, dim, *args, diag_func=kwargs['diag_func_hop'], **kwargs)
        band_indices, band_values = draw_band(
            amount, dim, bandweight, *args, which_band=1,
            band_func=kwargs['band_func_hop'], **kwargs)
        hopping_indices = np.append(diag_indices, band_indices)
        hopping_values = np.append(diag_values, band_values)
    elif dimension == '2D':
        # Get the diagonal indices and entries.
        diag_indices, diag_values = draw_diag(
            amount, dim, *args, diag_func=kwargs['diag_func_hop'], **kwargs)
        # Calculate the length of one side of the lattice.
        sqrtdim = int(np.sqrt(dim))
        # Get the band indices and entries next to the diagonal.
        band_indices, band_values = draw_band(
            amount, dim, bandweight, *args, which_band=1, leaveout_every=sqrtdim,
            band_func=kwargs['band_func_hop'], **kwargs)
        # Get the second band indices and entries.
        second_band_indices, second_band_values = draw_band(
            amount, dim, bandweight, *args, which_band=sqrtdim, 
            band_func=kwargs['band_func_hop'], **kwargs)
        # Put together the indices and values.
        hopping_indices = np.append(diag_indices, [band_indices, second_band_indices])
        hopping_values = np.append(diag_values, [band_values, second_band_values])
    # The hopping_indices are indexing the flattened hamiltonian matrices.  But
    # now the indices for the reshaped matrices are needed to denote the
    # swapping (or flipping).
    unraveled_hopping_indices = np.array(np.unravel_index(hopping_indices,
                                                          (amount, dim, dim)))
    # To avoid redundancy, the hopping indices of the first matrix are
    # extracted. Since the hamiltonians are all drawn the same, the hopping
    # indices are the same for all of them.
    sorted_hopping_indices, hopping_entries = extract_repeated_indices(
        unraveled_hopping_indices, *args, **kwargs)
    # Sort the values, too.
    sorted_hopping_values = hopping_values[sorted_hopping_indices]
    # Calculate the swapped (according to the hopping indices) fock vectors.
    # Meaning that here the action of the Hamiltonian on the fock basis is calculated, 
    # since in the end only the overlaps are important.
    flipped_hop_vectors = flip_operator(fock_basis,
                                        hopping_entries[0],
                                        hopping_entries[1], *args, **kwargs)
    # Do the same for the interaction term.
    # Get the indices and values of the interaction term.  They have the same
    # structure as the hopping indices and values but are meant to index a 
    # dim**2 x dim**2 matrix, as it is mathematically a 4 tensor.
    if dimension == '1D':
        interaction_indices, interaction_values = interaction_1d(
            amount, dim, *args, **kwargs)
        unraveled_interaction_indices = np.array(np.unravel_index(interaction_indices,
                                                        (amount, dim, dim, dim, dim)))
    elif dimension == '2D':
        interaction_indices, interaction_values = interaction_2d(
            amount, dim, *args, **kwargs)
        unraveled_interaction_indices = np.array(np.unravel_index(interaction_indices,
                                                        (amount, dim, dim, dim, dim)))
    # To, again, avoid redundancy, the interaction indices of the first matrix
    # are extracted. 
    sorted_interaction_indices, interaction_entries = extract_repeated_indices(
        unraveled_interaction_indices, *args, **kwargs)
    # Sort the values, too.
    sorted_interaction_values = interaction_values[sorted_interaction_indices]
    # Calculate the swapped (according to the interaction indices) fock vectors.
    # First swap according to the first and third layer.
    flipped_int_vectors = flip_operator(fock_basis,
                                        interaction_entries[0],
                                        interaction_entries[2], *args, **kwargs)
    # Then swap according to the second and fourth layer.
    flipped_int_vectors = flip_operator(flipped_int_vectors,
                                        interaction_entries[1],
                                        interaction_entries[3], *args, 
                                        tiled=True, **kwargs)
    # Initialize the list which, when filled, are the Hamiltonians in the fock
    # basis.
    hamiltonian_fock = []
    # For each matrix in int:amount matrices given, calculate the Hamiltonian
    # in fock basis.
    for matrix in range(amount):
        # Go through all vectors in the fock_basis with a for loop. Calculate
        # the overlap of a vector from the fock basis and a vector flipped 
        # through the hopping matrix.
        for row in range(fock_dim):
            # For every vector in the fock basis calculate individually the 
            # overlap of each fock vector which was altered by the hopping terms.
            for column in range(fock_dim):
                # Count the amount of trues, meaning that the entries are the
                # same as in fock_basis[row].
                # Since the fock_basis is an orthonormal basis, only multiplying
                # the same vectors gives 1, else 0.
                count_trues_hop = np.sum(
                    flipped_hop_vectors[:,column] == fock_basis[row], axis=1)
                count_trues_int = np.sum(
                    flipped_int_vectors[:,column] == fock_basis[row], axis=1)
                # This yields the indices where the hopping flipping gives the
                # same fock vector.
                overlap_indices_hop = np.argwhere(count_trues_hop == dim)
                overlap_indices_int = np.argwhere(count_trues_int == dim)
                # If there is no overlap, append 0.
                if len(overlap_indices_hop) == 0:
                    overlap_hop = 0
                # If there is an overlap, take the values at this points and
                # sum them up.
                else:
                    # Sum over all non-zero overlaps with the related values and 
                    # append this value to the list:hamiltonian_fock.
                    overlap_hop = np.sum(
                        sorted_hopping_values[overlap_indices_hop[0]
                        + matrix*len(hopping_entries[0])])
                # Again for the interaction overlap.
                if len(overlap_indices_int)==0:
                    overlap_int = 0
                # If there is an overlap, take the values at this points and
                # sum them up.
                else:
                    # Sum over all non-zero overlaps with the related values and 
                    # append this value to the list:hamiltonian_fock.
                    overlap_int = np.sum(
                        sorted_interaction_values[overlap_indices_int[0]
                        + matrix*len(interaction_entries[0])])
                # Append the overlap values. 
                hamiltonian_fock.append(overlap_hop + overlap_int)
    # Reshape the list into the new hamiltonian with the fock basis as basis!
    hamiltonian_fock = np.array(hamiltonian_fock).reshape(amount, 
                                                          fock_dim, fock_dim)
    # Make them hermitian.
    hamiltonian_fock = make_hermitian(hamiltonian_fock)
    return hamiltonian_fock

def draw_1d_interacting(amount, dim, *args, bandweight=1., **kwargs):
    """ Calls  draw_H_interacting with the dimension set to 1D
    """
    hamiltonians = draw_H_interacting(amount, dim, *args,
        dimension='1D', bandweight=bandweight, **kwargs)
    return hamiltonians


def draw_2d_interacting(amount, dim, *args, bandweight=1., **kwargs):
    """ Calls  draw_H_interacting with the dimension set to 2D.
    """
    hamiltonians = draw_H_interacting(amount, dim, *args,
        dimension='2D', bandweight=bandweight, **kwargs)
    return hamiltonians

###############################################################################
############################ Sampling methods #################################
###############################################################################

def sampling_pairs(N, dim, unitary_func, *args, **kwargs):
    """ Calculates the SUMMANDS (without taking the power or normalizing,
    hence not the whole frame potential) which are needed for the calculation
    of the frame potential F which goes as follows:
    F(S_N, Z_N) = 1/N^2 sum_(i,j) |Tr(U_i^dag*V_j)|^(2k)
    with S_N = {U_i~sigma_epsilon}^{N}_i=1, Z_N = {V_i~sigma_epsilon}^{N}_i=1 
    Meaning cross correlations are NOT calculated.
    Input:
        np.ndarray:gUnitaries, the set of depth N of unitaries
    Output:
        np.ndarray:unitaries,
        list:framepotentials,
    """
    # Check N and the dimension. There is a maximal amount of entries a
    # computer can handle,  in this case its approximately 360.000.
    max_N = int(360000.0/dim**2)
    # Try for cluster.
    max_N = 36000000
    # How many runs are needed:
    runs = int(float(N)/max_N)
    # Initialize the list:framepotential_summands variable.
    framepotential_summands = []
    # Do the runs.
    for kk in range(runs):
        # Draw two sets of unitaries.
        unitaries1 = unitary_func(max_N, dim, *args, **kwargs)
        unitaries2 = unitary_func(max_N, dim, *args, **kwargs)
        # And calculate the product of  it, one matrix from the first set
        # with one matrix from the second set, matmul does that.
        pairs = np.matmul(np.transpose(unitaries1,(0,2,1)).conj(), unitaries2)
        # Take the trace of every product matrix.
        trace_pairs = abs(np.trace(pairs, axis1=1, axis2=2))
        # Append the values.
        framepotential_summands.append(list(trace_pairs))
    # Calculate the remaining pairs to be calculated:
    remaining = int((float(N)/max_N - runs) * max_N)
    # Draw two sets of unitaries.
    unitaries1 = unitary_func(remaining, dim, *args, **kwargs)
    unitaries2 = unitary_func(remaining, dim, *args, **kwargs)
    # And calculate the product of  it, one matrix from the first set
    # with one matrix from the second set, matmul does that.
    pairs = np.matmul(np.transpose(unitaries1,(0,2,1)).conj(), unitaries2)
    # Take the trace of every product matrix.
    trace_pairs = abs(np.trace(pairs, axis1=1, axis2=2))
    # Append the values.
    framepotential_summands.append(list(trace_pairs))
    # Since for the spectral gap the unitaries are needed,
    # append them and return them.
    unitaries = np.append(unitaries1, unitaries2, axis=0)
    # Flatten the list and then return it.
    summands_flattened = [item for sublist in framepotential_summands for item in sublist]
    return unitaries, summands_flattened


def sampling_twosets(gunitaries, unitary_func, *args, **kwargs):
    """ Calculates the SUMMANDS (without taking the power or normalizing,
    hence not the whole frame potential) which are needed for the calculation
    of the frame potential F which goes as follows:
    F(S_N, Z_N) = 1/N^2 sum_(i,j) |Tr(U_i^dag*V_j)|^(2k)
    with S_N = {U_i~sigma_epsilon}^N_i=1, Z_N = {V_i~sigma_epsilon}^N_i=1 
    Meaning cross correlations are NOT calculated.
    Input:
        np.ndarray:gunitaries, the set of depth N of unitaries
    Output:
        float:framepotential
    """
    # Extract the dimensions of the unitaries
    N, dim, _ = gunitaries.shape
    # Initialize the float:framepotential number
    framepotential_summands = []
    # Fix first index, draw the unitary, and sum over all second indices.
    # Repeat with every first index.
    for ii in range(0, N):
        # Draw one unitary, take the adjoint.
        unitary = unitary_func(1, dim, *args, **kwargs)[0].conj().T
        # Calculate every product with the given unitaries.
        for kk in range(0, N):
            # Calculate |Tr(U_ii^dag*V_j)|^2.
            summand_without_potential = abs(np.trace(np.dot(unitary,
                gunitaries[kk])))
            # Store the summand without potential inside a list.
            framepotential_summands.append(summand_without_potential)
    # Return the frame potential summand values to calculate the
    # different k designs..
    return framepotential_summands


def sampling_oneset(gunitaries, *args, **kwargs): 
    """ Calculates the SUMMANDS (without taking the power or normalizing,
    hence not the whole frame potential) which are needed for the calculation
    of the frame potential F which goes as follows:
    F(S_N) = 1/N^2 sum_(i,j) |Tr(U_i^dag*U_j)|^(2k)
    with S_N = {U_i~sigma_epsilon}^N_i=1
    Meaning cross correlations are calculated.
    Input:
        np.ndarray:gunitaries, the set of depth N of unitaries
    Output:
        list:framepotential_summands
    """
    # Extract the dimensions of the unitaries

    N, _, _ = gunitaries.shape
    # Initialize the float:framepotential number
    framepotential_summands = []
    # Fix first index and sum over all second indices.
    # Repeat with every first index.
    for ii in range(0, N):
        # Extract U_i.
        U_ii = gunitaries[ii]
        # Calculate the daggered U_i.
        U_ii_dag = U_ii.T.conj()
        # ii is fixed now, sum over all second indices.
        for jj in range(0, N):
            # Extract U_j.
            U_jj = gunitaries[jj]
            # Calculate |Tr(U_i^dag*U_j)|^2.
            summand_without_potential = abs(np.trace(np.dot(U_ii_dag, U_jj)))
            # Store the summand without potential inside a list.
            framepotential_summands.append(summand_without_potential)
    # Return the frame potential summand values to calculate the different
    # k designs.
    return framepotential_summands

###############################################################################
############################ Frame Potentials #################################
###############################################################################

def average_framepotential(N, dim, design_list, unitary_func, runs=1,
    *args,**kwargs):
    """ This function combines the functions of drawing (or building) a set
    of unitaries and calculating the frame potential of the set. If averaging
    is necessary, the number int:runs of averaging can be increased.
    Input:
        int:N,    the amount of band diagonal Hamiltonians.
        int:dim,  the dimension of the Hamiltonians.
        list:design_list, gives the powers in frame potential calculation.
        int:runs, over how many times the frame potential is averaged.
        func:unitary_func
    Return:
        float:averagedFramePotantial
    """
    # Check if the spectral gap should be calculated and save, too.
    if kwargs.get('save_spectralgap'):
        # Set the variable such that it can store the singular values. 
        singularvalues  = 0
    else:
        # By setting it to False, in the for loop the spectral gap will not
        # be calculated and stored.
        singularvalues  = False
    # Initiate the averagedFramePotential variable
    averaged_framepotential_array = np.zeros(len(design_list))
    # Draw int:runs times Hamiltonians, build unitaries out of them
    # and calculate the frame potential, average over all runs.
    for current_run in range(runs):
        # Check which sampling method is required:
        # drawPairs, one_set, twosets
        if kwargs.get('sampling_method') == 'oneset':
            # Build the set unitaries.
            unitaries = unitary_func(N, dim, *args, **kwargs)
            # Calculate the frame potential summand where the 2kth power
            # is not yet taken!
            framepotential_summands = np.array(sampling_oneset(unitaries, *args,
                **kwargs))
            # There are N**2 values in the array
            divide_by = N**2
        if kwargs.get('sampling_method') == 'twosets':
            # Build the first set unitaries.
            unitaries = unitary_func(int(N/2), dim, *args, **kwargs)
            # Calculate the frame potential summands where the 2kth power
            # is not yet taken!
            framepotential_summands = np.array(sampling_twosets(unitaries,
                unitary_func, *args, **kwargs))
            # There are (N/2)**2 values in the array
            divide_by = (N/2)**2
        elif kwargs.get('sampling_method') == 'pairs':
            # Calculate the frame potential summands where the 2kth power
            # is not yet taken!
            unitaries, framepotential_summands = sampling_pairs(int(N/2.), dim,
                unitary_func, *args, **kwargs)
            framepotential_summands = np.array(framepotential_summands)
            # There are N/2 values in the array
            divide_by = N/2
        # Do the one set calculation if nothing is stated.
        else:
            # Build the set of unitaries.
            unitaries = unitary_func(N, dim, *args, **kwargs)
            # Calculate the frame potential summands where the 2kth power
            # is not yet taken!
            framepotential_summands = np.array(sampling_oneset(unitaries,
                                                               *args, **kwargs))
            # There are N**2 values in the array
            divide_by = N**2
        # Take the different to the power of k value arrays and sum them up!
        for count in range(len(design_list)):
            framepotentials_powerarray = framepotential_summands**(
                2*design_list[count])
            # Sum the values up (sum over product of all pairs of matrices).
            framepotential = np.sum(framepotentials_powerarray)/divide_by
            # Take the previos value and sum the different values
            # from the runs up.
            averaged_framepotential_array[count] = \
                averaged_framepotential_array[count] + framepotential
        # If needed, compute the spectral gap of the unitaries.
        if type(singularvalues) != bool:
            if len(design_list) != 1:
                print('kdesingList must have only ONE entry!')
            # Average over the number of runs, too.
            singularvalues = singularvalues + calculate_spectral_gap(unitaries,
                design_list[0])
    if type(singularvalues) != bool:
        # Average over the number of runs, too.
        singularvalues = singularvalues/runs
    # Return the averaged value by dividing it by the number of runs.
    return averaged_framepotential_array/runs, singularvalues

###############################################################################
################################ Circuits #####################################
###############################################################################

def many_circuits(N, dim, *args, **kwargs):
    """ This function first draws kwargs["depth"] many unitaries and multiplies
    them with each other to build the circuit. This is done int:N many times.
    Input:
        int:N,
        int:dim,
    Output:
        np.ndarray, N many unitaries of shape dim x dim.
    """
    depth = kwargs["depth"]
    # Extract the function with which the unitaries are drawn.
    circuit_func = kwargs["circuit_func"]
    # Get depth many unitaries.
    one_circuit = circuit_func(depth, dim, *args, **kwargs)
    # Initiate matrix to store product of matrices in.
    # Take into account in case of change of basis the dimension changes.
    new_dim = one_circuit.shape[1]
    # Multiply the unitaries with each other to build one matrix
    # out of them (circuit).
    multi_circuit = np.linalg.multi_dot(one_circuit)
    circuits = np.array([multi_circuit])
    # Do this procedure N-1 times.
    for index in range(1, N):
        # Get depth many unitaries.
        one_circuit = circuit_func(depth, dim, *args, **kwargs)
        # Multiply the unitaries with each other to build one matrix
        # out of them (circuit).
        many_circuit = np.linalg.multi_dot(one_circuit)
        circuits = np.array(np.append(circuits,
            np.array([many_circuit]))).reshape(index+1, new_dim, new_dim)
    return circuits


##############################################################################
############################# Unitaries #######################################
##############################################################################


def unitaries_from_hamiltonians(amount, dim, *args, **kwargs):
    """ This function makes multiple Hamiltonians which are generated from the
    func:hamiltonian_func and takes the singular values
    to the power of float:time times 1j. Needed parameters are passed via
    args and kwargs.
    Input:
        int:amount,
        int:dim,
        func:hamiltonian_func, returns an array of Hamiltonians.
        float:time,          the amount of time the unitary is evolving.
    Output:
        np.ndarray:unitaries,    of shape (int:N, int:dim, int:dim)
    """
    # Extract the function which will be used to generate the Hamiltonians.
    hamiltonian_func = kwargs['hamiltonian_func']
    # First generate the array of Hamiltonians.
    hamiltonians = hamiltonian_func(amount, dim, **kwargs)
    # The time step is stored in kwargs.
    time = kwargs.get('time', 1)
    # Perform a singular value decomposition to extract the singular values.
    # The unitaries U and Vdagg and the singular values are stored in
    # single matrices in one big matrix.
    full_U, full_singvalues, full_V_dagg = np.linalg.svd(hamiltonians)
    # Perform the exponential calculation on the singular values
    exponentiated_singvalues = np.exp(full_singvalues * 1j * time)
    # Initiate int:N matrices filled with int:dim x int:dim times zeros.
    # Since when a change of basis happens, the dimensions of the hamiltonians
    # changes, the dimension variable int:dim is not correct anymore.
    diag_singvalues = np.zeros((
        amount, hamiltonians.shape[1], hamiltonians.shape[1]), dtype = np.complex128)
    # Fill the diagonals with singular values each.
    np.einsum('jii->ji', diag_singvalues)[:] = exponentiated_singvalues
    # Now multiply everything:
    # 1 tensor full_U * 1 tensor diag(full_singvalues) * full_V_dagg
    # For the first step, where full_U and fullVdag is not yet restricted,
    # restrict it by  hand
    rightside_multiplicationmatrix = np.matmul(diag_singvalues, full_V_dagg)
    unitaries = np.matmul(full_U, rightside_multiplicationmatrix)
    return unitaries


def one_haar_unitary(dim, *args, **kwargs):
    """ Return an array (dim,dim) a Haar random unitary matrix of size int:dim.
    Input:
        int:dim, the dimension dim x dim of the matrix drawn.
    Output:
        np.ndarray:U, an array with shape (dim, dim) with haar random entries.
    """
    U = np.random.normal(0, 1, [dim, dim]) + \
        1j * np.random.normal(0, 1, [dim, dim])
    U, R = np.linalg.qr(U)
    D = np.diag(R)
    L = np.diag(np.divide(D, np.abs(D)))
    U = U.dot(L)
    return U


def many_haar_unitaries(N, dim, *args, **kwargs):
    """ This function takes the one_haar_unitary(dim) function
    and repeats it int:N times. 
    Input:
        int:N, amount of haar random unitaries.
        int:dim, dimension of single unitary.
    Output:
        np.ndarray:unitaries, with shape (N, dim, dim)
    """
    # Initiate the first haar random matrix.
    unitaries = one_haar_unitary(dim)
    # Loop over the remaining N-1 unitaries and append them.
    for kk in range(1, N):
        # Draw a haar random unitary.
        U = one_haar_unitary(dim)
        # Append it to the others, axis is important to be 0.
        unitaries = np.append(unitaries, U, axis = 0)
    # Reshape the array which is still a 1D N*dim*dim array.
    unitaries = unitaries.reshape(N, dim, dim)
    return unitaries

###############################################################################
############################### Simulation ####################################
###############################################################################




def run_simulation(unitary_func, depth_list, dim_list, design_list, N_list,
                   *args, **kwargs):
    """ With this function the frame potential is calculated in dependency of
    the variable str:vsWhat. The x-axis is given with np.ndarray:xAxis.
    The plot is stored with a not yet existing name where the number in the
    end is raised for every new plot.
    Input:
        str:unitary_func,
        list/np.ndarray:dim_list,
        list/np.ndarray:N_list
        list/np.ndarray:design_list,
        list/np.ndarray:depth_list,
    """
    if kwargs.get('save_data'):
        # Find a not yet existing directory, make it and store the name.
        # Something like "numericalData/12Simulation/"
        dir_name = structure_functions.create_dir()
        # Choose a file name for the .txt file where the frame potential values
        # are store.
        filename = f'{dir_name}/framepotentials.txt'
    else:
        dir_name,filename = '', ''
    # Sometimes the spectral gap has to be displayed, for that data create
    # another file name.
    if kwargs.get('save_spectralgap'):
        # Create a second file name.
        filename_sv = f'{dir_name}/spectralgap.txt'
        # Save in file that all information are in '..framepotentials.txt'.
        with open(filename_sv, 'a') as file:
            file.write('# Arrays of parameters are stored in same directory \n')
            file.write('# in file with ending ..framepotentials.txt \n')
    else:
        filename_sv = ''
    # Store the general information about how long the given lists are
    # and in which order they are stored.
    if kwargs.get('save_data'):
        # To make the automation of plotting and working with the data easier,
        # store the order in which the lists are stored.
        # Everything else depends on this string. Meaning altering this string
        # alters the way of storing.
        order_string = '# Order: depth_list, dim_list, N_list, design_list \n'
        with open(filename, 'a') as file:
            file.write(order_string)
        # Make a list out of the order_string.
        order_list = order_string[9:-2].split(', ')
        # Make space between two simulations in the labBook.txt file.
        structure_functions.write_to_txt('numericalData/labBook.txt',
                                         {'\n' : '\n'})
        # Write parameters defining this simulation into a labBook.txt file.
        structure_functions.write_to_txt('numericalData/labBook.txt',
            start=str(datetime.datetime.now()))
        # Write all the important information about the lists into the labbook file.
        structure_functions.write_labbook(
            'numericalData/labBook.txt',  datafile=filename,
            depth_list=depth_list, dim_list=dim_list, N_list=N_list,
            design_list=design_list)
        # And write the stuff stored in the kwargs dictionary into the labBook.
        structure_functions.write_to_txt('numericalData/labBook.txt', **kwargs)
        # Store the length of the lists to be able to recover them from the file.
        structure_functions.make_head_txtfile(
            filename, order_list, depthlen=len(depth_list),
            dimlen=len(dim_list), Nlen=len(N_list), designlen=len(design_list))
        # Store all given lists in the right order in the actual storage txt file.
        structure_functions.arrays_to_txtfile(filename, depth_list, dim_list,
                                              N_list, design_list)
    # Now loop over all lists to compute the desired dependencies.
    for depth in depth_list:
        # Store the int:depth variable in the dictionary since the functions are
        # build such that they only take the amount and dimension of unitaries
        # as main variables.
        kwargs['depth'] = depth
        for dim in dim_list:
            for N in N_list:
                # averaged_fp is an array containing the frame potential values
                # for the different k-values
                averaged_fp, sing_values = average_framepotential(
                    N, dim, design_list, unitary_func, *args, **kwargs)
                # Save this value to the designated .txt file.
                if kwargs['save_data']:
                    structure_functions.arrays_to_txtfile(filename, averaged_fp)
                # If wanted, save the spectral gap computation.
                if type(sing_values) != bool:
                    # Save the singular value array into the file, all the
                    # information are in the other "..framepotentials.txt"
                    # file.
                    structure_functions.arrays_to_txtfile(filename_sv,
                                                          sing_values)
                print(f'N:{N}, depth:{depth}, dim:{dim}, moment:{design_list}, FP:{averaged_fp}')
    if kwargs['save_data']:
        # Tell in labBook.txt when the simulation ended.
        structure_functions.write_to_txt('numericalData/labBook.txt',
            Done=f'for {filename}\n{datetime.datetime.now()}\n')
    # Done. Return the directory in which the data is stored, the filename of
    # the framepotential file and the file name of the singular values of the
    # moment operator file.
    return dir_name, filename, filename_sv

def run_kernel_simulation(unitary_func, depth_list, dim_list,
                          design_list, N_list, params_dictionary):
    """This function calls the run_simulation function. Since in the worker where several kernels can
    be used a **kwargs argumant cannot be used, this is the workaround.
     Input:
        str:unitary_func,
        list/np.ndarray:dim_list,
        list/np.ndarray:N_list
        list/np.ndarray:design_list,
        list/np.ndarray:depth_list,
        dict:params_dictionary
    """
    _, _, _ = run_simulation(unitary_func, depth_list, dim_list, design_list, N_list,
                             save_data=True, **params_dictionary)
