import numpy as np
import time

from multiprocessing import Process, Queue
from itertools import product

# Hier kannst du die Funktionen importieren, die du brauchst.
# Dein Skript sollte in eine Funktion gekapselt sein, der
# du die Parameter hier uebergeben kannst. 

from asrb import *
 
# parameter section
# Hier kannst Du deine Parameter fuer die
# Funktion definieren. Ich lasse das mal als
# Beispiel stehen. Die Parameter muessen
# als Liste uebergeben werden.

#################################################
# !! What to check before running a simulation: #
#################################################
save_data = True
save_spectralgap = False
kernels = 20
########################################
# Define the values for each variable list. #
########################################
a, b, c = np.arange(2, 20, 2), np.arange(20, 50, 10), np.array([80, 120])
depth = np.append(a, np.append(b, c))
depth_list = list(np.tile(depth, kernels).reshape(kernels, len(depth)))
# depth_list = [np.array([2]), np.array([2])]
# depth_list = [np.repeat(100, 1)]
dim_list  = [[9]]
# That is for non interacting
design_list = [[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]]
# This is for interacting
# design_list = [[1, 2, 3]]
N = 2*9**4
N_list = [[N]]
# N_list = list(np.tile(N, kernels).reshape(kernels, 1))
# Define a dictionary to store important information.
params = {}
## unitary_func
# many_circuits
# many_haar_unitaries
unitary_func_list = [many_circuits]
## circuit_func
# unitaries_from_hamiltonians
params["circuit_func"] = unitaries_from_hamiltonians
#############################################################################
######################## If band diagonal is chosen ###########################
#############################################################################
## hamiltonian_func
# draw_1d_noninteracting
# draw_2d_noninteracting
# draw_1d_interacting
# draw_2d_interacting
params["hamiltonian_func"] = draw_2d_noninteracting
# Set the number of particles.
params["number_particles"] = 1
## distributions
# normal_distribution
# uniform_distribution
# repeated_distribution, set distribution=normal_disbtribution
# only_number, set fixed_value=1.
params["diag_func_hop"] = only_number
params["band_func_hop"] = uniform_distribution
params["diag_func_int"] = only_number
## For drawing one entry and copying it to the whole diagonal/band for one matrix.
# random_entry_uniform
# random_entry_normal
# fixed_value
params["random_entry_uniform"] = True
params["low"] = -2
params["high"] = 2
# This defines the mean for the normal distribution.
# params["loc"] =  0.
# # This defines the deviation of the normal distribution.
# params["scale"] = 1.
#############################################################################
#############################################################################
#############################################################################
# Set the k-design value for calculating the frame potentials.
params["time"] = 1.
# Define how much smaller or bigger the band diagonal entries are than the
# diagonal entries.
params["bandweight"] = 1.
# Save the spectral gap singular values.
params["save_spectralgap"] = save_spectralgap
## sampling method
# pairs
# oneset
# twosets
params['sampling_method'] = 'pairs'

# Make the dictionary into a list to make it iterable.
params_list = [params]

class Worker(Process):
    def __init__(self, queue):
        super(Worker, self).__init__()
        self.queue = queue
    def run(self):
        print('Worker started')
        for data in iter(self.queue.get,None):
            # Hier kannst du festlegen, was er ausgeben soll,
            # sobald ein neues skript gestartet wird. Ich
            # lasse immer die benutzten Parameter anzeigen.
            # Print-Befehle Deines Skriptes werden trotzdem
            # angezeigt (aber bis zu 8 mal gleichzeitig.
            np.random.seed()
            print(data)
            # Den Stern brauchst du, wenn deine Funktion mehr
            # als einen Parameter enthaelt, andernfalls kannst Du
            # ihn auch weglassen.
            ##########################
            ## Run the actual code. ##
            ##########################
            run_kernel_simulation(*data)


request_queue = Queue()
for i in range(kernels):
    Worker(request_queue).start()
# Hier legst du die Parameter fest. Wichtig ist,
# dass sie in derselben Reihenfolge stehen, wie
# in der Funktionsdefinition.
for data in product(unitary_func_list, depth_list, dim_list, design_list, N_list, params_list):
    request_queue.put(data)
     # Since I want the scripts to write their paramters
    # in labbook.txt, wait 3 second before starting a new script to not interfere with
    # the writing in the .txt file.
    time.sleep(3)
# Sentinel objects to allow clean shutdown: 1 per worker.
# Hier legst Du die Anzahl an Kernen fest (1,8)
for i in range(kernels):
    request_queue.put(None)

