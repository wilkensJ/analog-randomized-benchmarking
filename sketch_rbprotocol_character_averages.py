import numpy as np
import time

import rbprotocol
import structure_functions
import asrb
import pdb
import datetime

# Initiate the dictionary where parameters for the simulation can be set.
params = {}
# dimensions, number of nodes
dim = 3
# Important for character function
number_particles = 1
# Not necesarry but the function wants it.
runs = 1
# Choose sequence lenghts. Here the parallel work can be used.
exponents = np.arange(6,7)
# final_sequences = np.array([4, 16, 64, 256, 1024, 4096, 16384])
final_sequences = 4**exponents
## Which character function should be used?
# 'character_function_young'
# 'character_function_schur'
params['characterfunction'] = 'character_function_schur'
params["interacting"] = False
#############################################################################
############################## unitaries ##################################
#############################################################################
## unitary_func
# many_circuits
# many_haar_unitaries
# one_haar_unitary
# params['unitary_func'] =  asrb.many_circuits
params['unitary_func'] =  asrb.many_haar_unitaries
# Set the depth of the circuits.
params['depth'] = 1
## circuit_func
# unitaries_from_hamiltonians
# --params["circuit_func"] = asrb.unitaries_from_hamiltonians
#############################################################################
######################## If band diagonal is chosen ###########################
#############################################################################
## hamiltonian_func
# draw_1d_noninteracting
# draw_2d_noninteracting
# draw_1d_interacting
# draw_2d_interacting
# --params["hamiltonian_func"] = asrb.draw_1d_noninteracting
## distributions
# normal_distribution
# uniform_distribution
# repeated_distribution, set distribution=normal_disbtribution
# only_number, set fixed_value=1.
# --params["diag_func_hop"] = asrb.only_number
# --params["band_func_hop"] = asrb.normal_distribution
# --params["diag_func_int"] = asrb.only_number
## For drawing one entry and copying it to the whole diagonal/band for one matrix.
# random_entry_uniform
# random_entry_normal
# fixed_value
# --params["random_entry_normal"] = True
# params["low"] = -2
# params["high"] = 2
# This defines the mean for the normal distribution.
# --params["loc"] =  0.
# This defines the deviation of the normal distribution.
# --params["scale"] = 1.
#############################################################################
#############################################################################
#############################################################################
# Set the k-design value for calculating the frame potentials.
# --params["time"] = 1.
# Define how much smaller or bigger the band diagonal entries are than the
# diagonal entries.
# --params["bandweight"] = 1.
params['which_simulation'] = 'character_average'
params['spectralgap'] = True
params['store_projector'] = True

# Build the list of filenames where the data will be stored.
dirname = structure_functions.create_dir(pathname='numericalData_rbprotocol')
# Build all the filenames for the postprocessed arrays to store.
filenames_list = []
for count in range(1):
    filenames_list.append(f'{dirname}/postprocessed_data{count}.txt')
# Write all the stuff in the labbook.
structure_functions.write_to_txt('numericalData_rbprotocol/labBook.txt', {'\n' : '\n'})
structure_functions.write_to_txt('numericalData_rbprotocol/labBook.txt', start=datetime.datetime.now())
structure_functions.write_to_txt('numericalData_rbprotocol/labBook.txt', dirname=dirname,
    dim=dim, number_particles = number_particles, runs=runs, **params)

# projector = rbprotocol.build_projector_haar(4000, dim, number_particles, number_boxes=1, **params)


rbprotocol.run_kernel_simulation_version2(dim, number_particles, final_sequences, runs, filenames_list, params)

with open(f'{dirname}/filenames.txt', 'a') as file:
    for name in filenames_list:
        file.write(name + '\n')
