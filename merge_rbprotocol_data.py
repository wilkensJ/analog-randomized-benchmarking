import rbprotocol
import structure_functions
import os.path
import pdb

dirname = 'numericalData_rbprotocol/7Simulation/'
filename_filenames = dirname + 'filenames.txt'
with open(filename_filenames, 'r') as file:
    filenames_list_with_n = file.readlines()
filenames_list = []
for filename in filenames_list_with_n:
    if os.path.isfile(filename[:-1]):
        filenames_list.append(filename[:-1])


rbprotocol.merge_postprocessed_data(filenames_list, dirname)