import structure_functions
import matplotlib.pyplot as plt
import numpy as np

file = 'numericalDataMerged/6Simulation/' 
txtname = 'merged_framepotentials.txt'

# file = 'numericalDataMerged/2Simulation/'
# txtname = 'merged_framepotentials.txt'
plotname = 'framepotentials_merged_1d_interacting.pdf'
shift_by_x = [0, 0, 0, 0, 0, 0]
shift_by_y = [-1., -2., -6., -24., -120., -720.]
structure_functions.plot_from_txtfile(file + txtname,
									   'depth',
									   'N',
									   showplot=False,
									   yscale='log',
									   gdesign_index=0,
									   boxplot=True,
									   addtolabel='1 moment',
									   plotcolor=10,
									   averagecolor=10,
									   shiftby_x=shift_by_x[0],
                                    #    shiftby_y=shift_by_y[0],
									#    variancetube=True,
									#    tubecolor=10
									   )
plt.plot([1, 125], [1,1], alpha=.5, color='black')
plt.plot([1, 125], [2,2], alpha=.5, color='black')

structure_functions.plot_from_txtfile(file + txtname,
									   'depth',
									   'N',
									   showplot=True,
									   yscale='log',
									   gdesign_index=1,
									   boxplot=True,
									   nonewfigure=True,
									   addtolabel='2 moment',
									   plotcolor=200,
									   averagecolor=200,
									   shiftby_x=shift_by_x[5],
                                    #    shiftby_y=shift_by_y[5],
									#    variancetube=True,
									#    tubecolor=200,
									   save_plot_in=file+plotname
									   )

