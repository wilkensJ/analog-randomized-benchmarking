import structure_functions as sf
import numpy as np
import os
import asrb
import rbprotocol
import pdb
from scipy.special import binom
from itertools import permutations

sf.plot_variance('numericalDataMerged/0Simulation/merged_framepotentials.txt', 'design', 'N', gdepth_index=0, showplot=True,
    # yscale='log'
    )