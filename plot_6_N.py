# from analogSimulatorBenchmarkingFunctions import *
import structure_functions
import matplotlib.pyplot as plt
import numpy as np

file = 'numericalDataMerged/8Simulation/' 
txtname = 'framepotentials.txt'
plotname = 'framepotentials_no_deviation.png'
shift_by_x = [-1.65, -1, -0.35, 0.35, 1, 1.65]
shift_by_y = np.array([-1., -2., -6., -24., -120., -720.])*10
structure_functions.plot_from_txtfile(file + txtname,
                                       'N',
                                       'depth',
                                       showplot=False,
                                       yscale='log',
                                       # xscale='log',
                                       # gN_index=0,
                                       gdesign_index=0,
                                       boxplot=True,
                                       addtolabel=', N=2',
                                       plotcolor=10,
                                       averagecolor=10,
                                       shiftby_x=shift_by_x[0],
                                       shiftby_y=shift_by_y[0],
                                    #    variancetube=True,
                                    #    tubecolor=10
                                       )
structure_functions.plot_from_txtfile(file + txtname,
									   'N',
									   'depth',
									   showplot=False,
									   yscale='log',
                              # xscale='log',
									   gdesign_index=1,
									   boxplot=True,
									   nonewfigure=True,
									   addtolabel=', 2 moment',
									   plotcolor=105,
									   averagecolor=105,
									   shiftby_x=shift_by_x[1],
                                    #    shiftby_y=shift_by_y[1],
									#    variancetube=True,
									#    tubecolor=105
									   )
# structure_functions.plot_from_txtfile(file + txtname,
# 									   'N',
# 									   'depth',
# 									   showplot=False,
									   # yscale='log',
                                       # xscale='log',
# 									   gdesign_index=2,
# 									   boxplot=True,
# 									   nonewfigure=True,
# 									   addtolabel=', 3 moment',
# 									   plotcolor=155,
# 									   averagecolor=155,
# 									   shiftby_x=shift_by_x[2],
#                                     #    shiftby_y=shift_by_y[2],
# 									#    variancetube=True,
# 									#    tubecolor=155
# 									   )
# structure_functions.plot_from_txtfile(file + txtname,
# 									   'N',
# 									   'depth',
# 									   showplot=False,
									   # yscale='log',
                                       # xscale='log',
# 									   gdesign_index=3,
# 									   boxplot=True,
# 									   nonewfigure=True,
# 									   addtolabel=', 4 moment',
# 									   plotcolor=155,
# 									   averagecolor=155,
# 									   shiftby_x=shift_by_x[3],
#                                     #    shiftby_y=shift_by_y[3],
# 									#    variancetube=True,
# 									#    tubecolor=155
# 									   )
# structure_functions.plot_from_txtfile(file + txtname,
# 									   'N',
# 									   'depth',
# 									   showplot=False,
									   # yscale='log',
                                       # xscale='log',
# 									   gdesign_index=4,
# 									   boxplot=True,
# 									   nonewfigure=True,
# 									   addtolabel=', 5 moment',
# 									   plotcolor=155,
# 									   averagecolor=155,
# 									   shiftby_x=shift_by_x[4],
#                                     #    shiftby_y=shift_by_y[4],
# 									#    variancetube=True,
# 									#    tubecolor=155
# 									   )
structure_functions.plot_from_txtfile(file + txtname,
									   'N',
									   'depth',
									   showplot=True,
									   yscale='log',
                                       # xscale='log',
									   gdesign_index=2,
									   boxplot=True,
									   nonewfigure=True,
									   addtolabel=', 3 moment',
									   plotcolor=200,
									   averagecolor=200,
									   shiftby_x=shift_by_x[2],
                                    #    shiftby_y=shift_by_y[5],
									#    variancetube=True,
									#    tubecolor=200,
									   # save_plot_in=file+plotname
									   )

