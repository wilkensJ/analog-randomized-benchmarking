import numpy as np
import matplotlib.pyplot as plt

def plot_several_boxplots(data, xaxis, **kwargs):
    """ Takes a two dimensional array (matrix) np.ndarray:data
    and plots every array in a boxplot for each point.
    Input:
        np.ndarray:data, shape(a, b),
        np.ndarray or list:xaxis, with len(xaxis)=a,
        
    """
    # Make a new figure only if the variable nonewfigure is False or
    # does not exist.
    if kwargs.get('nonewfigure'):
        print('No new figure.')
    else:
        plt.figure()
    # In case the opacity has to be set.
    alpha_plot = kwargs.get('alpha', .45)
    # Check, if boxplot is needed:

    # If yes, it is easier to store the plotted scattered data.
    # Since the boxplot version only makes sense, if  one variable is
    # repeated many times and the others are held constant, we just asume
    # that here.
    all_plot_data = []
    # Define a colormap and colors for average and median.
    infernofunc = plt.get_cmap("inferno")
    # For the boxplot thingies.
    plotcolor_number = kwargs.get('plotcolor', 100)
    plotcolor = infernofunc(plotcolor_number)
    # For the average line color.
    plotcolor_number = kwargs.get('averagecolor', 100)
    averagecolor = infernofunc(plotcolor_number)
    # For better visualization, the scattered values can be shifted even
    # thought they should be at the same x-axis point.
    if kwargs.get("shiftby_x"):
        shiftby_x = kwargs["shiftby_x"]
        width_marker = 70
    else:
        shiftby_x = 0
        width_marker = 400
    # To visualize the deviation from a wanted value, introduce the shiftby_y variable.
    shiftby_y = kwargs.get("shiftby_y", 0)
    # Values of the frame potential of one run of the simulation are plotted, all
    # parameters are the same for each simulation.
    for kk in range(len(xaxis)):
        plt.scatter(xaxis+shiftby_x, data[kk]+shiftby_y, s=width_marker, marker="_", linewidth=4,
                    alpha=alpha_plot, color=plotcolor)
    # # Calculate median of each singular value.
    # median = np.median(all_plot_data, axis=0)
    # Calculate average of each singular value
    average = np.average(data, axis=0)
    print(f"averages: {average}")
    # Plot the average with triangles and certain color.
    # Something can be added to the label with kwargs.
    addtolabel = kwargs.get('addtolabel', '')
    plt.scatter(xaxis+shiftby_x, average+shiftby_y, marker=4, s=60, color=averagecolor,
                alpha=.9, label=addtolabel)
    plt.plot(xaxis+shiftby_x, average+shiftby_y, "--", alpha=.9, color=averagecolor)
    # The yscale could be log for example.
    if kwargs.get("yscale"):
        plt.yscale(kwargs["yscale"])
    if kwargs.get("xscale"):
        plt.xscale(kwargs["xscale"])
    # Define the x-axis and y-axis labels.
    plt.xlabel(kwargs.get('gxlabel'))
    plt.ylabel(kwargs.get('gylabel'))
    plt.legend()
    # And the plot can be saved.
    if kwargs.get("save_plot_in"):
        plt.savefig(kwargs["save_plot_in"], format = "png")
    # For example via ssh showing plots is not possible.
    if kwargs.get("showplot"):
            plt.show()


gdata = np.arange(1,10,1).reshape(3,3)
gxaxis = np.array([1,5,10])

plot_several_boxplots(gdata,
                      gxaxis,
                      nonewfigure=False,
                      alpha=.45,
                      plotcolor=100,
                      averagecolor=100,
                      shiftby_x = 0,
                      shiftby_y = 0,
                      addtolabel=None,
                      yscale='linear',
                      xsxale='linear',
                      gxlabel='',
                      gylabel='',
                      save_plot_in='test.png',
                      showplot=False,)


gdata = np.arange(1,10,1).reshape(3,3)+10
gxaxis = np.array([1,5,10])

plot_several_boxplots(gdata,
                      gxaxis,
                      nonewfigure=True,
                      alpha=.45,
                      plotcolor=50,
                      averagecolor=50,
                      shiftby_x = 0,
                      shiftby_y = 0,
                      addtolabel=None,
                      yscale='linear',
                      xsxale='linear',
                      gxlabel='',
                      gylabel='',
                      save_plot_in='test.png',
                      showplot=True,)