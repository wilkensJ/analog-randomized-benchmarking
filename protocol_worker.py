from rbprotocol import *
import asrb
import numpy as np
import pdb


# Initiate the dictionary where parameters for the simulation can be set.
params = {}
# dimensions, number of nodes
dim = 3
# Important for character function
number_particles = 2
# Choose sequence lenghts.
sequences = 2**np.arange(1, 9, 1)
# How many times the same sequence lenght is applied and measured.
runs = 20
## Which character function should be used?
# 'character_function_young'
# 'character_function_schur'
params['characterfunction'] = 'character_function_schur'
## Set which simulation should be run.
# 'rb_protocol'
# 'character_average'
params['which_simulation'] = 'rb_protocol'
#############################################################################
############################## MEASUREMENT ##################################
#############################################################################
## What kind of measurement
# projective_measurement
params['basis_measurement'] = True
## Which density matrix
# default 0.
params['number_fock_vector_density_matrix'] = 0
# If a random density matrix is required. 
params['random_density_matrix'] = False
#############################################################################
############################## unitaries ##################################
#############################################################################
## unitary_func
# many_circuits
# many_haar_unitaries
# one_haar_unitary
params['unitary_func'] =  asrb.many_circuits
# params['unitary_func'] =  asrb.many_haar_unitaries
# Set the depth of the circuits.
params['depth'] = 30
## circuit_func
# unitaries_from_hamiltonians
params["circuit_func"] = asrb.unitaries_from_hamiltonians
#############################################################################
######################## If band diagonal is chosen ###########################
#############################################################################
## hamiltonian_func
# draw_1d_noninteracting
# draw_2d_noninteracting
# draw_1d_interacting
# draw_2d_interacting
params["hamiltonian_func"] = asrb.draw_1d_interacting
params["interacting"] = True
## distributions
# normal_distribution
# uniform_distribution
# repeated_distribution, set distribution=normal_disbtribution
# only_number, set fixed_value=1.
params["diag_func_hop"] = asrb.only_number
params["band_func_hop"] = asrb.normal_distribution
params["diag_func_int"] = asrb.only_number
## For drawing one entry and copying it to the whole diagonal/band for one matrix.
# random_entry_uniform
# random_entry_normal
# fixed_value
params["random_entry_normal"] = True
# params["low"] = -2
# params["high"] = 2
# This defines the mean for the normal distribution.
params["loc"] =  0.
# This defines the deviation of the normal distribution.
params["scale"] = 1.
#############################################################################
#############################################################################
#############################################################################
# Set the k-design value for calculating the frame potentials.
params["time"] = 1.
# Define how much smaller or bigger the band diagonal entries are than the
# diagonal entries.
params["bandweight"] = 1.
#############################################################################
########################## Actual simulation start #########################
#############################################################################

# run_kernel_simulation(dim, number_particles, [2, 4], runs, 'numericalData_rbprotocol/0Simulation/test1.txt', params)

# run_kernel_simulation(dim, number_particles, [6, 8], runs, 'numericalData_rbprotocol/0Simulation/test2.txt', params)

# postprocess_array, char_array, q_array, list_unitary_products = \
#     random_analog_bosonic_benchmarking_version2(dim, number_particles, sequences, runs, **params)

run_kernel_simulation_version2(
    dim, number_particles, sequences, runs, ['numericalData_rbprotocol/10Simulation/test2.txt'], params)
