import os
from pathlib import Path
import numpy as np
# import matplotlib.pyplot as plt
from scipy.stats import norm
# import matplotlib.mlab as mlab
from scipy.optimize import curve_fit
from scipy.stats import chisquare
from scipy.stats import chi2
from copy import deepcopy
import pdb

##############################################################################
######################### BEFORE running simulation #########################
##############################################################################

def create_dir(pathname='numericalData'):
    """ Finds a not yet existing directory, makes it and returns
    the name of it.
    The general dir name is "numericalData/"+str(count)+"Simulation/"
    where int:count has to be found.
    Output:
        str:dir_name
    """
    # Create the numericalData directory, if it does not exist yet.
    dir_name = Path(pathname)
    dir_name.mkdir(exist_ok=True)
    # Create a not yet existing directory to store the .h5 files in which
    # will be created during the computation of the recovery algorithm.
    # count will get bigger by one if the directory already exists.
    count = 0
    # Searches for directories, starts with
    # 1Simulation/ then 2Simulation/,...
    # until a not existing directory is found
    while os.path.exists(dir_name / f'{count}Simulation/'):
        count += 1
    # Create the directory name
    dir_name = dir_name / f'{count}Simulation/'
    # Create directory itself
    os.mkdir(dir_name)
    return dir_name


def write_to_txt(filename, *args, **kwargs):
    """ This function writes arbitrary amount of dictionaries into a .txt file
    Input:
        str:filename  the directory in which the dictionaries are written
        *args          the dictionaries are stored here
    """
    with open(filename, "a") as file:
        for dictionary in args:
            if type(dictionary) == dict and dictionary:
                for key in dictionary:
                    # Check if item is a function. Then only store the name. 
                    if callable(dictionary[key]):
                        file.write(
                            f'{key} : {dictionary[key].__name__}\n')
                    else:
                        file.write(f'{key} : {dictionary[key]}\n')
        for key in kwargs:
                # Check if item is a function. 
                if callable(kwargs[key]):
                    file.write(
                        f'{key} : {kwargs[key].__name__}\n')
                else:
                    file.write(f'{key} : {kwargs[key]}\n')


def write_labbook(filename, **kwargs):
    """  Checks what elements **kwargs has and according to the type stores
    the important information into the given file.
    Input:
        str:filename, the .txt file name.
    """
    # Initialize a new dictionary to store the compromised information in.
    small_dict = {}
    # Iterate over all items in args.
    # Check which type the item is and store accordingly the compromised
    # information.
    for key in kwargs:
        # Store the value of the dictionary into a variable:item.
        item = kwargs[key]
        # Check what type the item has.
        if type(item) == str:
            # Just copy the entry from dict:kwargs into dict:small_dict.
            small_dict[key] = item
        elif type(item) == list or type(item) == np.ndarray:
            # Check the depth, if there is only one entry this entry can
            # just be stored as one entry.
            if len(item) == 1:
                small_dict[key + " only entry"] = item[0]
            else:
                # Store the first element, the last element and the depth.
                small_dict[key + "[0]"] = item[0]
                small_dict[key + "[-1]"] = item[-1]
                small_dict[f"len({key})"] = len(item)
    # Now write all gathered information into the .txt file.
    write_to_txt(filename, small_dict)


def make_head_txtfile(filename, order_list, **kwargs):
    """ Saves the given dictionary as comments to a .txt file.
    Input:
        str:filename
        **kwargs, values which are written into file.
    """
    # Initiate a list to keep track of the order if the items.
    keeptrack_list = []
    # Initiate a list where the lines will be stored in.
    lines_list = []
    # To make counting available and store the progress in for loop.
    count = 0
    # Write for every dictionary entry in kwargs a new
    # comment line into file.
    for key in kwargs:
        # First check with index the key has in order_list and append it.
        keeptrack_list.append(order_list.index(f"{key[:-3]}_list"))
        # Create the line which will be stored in the file.
        lines_list.append(f"# {key} : {kwargs[key]}\n")
        count += 1
    # Open file in appending mode.
    with open(filename, "a") as file:
        # Iterate over the indices in the list:keeptrack_list and write the
        # lines into the file.
        for index in keeptrack_list:
            file.write(lines_list[index])

##############################################################################
######################### WHILE running simulation #########################
##############################################################################


def arrays_to_txtfile(filename, *garrays):
    """ Saves arrays into a .txt files.
    Input:
        str:filename,
        *garrays, all arrays which have to be stored
    """
    # Open file in appending mode.
    with open(filename, "a") as file:
        # Write the arrays under each other in columns.
        for array in garrays:
            np.savetxt(file, array)


def data_to_txtfile(filename, gValue, *args, **kwargs):
    """ Saves one value to given .txt file in columns.
    Input:
        str:filename, the file in which the value should be stored.
        float:gValue, the value which should be stored.
    """
    # Open file in appending mode.
    with open(filename, "a") as file:
        # Write the value in string format, append a new column.
        file.write(f"{gValue}\n")

##############################################################################
######################### AFTER running simulation #########################
##############################################################################

def define_indexing(*args, **kwargs):
    """ Check what variables are in args and if an index for a variable
    is given in dict:kwargs.
    Then set the right indices
    """
    # Initialize a list where all indices are stored in.
    store_indices = []
    ##########################################################
    # For amount of depth the frame potential is averaged over.
    ##########################################################
    if "depth" in args and "gdepth_index" not in kwargs:
        # Set the starting in ending index as whole range.
        depth_indices = slice(0, None)
    else:
        # Check if in dict:kwargs something is given.
        if "gdepth_index" in kwargs:
            depth_indices = kwargs["gdepth_index"]
        else:
            depth_indices = 0
    store_indices.append(depth_indices)
    #####################################
    # For dimension of Hamiltonians "dim".
    #####################################
    if "dim" in args:
        # Set the starting in ending index as whole range.
        dim_indices = slice(0, None)
    else:
        # Check if in dict:kwargs something is given.
        if "gdim_index" in kwargs:
            dim_indices = kwargs["gdim_index"]
        else:
            dim_indices = 0
    store_indices.append(dim_indices)
    ################################
    # For amount of Hamiltonians "N".
    ################################
    if "N" in args and "gN_index" not in kwargs:
        # Set the starting in ending index as whole range.
        N_indices = slice(0, None)
    else:
        # Check if in dict:kwargs something is given.
        if "gN_index" in kwargs:
            N_indices = kwargs["gN_index"]
        else:
            N_indices = 0
    store_indices.append(N_indices)
    ######################################
    # For design .
    ######################################
    if "design" in args:
        # Set the starting in ending index as whole range.
        design_indices = slice(0, None)
    else:
        # Check if in dict:kwargs something is given.
        if "gdesign_index" in kwargs:
            design_indices = kwargs["gdesign_index"]
        else:
            design_indices = 0
    store_indices.append(design_indices)
    # In the end return all the gathered single indices inside a tuple. 
    # They must be in the right order thought! depth, dim, t, N.
    return tuple(store_indices)


def extract_information(filename, *args, **kwargs):
    """ This function takes the file in which the parameters are stored and
    stores them in a dictionary which can be returned and used in other
    functions.
    """
    # Open the file in read mode and extract the comments.
    with open(filename, "r") as file:
        # The first line gives the order of the variable lists.
        firstline = file.readline()[9:-2]
        # Store the order which is necessary for the plot to be correct.
        order_list = firstline.split(", ")
        # Initialize a dictionary to store the dimension.
        pa_dict = {}
        # To count the amount of commented lines, initialize int:count.
        count = 1
        # To make indexing easier, initialize the index value.
        index = 0
        # Only read the comment lines. Use while loop to break
        # if a line is not commented any more.
        while True:
            comment = file.readline()
            # Break if the line starts not with "#".
            if not comment.startswith("#"):
                break
            # Extract the dimension and number from the comment line.
            (key, val) = comment.split(" : ")
            # The "# " has to be removed from the key variable.
            # And the \n has to be removed from the val variable.
            key, val = key[2:], val[:-1]
            # Store the values in the dictionary.
            pa_dict[key] = int(val)
            # Raise the index value by the depth of the variables list.
            index += int(val)
            # Store it in the dictionary
            pa_dict[f"{key[:-3]}index"] = index
            # Raise int:count value.
            count += 1
        # Now import all parameters data in one array.
        data = np.loadtxt(filename,
            skiprows=count)[:pa_dict["designindex"]]
        # Extract the single lists. The order is: depth, dim, t, N.
        pa_dict["depth_list"] = data[:pa_dict["depthindex"]]
        pa_dict["dim_list"] = data[
            pa_dict["depthindex"]:pa_dict["dimindex"]]
        pa_dict["N_list"] = data[pa_dict["dimindex"]:pa_dict["Nindex"]]
        pa_dict["design_list"] = data[
            pa_dict["Nindex"]:pa_dict["designindex"]]
    return pa_dict, order_list, count


def gauss_func(x, mu, sigma):
    """ Given an array x and constants a,sigma,mu and y0
    this function calculated the gaussian function
    for that array
    """
    return (1/(sigma*np.sqrt(2*np.pi)))*np.exp(-(x-mu)**2/(2*sigma**2))


def not_completed_runs_merge(filename_list_to_write_to, filename_list_from_and_delete, *args, **kwargs):
    """ Sometimes numerical simulations have to be aborted. Then unfinishes data
    points are flying around. With this function they can be appended to each other
    if the aborted run is continued in other files.
    """
    # First check if they have the same lenght.
    if len(filename_list_to_write_to) != len(filename_list_from_and_delete):
        print('not the same lenghts of given lists, abort')
        return ''
    # If yes, take the data from the second files given and write them to the first file.
    for count in range(len(filename_list_from_and_delete)):
        # Get the pairs of filenames.
        filename_from = f'{filename_list_from_and_delete[count]}/framepotentials.txt'
        filename_to = f'{filename_list_to_write_to[count]}/framepotentials.txt'
        # Get the information.
        # Extract the depth and indices of the parameters arrays.
        pa_dict, _, count_to_skip = extract_information(filename_from)
        # Get the number to skip the arrays indicating the parameters.
        skip = pa_dict['depthlen'] + pa_dict['dimlen'] +pa_dict['Nlen'] + pa_dict['designlen']
        # Open the file to get the data.
        with open(filename_from, 'r') as file:
            data_to_append = np.loadtxt(file, skiprows=count_to_skip+skip)
        # Finally write the data as flattened array.
        arrays_to_txtfile(filename_to, data_to_append)
        os.remove(filename_from)
        os.rmdir(filename_list_from_and_delete[count])
        print(filename_list_from_and_delete[count], ' written to ', filename_list_to_write_to[count], ' and deleted')


def merge_same_runs(filename_list, repeated_entry, *args, **kwargs):
    """ If there are simulations with the same paramters in different files they 
    can be merged with this function to one file.
    """
    # Define the new path file name.
    new_path = create_dir(pathname='numericalDataMerged')
    new_filename = f'{new_path}/merged_framepotentials.txt'
    # Write to the labBook.txt file in the new path.
    write_labbook('numericalDataMerged/labBook.txt', new_file_path=new_filename, merged_from=filename_list)
    write_to_txt('numericalDataMerged/labBook.txt', {'\n' : '\n'})
    # Initiate the dictionry where the paramters are merged.
    all_pa_dict = {}
    # Initiate the array where the all the data is stored.
    all_fp_data = np.array([])
    # Go through the files in the list.
    for filename in filename_list:
        # Extract the depth and indices of the parameters arrays.
        pa_dict, order_list, count = extract_information(filename)
        # Check if the dicionary is empty. If yes, just copy the existing pa_dict.
        if not all_pa_dict:
            all_pa_dict = deepcopy(pa_dict)
        else:
            #  Append the paramter lists.
            rep_entry_list = f'{repeated_entry}_list'
            all_pa_dict[rep_entry_list] = np.append(all_pa_dict[rep_entry_list], pa_dict[rep_entry_list])
        # Open the file in read mode and extract the comments.
        with open(filename, "r") as file:
            alldata = np.loadtxt(file, skiprows=count)
        # The data is arrange as the order_list. If, for example, the last entry in
        # order_list is design and the second last is N, the data is for example shaped as
        # N = 10, design = 1 : fp value
        # N = 10, design = 2 : fp value
        # N = 20, design = 1 : fp value
        # N = 20, design = 2 : fp value .
        # Reshape the given data which starts when the variable lists ends,
        # for example if design is the last variable, start with "designindex".
        last_index = f'{order_list[-1][:-5]}index'
        fp_data = alldata[pa_dict[last_index]:].reshape(pa_dict["depthlen"],
            pa_dict["dimlen"], pa_dict["Nlen"],pa_dict["designlen"])
        # If all_fp_data is empty (in the first run), just copy the fp_data
        # array. Else the concatenate function is not working.
        if len(all_fp_data) == 0:
            all_fp_data = deepcopy(fp_data)
        else:
            # Append the data in the right order, meaning the last paramters axis which is the
            # first axis.
            all_fp_data = np.concatenate((all_fp_data, fp_data), axis=1)
    # Write the order list.
    order_string = '# Order: depth_list, dim_list, N_list, design_list \n'
    with open(new_filename, 'a') as file:
        file.write(order_string)
    # Write the head of the .txt file.
    make_head_txtfile(new_filename, order_list, depthlen=len(all_pa_dict['depth_list']),
            dimlen=len(all_pa_dict['dim_list']), Nlen=len(all_pa_dict['N_list']), designlen=len(all_pa_dict['design_list']))
    for item in order_list:
        arrays_to_txtfile(new_filename, all_pa_dict[item])
    # Finally write the merged data as flattened array.
    arrays_to_txtfile(new_filename, all_fp_data.flatten())
    # Return the new filename!
    return new_filename
   

##############################################################################
################################### PLOT ###################################
##############################################################################

def plot_from_txtfile(filename, x_axis, third_axis, **kwargs):
    """ With this function the data stored in the given file are
    first imported, reshaped and then the desired values are
    plotted. The third axis gives different graphs which are
    overlaying in one plot.
    Input:
        str:filename,
        str:x_axis, either "N", "dim", "design", "depth". Defines
                        which variables is displayed on the xAxis.
        str:third_axis, either "N", "dim", "design", "depth", but not the
                       same as str:x_axis. Defines which
                       variables is altered, too, and displayed in
                       one figure.
    """
    # Extract the depth and indices of the parameters arrays.
    pa_dict, order_list, count = extract_information(filename)
    # Open the file in read mode and extract the comments.
    with open(filename, "r") as file:
        alldata = np.loadtxt(file, skiprows=count)
    # Reshape the given data which starts when the variable lists ends,
    # for example if design is the last variable, start with "designindex".
    last_index = f'{order_list[-1][:-5]}index'
    fp_data = alldata[pa_dict[last_index]:].reshape(pa_dict["depthlen"],
        pa_dict["dimlen"], pa_dict["Nlen"],pa_dict["designlen"])
    # Gather the right indices.
    all_indices = define_indexing(x_axis, third_axis, **kwargs)
    # Index the data according to the start and end indices which where
    # just chosen.
    fp_data = fp_data[all_indices]
    # Check, if x_axis is in order before or after third_axis.
    # If so, the axis in np.ndarray:fp_data have to be switched.
    if order_list.index(f"{x_axis}_list") < \
        order_list.index(f"{third_axis}_list"):
        fp_data = fp_data.T
    # Make a new figure, only  if the variable nonewfigure is False or
    # does not exist.
    if kwargs.get('nonewfigure'):
        print('No new figure.')
    else:
        plt.figure()
    # In case the opacity has to be set.
    alpha_plot = kwargs.get('alpha', 1.0)
    # Calculate the x values.
    if kwargs.get('gxarray'):
        xarray = kwargs['gxarray']
    else:
        xarray = pa_dict[f"{x_axis}_list"][all_indices[
            order_list.index(f"{x_axis}_list")]]
    # Check, if boxplot is needed:
    if kwargs.get("boxplot"):
        # If yes, it is easier to store the plotted scattered data.
        # Since the boxplot version only makes sense, if  one variable is
        # repeated many times and the others are held constant, we just asume
        # that here.
        all_plot_data = []
        # Define a colormap and colors for average and median.
        infernofunc = plt.get_cmap("inferno")
        # For the boxplot thingies.
        plotcolor_number = kwargs.get('plotcolor', 100)
        plotcolor = infernofunc(plotcolor_number)
        # For the average line color.
        plotcolor_number = kwargs.get('averagecolor', 100)
        averagecolor = infernofunc(plotcolor_number)
        # For better visualization, the scattered values can be shifted even
        # thought they should be at the same x-axis point.
        if kwargs.get("shiftby_x") != None:
            shiftby_x = kwargs["shiftby_x"]
            width_marker = 70
        else:
            shiftby_x = 0
            width_marker = 400
        # To visualize the deviation from a wanted value, introduce the shiftby_y variable.
        shiftby_y = kwargs.get("shiftby_y", 0)
        # Values of the frame potential of one run of the simulation are plotted, all
        # parameters are the same for each simulation.
        for kk in range(pa_dict[third_axis + "len"]):
            plt.scatter(xarray+shiftby_x, fp_data[kk]+shiftby_y, s=width_marker, marker="_", linewidth=4,
                        alpha=alpha_plot - .55, color=plotcolor)
            # Store the right now plotted data.
            all_plot_data.append(fp_data[kk])
        # Reshape it to calculate the median and average of each singular value.
        all_plot_data = np.array(all_plot_data).reshape(
            pa_dict[f"{third_axis}len"], len(fp_data[0]))
        # # Calculate median of each singular value.
        # median = np.median(all_plot_data, axis=0)
        # Calculate average of each singular value
        average = np.average(all_plot_data, axis=0)
        print(f"averages: {average}")
        # Check if the variance of the frame potentials should be plotted, too.
        if kwargs.get("variancetube"):
            # Initiate the array where the variances are stored.
            variances = np.zeros(pa_dict[f"{x_axis}len"])
            # Iterate over the x axis and calculate the variance.
            for kk in range(pa_dict[f"{x_axis}len"]):
                # Take the kk'th row of data
                data_for_hist = fp_data[:,kk]
                # Calculate the histogram and fit a gauss.
                # Return the fitting variables.
                popt, _ = hist_gaussfit(data_for_hist,
                                           show_hist=False ,**kwargs)
                variances[kk] = popt[1]
            # Calculate the upper and lower bound.
            upperBound = average + variances
            lowerBound = average - variances
            # Set the tubecolor.
            plotcolor_number = kwargs.get("tubecolor", 140)
            tubecolor = infernofunc(plotcolor_number)
            plt.plot(xarray+shiftby_x, upperBound+shiftby_y, "--", color=tubecolor)
            plt.plot(xarray+shiftby_x, lowerBound+shiftby_y, "--", color=tubecolor)
                     #label="Variance tube")
            print(variances)
        # Plot the average with triangles and certain color.
        # Something can be added to the label with kwargs.
        addtolabel = kwargs.get('addtolabel', '')
        if kwargs.get('nolabel'):
            plt.scatter(xarray+shiftby_x, average+shiftby_y, marker=4, s=60, color=averagecolor,
                        alpha=alpha_plot - (1. - alpha_plot))
        else:
            plt.scatter(xarray+shiftby_x, average+shiftby_y, marker=4, s=60, color=averagecolor,
                        alpha=alpha_plot-(1.-alpha_plot),
                        label=
                        # third_axis + " " \
                        # + str(pa_dict[third_axis + "_list"][0]) +
                        addtolabel)
        plt.plot(xarray+shiftby_x, average+shiftby_y, "--", alpha=alpha_plot - (1. - alpha_plot),
                 color=averagecolor)
    else:
        # Go through the arrays in np.ndarray:fp_data and plot them one by one.
        if kwargs.get('nolabel'):
            for kk in range(pa_dict[f"{third_axis}len"]):
                plt.plot(xarray, fp_data[kk], alpha=alpha_plot)
        else:
            for kk in range(pa_dict[f"{third_axis}len"]):
                plt.plot(xarray, fp_data[kk], alpha=alpha_plot,
                         label=f"{third_axis}:" \
                         + str(pa_dict[f"{third_axis}_list"][kk]))
    # The yscale could be log for example.
    if kwargs.get("yscale"):
        plt.yscale(kwargs["yscale"])
    if kwargs.get("xscale"):
        plt.xscale(kwargs["xscale"])
    # Define the x-axis.
    xLabel = kwargs.get('gxLabel', x_axis)
    plt.xlabel(xLabel)
    plt.ylabel("frame potential")
    plt.legend()
    # And the plot can be saved.
    if kwargs.get("save_plot_in"):
        plt.savefig(kwargs["save_plot_in"], format = "pdf")
    # For example via ssh showing plots is not possible.
    if kwargs.get("showplot"):
            plt.show()
    # An histogram can be done for one value of N.
    if kwargs.get("histogram"):
        # Check if a value for N is given, if not set it to -1.
        indN = kwargs.get("hist_for_index_N", -1)
        # Take the indN'th row of data
        data_for_hist = fp_data[:,indN]
        usedN = pa_dict[f"{x_axis}_list"][indN]
        # Make the histogram.
        popt, pcov = hist_gaussfit(data_for_hist, N=usedN, **kwargs)


def plot_variance(filename, x_axis, third_axis, scaled_value=False, **kwargs):
    """ With this function the data stored in the given file are
    first imported, reshaped and then the desired values are
    plotted. The third axis gives different graphs which are
    overlaying in one plot.
    Input:
        str:filename,
        str:x_axis, either "N", "dim", "design", "depth". Defines
                        which variables is displayed on the xAxis.
    """
    # Extract the depth and indices of the parameters arrays.
    pa_dict, order_list, count = extract_information(filename)
    # Open the file in read mode and extract the comments.
    with open(filename, "r") as file:
        alldata = np.loadtxt(file, skiprows=count)
    # Reshape the given data which starts when the variable lists ends,
    # since N is the last variable, start with "N_index".
    fp_data_original = alldata[pa_dict["designindex"]:].reshape(pa_dict["depthlen"],
        pa_dict["dimlen"], pa_dict["Nlen"],pa_dict["designlen"])
    all_variances = []
    # Go through the different scaled_value given.
    # pdb.set_trace()
    if scaled_value == 'N' and x_axis == 'design':
        for count_N in range(pa_dict["Nlen"]):
            # Gather the right indices.
            all_indices = define_indexing(x_axis, third_axis, gN_index=count_N)
            xarray = pa_dict[f"{x_axis}_list"][all_indices[
                order_list.index(f"{x_axis}_list")]]
            # Index the data according to the start and end indices which where
            # just chosen.
            fp_data = fp_data_original[all_indices]
            variances = np.zeros(pa_dict[f"{x_axis}len"])
            # Iterate over the x axis and calculate the variance.
            for kk in range(pa_dict[f"{x_axis}len"]):
                factorial_value = np.math.factorial(int(pa_dict[f"{x_axis}_list"][kk]))
                # Take the kk'th row of data
                data_for_hist = fp_data[:,kk]
                # Calculate the histogram and fit a gauss.
                # Return the fitting variables.
                popt, _ = hist_gaussfit(data_for_hist,
                    show_hist=False, gaverage_value=factorial_value, **kwargs)
                variances[kk] = popt[1]
            all_variances.append(variances)
            plt.plot(xarray[count_N], variances[count_N], "x", label=f'N: {pa_dict["N_list"][count_N]}')
    elif not scaled_value and kwargs.get('gdesign_index') != None:
        # Gather the right indices.
        all_indices = define_indexing(x_axis, third_axis, **kwargs)
        xarray = pa_dict[f"{x_axis}_list"][all_indices[
            order_list.index(f"{x_axis}_list")]]
        # Index the data according to the start and end indices which where
        # just chosen.
        fp_data = fp_data_original[all_indices]
        variances = np.zeros(pa_dict[f"{x_axis}len"])
        # Iterate over the x axis and calculate the variance.
        for kk in range(pa_dict[f"{x_axis}len"]):
            factorial_value = np.math.factorial(int(pa_dict['design_list'][kwargs['gdesign_index']]))
            # Take the kk'th row of data
            data_for_hist = fp_data[:,kk]
            # Calculate the histogram and fit a gauss.
            # Return the fitting variables.
            popt, _ = hist_gaussfit(data_for_hist,
                show_hist=False, gaverage_value=factorial_value, **kwargs)
            variances[kk] = popt[1]
        all_variances.append(variances)
        for_label = pa_dict['design_list'][kwargs['gdesign_index']]
        plt.plot(xarray, variances, "x--", label=f'moment: {for_label}')
    elif x_axis == 'design' and third_axis == 'N':
        # Gather the right indices.
        all_indices = define_indexing(x_axis, third_axis, **kwargs)
        xarray = pa_dict[f"{x_axis}_list"][all_indices[
            order_list.index(f"{x_axis}_list")]]
        # Index the data according to the start and end indices which where
        # just chosen.
        fp_data = fp_data_original[all_indices]
        variances = np.zeros(pa_dict[f"{x_axis}len"])
        # Iterate over the x axis and calculate the variance.
        for kk in range(pa_dict[f"{x_axis}len"]):
            factorial_value = np.math.factorial(int(pa_dict[f'{x_axis}_list'][kk]))
            # Take the kk'th row of data
            data_for_hist = fp_data[:,kk]
            # Calculate the histogram and fit a gauss.
            # Return the fitting variables.
            popt, _ = hist_gaussfit(data_for_hist,
                show_hist=False, gaverage_value=factorial_value, **kwargs)
            variances[kk] = popt[1]
        all_variances.append(variances)
        for_label = pa_dict['N_list'][0]
        plt.plot(xarray, variances, "x--", label=f'N: {for_label}')
        plt.plot(xarray, 9**(xarray*1.21-14))

    print(all_variances)
    # The yscale could be log for example.
    if kwargs.get("yscale"):
        plt.yscale(kwargs["yscale"])
    if kwargs.get("xscale"):
        plt.xscale(kwargs["xscale"])
    # Define the x-axis.
    if kwargs.get('gxLabel'):
        xLabel = kwargs['gxLabel']
    else:
        xLabel = x_axis
    plt.xlabel(xLabel)
    plt.ylabel("variance")
    plt.legend()
    # And the plot can be saved.
    if kwargs.get("save_plot_in" ):
        plt.savefig(kwargs["save_plot_in"], format="png")
    # For example via ssh showing plots is not possible.
    if kwargs.get("showplot"):
        plt.show()
    # An histogram can be done for one value of N.
    if kwargs.get("histogram"):
        # Check if a value for N is given, if not set it to -1.
        indN = kwargs.get("hist_for_index_N", -1)
        # Take the indN'th row of data
        data_for_hist = fp_data[:,indN]
        usedN = pa_dict[f"{x_axis}_list"][indN]
        # Make the histogram.
        popt, pcov = hist_gaussfit(data_for_hist, N=usedN, **kwargs)


def singvalues_plot(filename_info, filename_sv, which_singvalues_list,
                    plot_this, *args, change_of_basis=False, **kwargs):
    """
    This function takes the stored singular values of the second moment of
    a set of matrices and displays the first, second, third, ... singular
    value, possibly depending on a parameter such as depth (depth) of circuit.
    If "boxplot" is given as True in **kwargs, the mean and average value is
    calculated and added. Only useful if parameters are the same for
    every data set.
    If "histogram" is given as True in **kwargs, a histogram of the third
    singular values is done with a Gaussian fit of the data. Again only useful,
    if parameters don't change for all runs.
    Input:
        str:filename_info, the file in which all parameters are stored.
        str:filename_sv,      the file in which the singular values are stored.
        which_singvalues_list, a list or array where indices of the singular
                             values are listed which will be shown in
                             different histograms.
        str:plot_this,        sets what parameters differs on the third axis.
    """
    # Extract the information needed.
    pa_dict, order_list, _ = extract_information(filename_info)
    # Get the data from the gap file.
    alldata = np.loadtxt(filename_sv)
    # The list:dim_list must have only one entry!
    if pa_dict["dimlen"] != 1:
        print("dim list has to have only one entry! Abort.")
        return None
    if change_of_basis:
        new_dim_list = [0, 1, 3, 6, 10]
        dim = new_dim_list[int(pa_dict["dim_list"][0])]
    else:
        # If dim_len is one, proceed. Then for every ensemble of parameters, there
        # are dim**4 many entries of singular values (because the formula take
        # for times the tensor product of the unitary.)
        dim = int(pa_dict["dim_list"][0])
    # Reshape the alldata vector.
    singvalue_data = alldata.reshape(pa_dict["depthlen"], pa_dict["dimlen"],
                                     pa_dict["Nlen"], pa_dict["designlen"],
                                     dim**(2*int(pa_dict['design_list'][0])))
    # Extract the wanted indices.
    indices_list = list(define_indexing(plot_this, **kwargs))
    # Append the last axis since it is not one but int:dim**4
    indices_list.append(slice(0, None))
    # Slice the data right.
    plot_data = singvalue_data[tuple(indices_list)][:, which_singvalues_list]
    # Make a new figure.
    plt.figure()
    # Check, if the boxplot version has to be plotted.
    if kwargs.get("boxplot"):
        # If yes, it is easier to store the plotted scattered data.
        # Since the boxplot version only makes sense, if  one variable is
        # repeated many times and the others are held constant, we just asume
        # that here.
        all_plot_data = []
        # Go through the single vectors, each vector contains the singular
        # values of the moment operator of one run of the simulation, all
        # parameters are the same for each simulation.
        for kk in range(pa_dict[f"{plot_this}len"]):
            plt.scatter(which_singvalues_list, plot_data[kk], s=400, 
                        marker="_", linewidth=4, alpha=.4)
            # Store the right now plotted data.
            all_plot_data.append(plot_data[kk])
        # Reshape it to calculate the median and average of
        # each singular value.
        all_plot_data = np.array(all_plot_data).reshape(
            pa_dict[f"{plot_this}len"], len(plot_data[0]))
        # # Calculate median of each singular value.
        # median = np.median(all_plot_data, axis=0)
        # Calculate average of each singular value
        average = np.average(all_plot_data, axis=0)
        # Define a colormap and colors for average and median.
        infernofunc = plt.get_cmap("inferno")
        averagecolor = infernofunc(100)
        median_color  = infernofunc(180)
        # Plot the average with triangles and certain color.
        plt.scatter(which_singvalues_list, average, marker=4, s=60, 
                    color=averagecolor,
                    label=f"average for " + plot_this + " " + \
                    str(pa_dict[plot_this + "_list"][0]))
    else:
        for kk in range(pa_dict[f"{plot_this}len"]):
            plt.scatter(which_singvalues_list, plot_data[kk], s=400,
                marker="_", linewidth=4, alpha=.4,
                label=plot_this +":" + \
                str(pa_dict[plot_this + "_list"][kk]))
    plt.xlabel("Nr. Singular value")
    plt.ylabel("Magnitude")
    plt.legend()
    # And the plot can be saved.
    if "save_plot_in" in kwargs:
        plt.savefig(kwargs["save_plot_in"] , format="png")
    # For example via ssh showing plots is not possible.
    if "showplot" in kwargs:
        if kwargs["showplot"]:
            plt.show()
    # Option of histogram is checked.
    if kwargs.get("histogram"):
        # The second singular value is plotted in a histogram,
        # only useful if the parameters stay the same, useful for chi-squared
        # test.
        data_third_sv = plot_data[:,2]
        # Do the histogram and fitting
        _, _ = hist_gaussfit(data_third_sv, *args, **kwargs)
       

def hist_gaussfit(gdata,*args,**kwargs):
    """
    Build a histogram out of the given data and fit it with a Gaussian.
    """
    from_this_residual = kwargs.get('gaverage_value', np.average(gdata))
    # Calculate the average.
    # Now get the residuals, meaning the deviations from the average value.
    residuals = gdata - from_this_residual
    # Make a histogram. Store the counts (y-axis), bins (x-axis) and amount
    # of used bins, bars.
    plt.figure()
    # To fit a probability distribution, the histogram has to be normalized.
    counts, bins, bars = plt.hist(residuals, density=True)
    # Calculate how wide one bin is.
    delta_bin = bins[1]-bins[0]
    # print("degrees of freedom without schaetzen: " + str(len(counts)))
    # Get a good estimate for the data
    (mu, sigma) = norm.fit(residuals)
    # Fit the data with a gaussian function, defined here (gauss_func).
    # y0 is not a fit parameter, meaning it is technically 0.
    # The bins have to be shifted half a bin, since the histogram orders
    # the values in one interval, meaning that there is one bin more than
    # counts.
    xpoints = bins[:-1]+delta_bin/2.
    popt, pcov = curve_fit(gauss_func, xpoints, counts, p0=(mu, sigma))
    # Make the fitting x-axis more precise for plotting
    xfit = np.arange(bins[0],bins[-1],delta_bin/5.)
    # Calculate the data with the parameter calculated by the curve_fit
    # function and the x-axis which is more precise.
    yfit = gauss_func(xfit, *popt)
    # Now a chi-squared test can be done to check, if the given data
    # really obeys an gaussian distribution
    if kwargs.get("chisquared_test"):
        # counts is our observed data, the y points of the gaussian function
        # with the fitting parameters is our expected data and since we had
        # to estimate two parameters for the gaussian fit ddof is set to 2
        # and the overall degrees of freedom are then:
        # amount of counts - 1 - ddof (here 2)
        dof = len(counts)-1-3
        chisq, p = chisquare(counts, gauss_func(xpoints, *popt), ddof=3)
        # Calculate the critical chi-squared value for a certain probability
        probability = .95
        critical = chi2.ppf(probability, dof)
        print("Calculated X**2 for data with hypothesis gauss distribution: "
            + str(chisq)
            + ", \n critical value under which hypothesis is accepted: "
            + str(critical))
        print("And the standard deviation = " + str(popt[1]))
    # Plot the fitted data into the histogram.
    plt.plot(xfit, yfit,'r--',linewidth=2, label="gaussian fit")
    plt.grid(True)
    plt.xlabel("frame potential value")
    plt.ylabel("amount")
    plt.legend()
    if kwargs.get("N") != None:
        plt.title(
            "Normalized histogram of residuals for N = " + str(kwargs["N"]))
    else:
        plt.title("Normalized histogram of residuals")
    # And the plot can be saved.
    if "save_hist" in kwargs:
        plt.savefig(kwargs["save_hist"] , format = "png")
    # For example via ssh showing plots is not possible.
    if kwargs.get("show_hist"):
        plt.show()
    else:
        # Have to delete the figure, else it will pop up when another
        # show() appears!
        plt.close()
    # Return the fitting parameters.
    return popt, pcov

