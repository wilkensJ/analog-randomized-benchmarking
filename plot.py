# from analogSimulatorBenchmarkingFunctions import *
import structure_functions

file = 'numericalData/9Simulation/' 
txtname = 'framepotentials.txt'
plotname = 'framepotentials.png'
shift_by = [-10, -3.5, 3.5, 10]
shift_by = [-1, -0.35, 0.35, 1]
structure_functions.plot_from_txtfile(file + txtname,
									   'depth',
									   'N',
									   showplot=False,
									   yscale='log',
									   gdesign_index=0,
									   boxplot=True,
									   addtolabel=', 1 design',
									   plotcolor=10,
									   averagecolor=10,
									   shiftby=shift_by[0],
									#    variancetube=True,
									#    tubecolor=10
									   )
structure_functions.plot_from_txtfile(file + txtname,
									   'depth',
									   'N',
									   showplot=False,
									   yscale='log',
									   gdesign_index=1,
									   boxplot=True,
									   nonewfigure=True,
									   addtolabel=', 2 design',
									   plotcolor=105,
									   averagecolor=105,
									   shiftby=shift_by[1],
									#    variancetube=True,
									#    tubecolor=105
									   )
structure_functions.plot_from_txtfile(file + txtname,
									   'depth',
									   'N',
									   showplot=False,
									   yscale='log',
									   gdesign_index=2,
									   boxplot=True,
									   nonewfigure=True,
									   addtolabel=', 3 design',
									   plotcolor=155,
									   averagecolor=155,
									   shiftby=shift_by[2],
									#    variancetube=True,
									#    tubecolor=155
									   )
structure_functions.plot_from_txtfile(file + txtname,
									   'depth',
									   'N',
									   showplot=False,
									   yscale='log',
									   gdesign_index=3,
									   boxplot=True,
									   nonewfigure=True,
									   addtolabel=', 4 design',
									   plotcolor=200,
									   averagecolor=200,
									   shiftby=shift_by[3],
									#    variancetube=True,
									#    tubecolor=200,
									   save_plot_in=file+plotname
									   )
