# from asrb import *
import structure_functions
file = 'numericalData/24Simulation/' 
txtname = 'framepotentials.txt'
plotname = 'variance.png'

structure_functions.plot_variance(file+txtname,
									   'N',
									   'depth',
									   scaled_value=False,
									   gdesign_index=0,
									#    showplot = True,
									   yscale = 'log',
									#    boxplot = True,
									#    gdesign_index=0,
									#    gN_index=0
									#    save_plot_in=file+plotname
									   )
structure_functions.plot_variance(file+txtname,
									   'N',
									   'depth',
									   scaled_value=False,
									   gdesign_index=1,
									   showplot = True,
									   nonewfigure=True,
									   yscale = 'log',
									#    boxplot = True,
									#    gdesign_index=0,
									#    gN_index=0
									   save_plot_in=file+plotname
									   )

# structure_functions.plot_from_txtfile(file+txtname,
# 									  'N',
# 									  'depth',
# 									  gdesign_index=0,
# 									  showplot=True,
# 									  boxplot=True)
