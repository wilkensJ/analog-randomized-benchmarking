from asrb import *
import structure_functions
#################################################
# !! What to check before running a simulation: #
#################################################
save_data = True
save_spectralgap = False
showplot = False
plotname_fp, plotname_sv = 'framepotential.png', 'singularvalues.png'
########################################
# Define the values for each variable list. #
########################################
depth_list = np.repeat(30, 1)
dim_list  = np.arange(3, 4, 1)
design_list = np.array([2])
N_list = np.array([4])
# Define a dictionary to store important information.
params = {}
## unitary_func
# many_circuits
# many_haar_unitaries
unitary_func = many_circuits
## circuit_func
# unitaries_from_hamiltonians
params['circuit_func'] = unitaries_from_hamiltonians
#############################################################################
######################## If band diagonal is chosen ###########################
#############################################################################
## hamiltonian_func
# draw_1d_noninteracting
# draw_2d_noninteracting
# draw_1d_interacting
# draw_2d_interacting
params['hamiltonian_func'] = draw_1d_interacting
params['number_particles'] = 2
## distributions
# normal_distribution
# uniform_distribution
# repeated_distribution, set distribution=normal_disbtribution
# only_number, set fixed_value=1.
params['diag_func_hop'] = only_number
params['band_func_hop'] = uniform_distribution
params['diag_func_int'] = only_number
## For drawing one entry and copying it to the whole diagonal/band for one matrix.
# random_entry_uniform
# random_entry_normal
# fixed_value
params['random_entry_normal'] = True
# params['fixed_value'] = 0.5
# params['number_particles'] = 2
# params['low'] = -2
# params['high'] = 2
# This defines the mean for the normal distribution.
params['loc'] =  0.
# This defines the deviation of the normal distribution.
params['scale'] = 1.
#############################################################################
#############################################################################
#############################################################################
# Set the k-design value for calculating the frame potentials.
params['time'] = 1.
# Define how much smaller or bigger the band diagonal entries are than the
# diagonal entries.
params['bandweight'] = 1.
# Save the spectral gap singular values.
params['save_spectralgap'] = save_spectralgap
## sampling method
# pairs
# oneset
# twosets
params['sampling_method'] = 'pairs'
##########################
## Run the actual code. ##
##########################
dirname, filename_fp, filename_sv = run_simulation(unitary_func,
	depth_list, dim_list, design_list, N_list, **params, save_data = save_data)
############################ 
###### Plot the data #######
############################
# if save_data:
# 	plotname_fp = f'{dirname}{plotname_fp}'
# 	# Generate the plot.
# 	structure_functions.plot_from_txtfile(
# 		filename_fp, 'N', 'design', showplot=showplot, save_plot_in=plotname_fp,
# 		boxplot=True)
# Make the singular values histogram.
if save_spectralgap:
	list_singvalues_ind = [0, 1, 2, 3]
	plotname_sv = f'{dirname}{plotname_sv}'
	structure_functions.singvalues_plot(
		filename_fp, filename_sv, list_singvalues_ind, 'depth', gN_index=-1,
		gdesign_index = 0, change_of_basis=True,
		showplot=showplot, save_plot_in=plotname_sv)
# Done'
print('done!')

