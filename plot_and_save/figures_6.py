from plot_functions import *

# Gauss
filenames = [f'../numericalDataMerged/{x}Simulation/merged_framepotentials.txt' for x in range(0,4)]
filenames_list = []
filenames_list.append(f'../numericalDataMerged/{0}Simulation/merged_framepotentials.txt')
filenames_list.append(f'../numericalDataMerged/{1}Simulation/merged_framepotentials.txt')
filenames_list.append(f'../numericalDataMerged/{0}Simulation/merged_framepotentials.txt')
filenames_list.append(f'../numericalDataMerged/{1}Simulation/merged_framepotentials.txt')
filenames_list.append(f'../numericalDataMerged/{9}Simulation/merged_framepotentials.txt')
filenames_list.append(f'../numericalDataMerged/{10}Simulation/merged_framepotentials.txt')
designlistlist = [[7,12], [7,12], [1,2,3],[1,2,3], [1,2],[1,2]]
plotname = filenames[0][:23] + 'gauss_banddiagonal_6figures_2.pdf'
kwargs = {}

plot_6_subplots_framepotential(filenames_list, plotname, designlist_list=designlistlist)

# Uniform
filenames = [f'../numericalDataMerged/{x}Simulation/merged_framepotentials.txt' for x in range(4,8)]
filenames_list = []
filenames_list.append(filenames[0])
filenames_list.append(filenames[1])
filenames_list.append(filenames[0])
filenames_list.append(filenames[1])
filenames_list.append(filenames[2])
filenames_list.append(filenames[3])
designlistlist = [[7,12], [7,12], [1,2,3],[1,2,3], [1,2],[1,2]]
plotname = filenames[0][:23] + 'uniform_banddiagonal_6figures_2.pdf'
kwargs = {}

plot_6_subplots_framepotential(filenames_list, plotname, designlist_list=designlistlist)