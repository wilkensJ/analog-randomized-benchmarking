from plot_functions import *

# Gauss
filenames = [f'../numericalDataMerged/{x}Simulation/merged_framepotentials.txt' for x in range(0,4)]
designlistlist = [[1,2,6,12],[1,2,6,12],[1,2],[1,2]]
plotname = filenames[0][:23] + 'gauss_banddiagonal.pdf'
kwargs = {}

plot_4_subplots_framepotential(filenames, plotname, designlist_list=designlistlist)

# Uniform
filenames = [f'../numericalDataMerged/{x}Simulation/merged_framepotentials.txt' for x in range(4,8)]
designlistlist = [[1,2,6,12],[1,2,6,12],[1,2],[1,2]]
plotname = filenames[0][:23] + 'uniform_banddiagonal.pdf'
kwargs = {}

plot_4_subplots_framepotential(filenames, plotname, designlist_list=designlistlist)