import os
from pathlib import Path
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy.stats import norm
import matplotlib
from scipy.stats import chisquare
from scipy.stats import chi2
from scipy.special import factorial
from copy import deepcopy
from scipy.optimize import curve_fit
import math
import pdb



def plot_several_boxplots(data, xaxis, **kwargs):
    """ Takes a two dimensional array (matrix) np.ndarray:data
    and plots every array in a boxplot for each point.
    Input:
        np.ndarray:data, shape(a, b),
        np.ndarray or list:xaxis, with len(xaxis)=a,
    Stuff that can be defined through kwargs:
        nonewfigure = True or False
        alpha = float between 0 and 1
        plotcolor = number between 1 and 200
        averagecolor = number between 1 and 200
        show_average_connection = by default True, can be set to False
        shiftby_x = a float/integer for which all data points are shifted to left/right
        shiftby_y = a float/integer for which all data points are shifted up/down
        addtolabel = a string
        yscale = nothing or 'log'
        xscale = nothing or 'log'
        gylabel = string
        gxlabel = string
        save_plot_in = a string of filename
        showplot = True or False
        
    """
    # Make a new figure only if the variable nonewfigure is False or
    # does not exist.
    if kwargs.get('nonewfigure'):
        print('No new figure.')
    else:
        plt.figure()
    # In case the opacity has to be set.
    alpha_plot = kwargs.get('alpha', .45)
    # Define a colormap and colors for average and median.
    infernofunc = plt.get_cmap("inferno")
    # For the boxplot thingies.
    plotcolor_number = kwargs.get('plotcolor', 100)
    plotcolor = infernofunc(plotcolor_number)
    # For the average line color.
    plotcolor_number = kwargs.get('averagecolor', 100)
    averagecolor = infernofunc(plotcolor_number)
    # For better visualization, the scattered values can be shifted even
    # thought they should be at the same x-axis point.
    if kwargs.get("shiftby_x"):
        shiftby_x = kwargs["shiftby_x"]
        width_marker = 70
    else:
        shiftby_x = 0
        width_marker = 400
    # To visualize the deviation from a wanted value, introduce the shiftby_y variable.
    shiftby_y = kwargs.get("shiftby_y", 0)
    # Values of the frame potential of one run of the simulation are plotted, all
    # parameters are the same for each simulation.
    for kk in range(data.shape[0]):
        plt.scatter(xaxis+shiftby_x, data[kk]+shiftby_y, s=width_marker, marker="_", linewidth=4,
                    alpha=alpha_plot, color=plotcolor)
    # # Calculate median of each singular value.
    # median = np.median(all_plot_data, axis=0)
    # Calculate average of each singular value
    average = np.average(data, axis=0)
    print(f"averages: {average}")
    # Plot the average with triangles and certain color.
    # Something can be added to the label with kwargs.
    addtolabel = kwargs.get('addtolabel', '')
    plt.scatter(xaxis+shiftby_x, average+shiftby_y, marker=4, s=60, color=averagecolor,
                alpha=.9, label=addtolabel)
    if kwargs.get('show_average_connection', True):
        plt.plot(xaxis+shiftby_x, average+shiftby_y, "--", alpha=.9, color=averagecolor)
    # The yscale could be log for example.
    if kwargs.get("yscale"):
        plt.yscale(kwargs["yscale"])
    if kwargs.get("xscale"):
        plt.xscale(kwargs["xscale"])
    # Define the x-axis and y-axis labels.
    plt.xlabel(kwargs.get('gxlabel'))
    plt.ylabel(kwargs.get('gylabel'))
    plt.legend()
    # And the plot can be saved.
    if kwargs.get("save_plot_in"):
        plt.savefig(kwargs["save_plot_in"], format = "png")
    # For example via ssh showing plots is not possible.
    if kwargs.get("showplot"):
            plt.show()
    return average


def plot_from_txtfile(filename, x_axis, third_axis, **kwargs):
    """ With this function the data stored in the given file are
    first imported, reshaped and then the desired values are
    plotted. The third axis gives different graphs which are
    overlaying in one plot.
    Input:
        str:filename,
        str:x_axis, either "N", "dim", "design", "depth". Defines
                        which variables is displayed on the xAxis.
        str:third_axis, either "N", "dim", "design", "depth", but not the
                       same as str:x_axis. Defines which
                       variables is altered, too, and displayed in
                       one figure.
    """
    # Extract the depth and indices of the parameters arrays.
    pa_dict, order_list, count = extract_information(filename)
    # Open the file in read mode and extract the comments.
    with open(filename, "r") as file:
        alldata = np.loadtxt(file, skiprows=count)
    # Reshape the given data which starts when the variable lists ends,
    # for example if design is the last variable, start with "designindex".
    last_index = f'{order_list[-1][:-5]}index'
    fp_data = alldata[pa_dict[last_index]:].reshape(pa_dict["depthlen"],
        pa_dict["dimlen"], pa_dict["Nlen"],pa_dict["designlen"])
    # Gather the right indices.
    all_indices = define_indexing(x_axis, third_axis, **kwargs)
    # Index the data according to the start and end indices which where
    # just chosen.
    fp_data = fp_data[all_indices]
    # Check, if x_axis is in order before or after third_axis.
    # If so, the axis in np.ndarray:fp_data have to be switched.
    if order_list.index(f"{x_axis}_list") < \
        order_list.index(f"{third_axis}_list"):
        fp_data = fp_data.T
    # Make a new figure, only  if the variable nonewfigure is False or
    # does not exist.
    if kwargs.get('nonewfigure'):
        print('No new figure.')
    elif kwargs.get('figure_object'):
        plt = kwargs.get('figure_object')
    else:
        plt.figure()
    # In case the opacity has to be set.
    alpha_plot = kwargs.get('alpha', 1.0)
    # Calculate the x values.
    if kwargs.get('gxarray'):
        xarray = kwargs['gxarray']
    else:
        xarray = pa_dict[f"{x_axis}_list"][all_indices[
            order_list.index(f"{x_axis}_list")]]
    # Check, if boxplot is needed:
    if kwargs.get("boxplot"):
        # If yes, it is easier to store the plotted scattered data.
        # Since the boxplot version only makes sense, if  one variable is
        # repeated many times and the others are held constant, we just asume
        # that here.
        all_plot_data = []
        if kwargs.get('figure_object'):
            infernofunc = kwargs.get('infernofunc')
        else:   
            # Define a colormap and colors for average and median.
            infernofunc = plt.get_cmap("inferno")
        # For the boxplot thingies.
        plotcolor_number = kwargs.get('plotcolor', 100)
        plotcolor = infernofunc(plotcolor_number)
        # For the average line color.
        plotcolor_number = kwargs.get('averagecolor', 100)
        averagecolor = infernofunc(plotcolor_number)
        # For better visualization, the scattered values can be shifted even
        # thought they should be at the same x-axis point.
        if kwargs.get("shiftby_x") != None:
            shiftby_x = kwargs["shiftby_x"]
            width_marker = 70
        else:
            shiftby_x = 0
            width_marker = 400
        # To visualize the deviation from a wanted value, introduce the shiftby_y variable.
        shiftby_y = kwargs.get("shiftby_y", 0)
        # Values of the frame potential of one run of the simulation are plotted, all
        # parameters are the same for each simulation.
        for kk in range(pa_dict[third_axis + "len"]):
            plt.scatter(xarray+shiftby_x, fp_data[kk]+shiftby_y, s=width_marker, marker="_", linewidth=4,
                        alpha=alpha_plot - .55, color=plotcolor)
            # Store the right now plotted data.
            all_plot_data.append(fp_data[kk])
        # Reshape it to calculate the median and average of each singular value.
        all_plot_data = np.array(all_plot_data).reshape(
            pa_dict[f"{third_axis}len"], len(fp_data[0]))
        # # Calculate median of each singular value.
        # median = np.median(all_plot_data, axis=0)
        # Calculate average of each singular value
        average = np.average(all_plot_data, axis=0)
        print(f"averages: {average}")
        # Check if the variance of the frame potentials should be plotted, too.
        if kwargs.get("variancetube"):
            # Initiate the array where the variances are stored.
            variances = np.zeros(pa_dict[f"{x_axis}len"])
            # Iterate over the x axis and calculate the variance.
            for kk in range(pa_dict[f"{x_axis}len"]):
                # Take the kk'th row of data
                data_for_hist = fp_data[:,kk]
                # Calculate the histogram and fit a gauss.
                # Return the fitting variables.
                popt, _ = hist_gaussfit(data_for_hist,
                                           show_hist=False ,**kwargs)
                variances[kk] = popt[1]
            # Calculate the upper and lower bound.
            upperBound = average + variances
            lowerBound = average - variances
            # Set the tubecolor.
            plotcolor_number = kwargs.get("tubecolor", 140)
            tubecolor = infernofunc(plotcolor_number)
            plt.plot(xarray+shiftby_x, upperBound+shiftby_y, "--", color=tubecolor)
            plt.plot(xarray+shiftby_x, lowerBound+shiftby_y, "--", color=tubecolor)
                     #label="Variance tube")
            print(variances)
        # Plot the average with triangles and certain color.
        # Something can be added to the label with kwargs.
        addtolabel = kwargs.get('addtolabel', '')
        if kwargs.get('nolabel'):
            plt.scatter(xarray+shiftby_x, average+shiftby_y, marker=4, s=60, color=averagecolor,
                        alpha=alpha_plot - (1. - alpha_plot))
        else:
            plt.scatter(xarray+shiftby_x, average+shiftby_y, marker=4, s=60, color=averagecolor,
                        alpha=alpha_plot-(1.-alpha_plot),
                        label=
                        # third_axis + " " \
                        # + str(pa_dict[third_axis + "_list"][0]) +
                        addtolabel)
        plt.plot(xarray+shiftby_x, average+shiftby_y, "--", alpha=alpha_plot - (1. - alpha_plot),
                 color=averagecolor)
    else:
        # Go through the arrays in np.ndarray:fp_data and plot them one by one.
        if kwargs.get('nolabel'):
            for kk in range(pa_dict[f"{third_axis}len"]):
                plt.plot(xarray, fp_data[kk], alpha=alpha_plot)
        else:
            for kk in range(pa_dict[f"{third_axis}len"]):
                plt.plot(xarray, fp_data[kk], alpha=alpha_plot,
                         label=f"{third_axis}:" \
                         + str(pa_dict[f"{third_axis}_list"][kk]))
    # The yscale could be log for example.
    if kwargs.get("yscale"):
        plt.set_yscale(kwargs["yscale"])
    if kwargs.get("xscale"):
        plt.set_xscale(kwargs["xscale"])
    plt.legend()
    # And the plot can be saved.
    if kwargs.get("save_plot_in"):
        plt.savefig(kwargs["save_plot_in"], format = "pdf")
    # For example via ssh showing plots is not possible.
    if kwargs.get("showplot"):
            plt.show()
    # An histogram can be done for one value of N.
    if kwargs.get("histogram"):
        # Check if a value for N is given, if not set it to -1.
        indN = kwargs.get("hist_for_index_N", -1)
        # Take the indN'th row of data
        data_for_hist = fp_data[:,indN]
        usedN = pa_dict[f"{x_axis}_list"][indN]
        # Make the histogram.
        popt, pcov = hist_gaussfit(data_for_hist, N=usedN, **kwargs)


def define_indexing(*args, **kwargs):
    """ Check what variables are in args and if an index for a variable
    is given in dict:kwargs.
    Then set the right indices
    """
    # Initialize a list where all indices are stored in.
    store_indices = []
    ##########################################################
    # For amount of depth the frame potential is averaged over.
    ##########################################################
    if "depth" in args and "gdepth_index" not in kwargs:
        # Set the starting in ending index as whole range.
        depth_indices = slice(0, None)
    else:
        # Check if in dict:kwargs something is given.
        if "gdepth_index" in kwargs:
            depth_indices = kwargs["gdepth_index"]
        else:
            depth_indices = 0
    store_indices.append(depth_indices)
    #####################################
    # For dimension of Hamiltonians "dim".
    #####################################
    if "dim" in args:
        # Set the starting in ending index as whole range.
        dim_indices = slice(0, None)
    else:
        # Check if in dict:kwargs something is given.
        if "gdim_index" in kwargs:
            dim_indices = kwargs["gdim_index"]
        else:
            dim_indices = 0
    store_indices.append(dim_indices)
    ################################
    # For amount of Hamiltonians "N".
    ################################
    if "N" in args and "gN_index" not in kwargs:
        # Set the starting in ending index as whole range.
        N_indices = slice(0, None)
    else:
        # Check if in dict:kwargs something is given.
        if "gN_index" in kwargs:
            N_indices = kwargs["gN_index"]
        else:
            N_indices = 0
    store_indices.append(N_indices)
    ######################################
    # For design .
    ######################################
    if "design" in args:
        # Set the starting in ending index as whole range.
        design_indices = slice(0, None)
    else:
        # Check if in dict:kwargs something is given.
        if "gdesign_index" in kwargs:
            design_indices = kwargs["gdesign_index"]
        else:
            design_indices = 0
    store_indices.append(design_indices)
    # In the end return all the gathered single indices inside a tuple. 
    # They must be in the right order thought! depth, dim, t, N.
    return tuple(store_indices)

def extract_information(filename, *args, **kwargs):
    """ This function takes the file in which the parameters are stored and
    stores them in a dictionary which can be returned and used in other
    functions.
    """
    # Open the file in read mode and extract the comments.
    with open(filename, "r") as file:
        # The first line gives the order of the variable lists.
        firstline = file.readline()[9:-2]
        # Store the order which is necessary for the plot to be correct.
        order_list = firstline.split(", ")
        # Initialize a dictionary to store the dimension.
        pa_dict = {}
        # To count the amount of commented lines, initialize int:count.
        count = 1
        # To make indexing easier, initialize the index value.
        index = 0
        # Only read the comment lines. Use while loop to break
        # if a line is not commented any more.
        while True:
            comment = file.readline()
            # Break if the line starts not with "#".
            if not comment.startswith("#"):
                break
            # Extract the dimension and number from the comment line.
            (key, val) = comment.split(" : ")
            # The "# " has to be removed from the key variable.
            # And the \n has to be removed from the val variable.
            key, val = key[2:], val[:-1]
            # Store the values in the dictionary.
            pa_dict[key] = int(val)
            # Raise the index value by the depth of the variables list.
            index += int(val)
            # Store it in the dictionary
            pa_dict[f"{key[:-3]}index"] = index
            # Raise int:count value.
            count += 1
        # Now import all parameters data in one array.
        data = np.loadtxt(filename,
            skiprows=count)[:pa_dict["designindex"]]
        # Extract the single lists. The order is: depth, dim, t, N.
        pa_dict["depth_list"] = data[:pa_dict["depthindex"]]
        pa_dict["dim_list"] = data[
            pa_dict["depthindex"]:pa_dict["dimindex"]]
        pa_dict["N_list"] = data[pa_dict["dimindex"]:pa_dict["Nindex"]]
        pa_dict["design_list"] = data[
            pa_dict["Nindex"]:pa_dict["designindex"]]
    return pa_dict, order_list, count


def gauss_func(x, mu, sigma):
    """ Given an array x and constants a,sigma,mu and y0
    this function calculated the gaussian function
    for that array
    """
    return (1/(sigma*np.sqrt(2*np.pi)))*np.exp(-(x-mu)**2/(2*sigma**2))

def linear_func(x, m, n):
    """ Given an array, the slope m and the y-intercept n,
    calculate the y values of a linear function.
    """
    return m*x + n


def hist_gaussfit(gdata,*args,**kwargs):
    """
    Build a histogram out of the given data and fit it with a Gaussian.
    """
    from_this_residual = kwargs.get('gaverage_value', np.average(gdata))
    # Calculate the average.
    # Now get the residuals, meaning the deviations from the average value.
    residuals = gdata - from_this_residual
    # Make a histogram. Store the counts (y-axis), bins (x-axis) and amount
    # of used bins, bars.
    plt.figure()
    # To fit a probability distribution, the histogram has to be normalized.
    counts, bins, bars = plt.hist(residuals, density=True)
    # Calculate how wide one bin is.
    delta_bin = bins[1]-bins[0]
    # Get a good estimate for the data
    (mu, sigma) = norm.fit(residuals)
    # Fit the data with a gaussian function, defined here (gauss_func).
    # y0 is not a fit parameter, meaning it is technically 0.
    # The bins have to be shifted half a bin, since the histogram orders
    # the values in one interval, meaning that there is one bin more than
    # counts.
    xpoints = bins[:-1]+delta_bin/2.
    popt, pcov = curve_fit(gauss_func, xpoints, counts, p0=(mu, sigma))
    # Make the fitting x-axis more precise for plotting
    xfit = np.arange(bins[0],bins[-1],delta_bin/5.)
    # Calculate the data with the parameter calculated by the curve_fit
    # function and the x-axis which is more precise.
    yfit = gauss_func(xfit, *popt)
    # Now a chi-squared test can be done to check, if the given data
    # really obeys an gaussian distribution
    if kwargs.get("chisquared_test"):
        # counts is our observed data, the y points of the gaussian function
        # with the fitting parameters is our expected data and since we had
        # to estimate two parameters for the gaussian fit ddof is set to 2
        # and the overall degrees of freedom are then:
        # amount of counts - 1 - ddof (here 2)
        dof = len(counts)-1-3
        chisq, p = chisquare(counts, gauss_func(xpoints, *popt), ddof=3)
        # Calculate the critical chi-squared value for a certain probability
        probability = .95
        critical = chi2.ppf(probability, dof)
        print("Calculated X**2 for data with hypothesis gauss distribution: "
            + str(chisq)
            + ", \n critical value under which hypothesis is accepted: "
            + str(critical))
        print("And the standard deviation = " + str(popt[1]))
    # Plot the fitted data into the histogram.
    plt.plot(xfit, yfit,'r--',linewidth=2, label="gaussian fit")
    plt.grid(True)
    plt.xlabel("frame potential value")
    plt.ylabel("amount")
    plt.legend()
    if kwargs.get("N") != None:
        plt.title(
            "Normalized histogram of residuals for N = " + str(kwargs["N"]))
    else:
        plt.title("Normalized histogram of residuals")
    # And the plot can be saved.
    if "save_hist" in kwargs:
        plt.savefig(kwargs["save_hist"] , format = "png")
    # For example via ssh showing plots is not possible.
    if kwargs.get("show_hist"):
        plt.show()
    else:
        # Have to delete the figure, else it will pop up when another
        # show() appears!
        plt.close()
    # Return the fitting parameters.
    return popt, pcov



def plot_variance(filename, x_axis, third_axis, **kwargs):
    """ With this function the data stored in the given file are
    first imported, reshaped and then the desired values are
    plotted. The third axis should be the value which was the same
    for statistical purpose! 
    Input:
        str:filename,
        str:x_axis, either "N", "dim", "design", "depth". Defines
                        which variables is displayed on the xAxis.
        str:third_axis should be the value which stayed constant and
            was repeated to make a boxplot.
    """
    # Extract the depth and indices of the parameters arrays.
    pa_dict, order_list, count = extract_information(filename)
    # Open the file in read mode and extract the comments.
    with open(filename, "r") as file:
        alldata = np.loadtxt(file, skiprows=count)
    # Reshape the given data which starts when the variable lists ends,
    # since N is the last variable, start with "N_index".
    fp_data_original = alldata[pa_dict["designindex"]:].reshape(pa_dict["depthlen"],
        pa_dict["dimlen"], pa_dict["Nlen"],pa_dict["designlen"])
    all_variances = []
    all_averages = []
    all_factorials = []
    # Go through the 
    if x_axis == 'design' and third_axis == 'N':
        # Gather the right indices.
        all_indices = define_indexing(x_axis, third_axis, **kwargs)
        xarray = pa_dict[f"{x_axis}_list"][all_indices[
            order_list.index(f"{x_axis}_list")]]
        # Index the data according to the start and end indices which where
        # just chosen.
        fp_data = fp_data_original[all_indices]
        variances = np.zeros(pa_dict[f"{x_axis}len"])
        averages = np.zeros(pa_dict[f"{x_axis}len"])
        # Iterate over the x axis and calculate the variance.
        for kk in range(pa_dict[f"{x_axis}len"]):
            factorial_value = np.math.factorial(int(pa_dict[f'{x_axis}_list'][kk]))
            all_factorials.append(factorial_value)
            # Take the kk'th row of data
            data_for_hist = fp_data[:,kk]
            # Calculate the histogram and fit a gauss.
            # Return the fitting variables.
            popt, pcov = hist_gaussfit(data_for_hist,
                show_hist=False, gaverage_value=factorial_value, **kwargs)
            variances[kk] = popt[1]
            averages[kk] = popt[0]
        all_variances.append(variances)
        all_averages.append(averages)
        for_label = pa_dict['N_list'][0]
        plt.plot(xarray, variances, "x--", label=f'N: {for_label}')
    print('averages: ', np.array(all_averages))
    print('variances: ', all_variances)
    # The yscale could be log for example.
    if kwargs.get("yscale"):
        plt.yscale(kwargs["yscale"])
    if kwargs.get("xscale"):
        plt.xscale(kwargs["xscale"])
    # Define the x-axis.
    if kwargs.get('gxLabel'):
        xLabel = kwargs['gxLabel']
    else:
        xLabel = x_axis
    plt.xlabel(xLabel)
    plt.ylabel("variance")
    plt.legend()
    # And the plot can be saved.
    if kwargs.get("save_plot_in" ):
        plt.savefig(kwargs["save_plot_in"], format="png")
    # For example via ssh showing plots is not possible.
    if kwargs.get("showplot"):
        plt.show()
    # An histogram can be done for one value of N.
    if kwargs.get("histogram"):
        # Check if a value for N is given, if not set it to -1.
        indN = kwargs.get("hist_for_index_N", -1)
        # Take the indN'th row of data
        data_for_hist = fp_data[:,indN]
        usedN = pa_dict[f"{x_axis}_list"][indN]
        # Make the histogram.
        popt, pcov = hist_gaussfit(data_for_hist, N=usedN, **kwargs)
    return xarray, variances



def plot_deviation_Hoeffdings(filename, **kwargs):
    """ With this function the average absolute deviation
    aad=W(X)=sum_i^N |X_i - bar(X)| where X_is are the framepotential
    values and bar(X) the average of the numerical frame potentials and the deviation of the average
    from the analytical value 
    The third axis should be the value which was the same
    for statistical purpose! 
    Input:
        str:filename,
        str:x_axis, either "N", "dim", "design", "depth". Defines
                        which variables is displayed on the xAxis.
        str:third_axis should be the value which stayed constant and
            was repeated to make a boxplot.
    """
    font = {'size'   : 15, 'family':'serif','serif':['Times'] }
    matplotlib.rc('font', **font)

    x_axis, third_axis = 'design', 'N'
    # Set the color map.
    infernofunc = plt.get_cmap("inferno")
    # Extract the depth and indices of the parameters arrays.
    pa_dict, order_list, count = extract_information(filename)
    # Open the file in read mode and extract the comments.
    with open(filename, "r") as file:
        alldata = np.loadtxt(file, skiprows=count)
    # Reshape the given data which starts when the variable lists ends,
    # since N is the last variable, start with "N_index".
    fp_data_original = alldata[pa_dict["designindex"]:].reshape(pa_dict["depthlen"],
        pa_dict["dimlen"], pa_dict["Nlen"],pa_dict["designlen"])
    # Go through the 
    # Gather the right indices.
    all_indices = define_indexing(x_axis, third_axis, **kwargs)
    # Extract the x-axis.
    xarray = pa_dict[f"{x_axis}_list"][all_indices[
        order_list.index(f"{x_axis}_list")]]
    # Index the data according to the start and end indices which where
    # just chosen.
    fp_data = fp_data_original[all_indices]
    # Extract the sample size.
    N = int(pa_dict['N_list'][0])
    # Extract how many runs there are
    runs = len(pa_dict['N_list'])
    # Get the dimension used for the simulation.
    dim = pa_dict['dim_list'][0]
    # Calculate the factorial value, e.g. the analytical framepotential of each t-design.
    factorials = factorial(xarray)
    # Tile the factorial such that it fits the shape of fp_data.
    np.tile(factorials, runs)
    # deviation to analytical value, dta
    # Calculate the deviation of the framepotentials from the analytical framepotential t!.
    dta = np.absolute(fp_data - factorials)
    # Set the showplot variabel to false since it should not be shown, yet.
    showplot = kwargs.get('showplot', False)
    kwargs['showplot'] = False
    averages = plot_several_boxplots(
        dta, xarray, show_average_connection=False, **kwargs)
    # Estimate the slope.
    estimated_slope = (math.log(averages[-1], dim) - math.log(averages[0], dim))/(xarray[-1] - xarray[0])
    # And the fit. Since the math.log function does not take an array as argument, 
    # the basis of the logarithm has to be changed by hand.
    popt, pcov = curve_fit(
        linear_func, xarray, np.log(averages)/np.log(dim), p0=(estimated_slope, 0))
    # Calculate the fitted linear function y values.
    fitted_data = dim**linear_func(xarray, *popt)
    # Plot!
    plt.plot(xarray, fitted_data, '--', color=infernofunc(kwargs.get('averagecolor', 100)), label='fit')
    plt.legend()
    print(f'slope = {popt[0]}+- {np.sqrt(np.diag(pcov))[0]}')
    plt.xlabel(r'$t$')
    plt.ylabel(r'$|F(t)-t!|$')
    if kwargs.get('save_plot_in'):
        plt.savefig(kwargs.get('save_plot_in'), format='pdf')
    if showplot:
        plt.show()


def plot_deviation_Hoeffdings_variable_N(filename, **kwargs):
    """ With this function the average absolute deviation
    aad=W(X)=sum_i^N |X_i - bar(X)| where X_is are the framepotential
    values and bar(X) the average of the numerical frame potentials and the deviation of the average
    from the analytical value 
    The third axis should be the value which was the same
    for statistical purpose! 
    Input:
        str:filename,
        str:x_axis, either "N", "dim", "design", "depth". Defines
                        which variables is displayed on the xAxis.
        str:third_axis should be the value which stayed constant and
            was repeated to make a boxplot.
    """
    x_axis, third_axis = 'design', 'N'
    # Set the color map.
    infernofunc = plt.get_cmap("inferno")
    # Extract the depth and indices of the parameters arrays.
    pa_dict, order_list, count = extract_information(filename)
    # Open the file in read mode and extract the comments.
    with open(filename, "r") as file:
        alldata = np.loadtxt(file, skiprows=count)
    # Reshape the given data which starts when the variable lists ends,
    # since N is the last variable, start with "N_index".
    fp_data_original = alldata[pa_dict["designindex"]:].reshape(pa_dict["depthlen"],
        pa_dict["dimlen"], pa_dict["Nlen"],pa_dict["designlen"])
    runs = 20
    data_list = []
    # Gather the right indices.
    for kk in range(min(pa_dict["designlen"], pa_dict["Nlen"])):
        data = fp_data_original[0,0,kk::6*7,kk]
        data_list.append(data)
    pdb.set_trace()
    



def plot_4_subplots_framepotential(filenames_list, plotname, designlist_list=[[1],[1],[1],[1]], **kwargs):
    """ Bla
    """
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(8.2, 6.8))
    params = {}
    params['infernofunc'] = plt.get_cmap('inferno')
    params['showplot'] = False
    params['boxplot'] = True
    params['yscale'] = 'log'
    for kk in range(4):
        shift_by_x = np.linspace(0, 0, len(designlist_list[kk]))
        factorialvalues = factorial(np.array(designlist_list[kk]))
        plotcolorlist = np.int_(np.linspace(10, 200, len(designlist_list[kk])))
        if kk == 0:
            ax = ax1
            ax.set_title('(a)')
        elif kk == 1:
            ax = ax2
            ax.set_title('(b)')
        elif kk == 2:
            ax = ax3
            ax.set_title('(c)')
        elif kk == 3:
            ax = ax4
            ax.set_title('(d)')
        
        for count_design in range(0, len(designlist_list[kk])):
            ax.plot([1, 125], [factorialvalues[count_design],factorialvalues[count_design]], alpha=.5, color='black')
            plot_from_txtfile(filenames_list[kk],
                                'depth',
                                'N',
                                figure_object = ax,
                                gdesign_index=designlist_list[kk][count_design]-1,
                                addtolabel=f'{designlist_list[kk][count_design]} moment',
                                plotcolor=plotcolorlist[count_design],
                                averagecolor=plotcolorlist[count_design],
                                shiftby_x=shift_by_x[count_design],
                                **params
                                )
            
    fig.tight_layout()
    plt.savefig(plotname, format='pdf')
    plt.show()


def plot_6_subplots_framepotential(filenames_list, plotname, designlist_list=[[1],[1],[1],[1],[1],[1]], **kwargs):
    """ Bla
    """
    font = {'size'   : 11, 'family':'serif','serif':['Times'] }
    matplotlib.rc('font', **font)
    fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2, figsize=(6.8, 8.2), sharex=True)
    params = {}
    params['infernofunc'] = plt.get_cmap('inferno')
    params['showplot'] = False
    params['boxplot'] = True
    params['yscale'] = 'log'
    for kk in range(6):
        shift_by_x = np.linspace(0, 0, len(designlist_list[kk]))
        factorialvalues = factorial(np.array(designlist_list[kk]))
        plotcolorlist = np.int_(np.linspace(10, 200, len(designlist_list[kk])))
        if kk == 0:
            ax = ax1
            ax.set_title('(a)')
        elif kk == 1:
            ax = ax2
            ax.set_title('(b)')
        elif kk == 2:
            ax = ax3
            ax.set_title('(c)')
        elif kk == 3:
            ax = ax4
            ax.set_title('(d)')
        elif kk == 4:
            ax = ax5
            ax.set_title('(e)')
        elif kk == 5:
            ax = ax6
            ax.set_title('(f)')
        
        for count_design in range(0, len(designlist_list[kk])):
            ax.plot([1, 125], [factorialvalues[count_design],factorialvalues[count_design]], alpha=.5, color='black')
            plot_from_txtfile(filenames_list[kk],
                                'depth',
                                'N',
                                figure_object = ax,
                                gdesign_index=designlist_list[kk][count_design]-1,
                                addtolabel=f'{designlist_list[kk][count_design]} moment',
                                plotcolor=plotcolorlist[count_design],
                                averagecolor=plotcolorlist[count_design],
                                shiftby_x=shift_by_x[count_design],
                                **params
                                )
    ax6.set_xlabel('depth')
    ax5.set_xlabel('depth')
    fig.tight_layout()
    plt.savefig(plotname, format='pdf')
    plt.show()
        



    
    