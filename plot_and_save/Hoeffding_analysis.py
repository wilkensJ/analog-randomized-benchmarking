import plot_functions as pf
import numpy as np
import matplotlib.pyplot as plt

dir_name = '../numericalDataMerged/0Simulation/'

filename = f'{dir_name}merged_framepotentials.txt'
infernofunc = plt.get_cmap("inferno")
save_plot_in = f'{dir_name}Hoeffding.pdf'
pf.plot_deviation_Hoeffdings(filename,
                             gdepth_index=-1,
                             yscale='log',
                             plotcolor=150,
                             averagecolor=50,
                             addtolabel = 'average',
                             showplot = True,
                             save_plot_in=save_plot_in)




