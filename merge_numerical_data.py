from structure_functions import merge_same_runs
from structure_functions import not_completed_runs_merge

''' Merge files withe the exact same parameters
'''
# Create the list with filenames
second_part = 'Simulation/framepotentials.txt'
list_filenames = [f'numericalData/{x}{second_part}' for x in range(200, 220)]
print('Merge:')
print(list_filenames)
merge_same_runs(list_filenames, 'N')
print('Done!')


''' Merge not completed runs to other files and delete the not completed runs.
'''
# # Create the list with filenames
# second_part = 'Simulation'
# list_filenames_write_to = [f'numericalData/{x}{second_part}' for x in range(47,67)]
# list_filenames_delete = [f'numericalData/{x}{second_part}' for x in range(87, 107)]

# not_completed_runs_merge(list_filenames_write_to,list_filenames_delete)