#!/bin/bash

#SBATCH --job-name=2dnonint               # Job name, will show up in squeue output
#SBATCH --ntasks=20                        # Number of cores
#SBATCH --nodes=1                         # Ensure that all cores are on one machine
#SBATCH --time=3-00:00:00                 # Runtime in DAYS-HH:MM:SS format
#SBATCH --mem-per-cpu=1024                # Memory per cpu in MB (see also --mem) 
#SBATCH --output=2dnonint_%j.out          # File to which standard out will be written
#SBATCH --error=2dnonint_%j.err           # File to which standard err will be written
#SBATCH --mail-type=ALL                   # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=j.wilkens@fu-berlin.de   # Email to which notifications will be sent 

# store job info in output file, if you want...
scontrol show job $SLURM_JOBID

# sleep 80

# run your program...
# module add python/3.5.3
/net/opt/python/stretch/3.6.5/bin/python3 kernel_worker_2d_noninteracting.py

