#!/bin/bash

#SBATCH --job-name=rb005depol              # Job name, will show up in squeue output
#SBATCH --ntasks=10                     # Number of cores
#SBATCH --nodes=1                      # Ensure that all cores are on one machine
#SBATCH --time=1-00:00:00              # Runtime in DAYS-HH:MM:SS format
#SBATCH --mem-per-cpu=10240             # Memory per cpu in MB (see also --mem) 
#SBATCH --output=rb005depol_%j.out           # File to which standard out will be written
#SBATCH --error=rb005depol_%j.err            # File to which standard err will be written
#SBATCH --mail-type=ALL                # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=j.wilkens@fu-berlin.de   # Email to which notifications will be sent 

# store job info in output file, if you want...
scontrol show job $SLURM_JOBID

# sleep 80

# run your program...
# module add python/3.6.5
/net/opt/python/stretch/3.6.5/bin/python3 rbprotocol_kernel_worker_005depol_2p.py




