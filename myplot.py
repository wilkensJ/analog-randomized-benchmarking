from itertools import count
from typing import Sequence
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os.path
from scipy.optimize import curve_fit
import math
import pdb




def extract_postprocess_data(filename, *args, **kwargs):
    """ With the given filename the data can be extracted,
    first comment: how many irreps (or decays),
    second comment: how long the sequence length is
    without comment: the sequence lengths,
    then the data, starting with the first decay, then second, ...
    """
    with open(filename, 'r') as file:
        amount_decays = int(file.readline()[-2])
        len_sequences = int(file.readline()[-2])
        all_data = np.loadtxt(filename, skiprows=3)
        sequences = all_data[:len_sequences]
        data = all_data[len_sequences:].reshape(amount_decays, len_sequences)
    return sequences, data


def standard_plot(xaxis, data, *args, showplot=True, **kwargs):
    """ Plots x's for every data point with no connection.
    Input:
        np.ndarray:data, if the shape is indicating a matrix, several colors are used for
            the different arrays in np.ndarray:data.
        np.ndarray:xaxis, the x axis values, can only be an array or fit the dimension of np.ndarray:data.
    """
    # Check the dimensions of np.ndarray:data.
    if len(data.shape) == 2 and len(xaxis.shape) == 1:
        # A matrix is given, meaning for every array in np.ndarray:data x's with different colors are plotted.
        plots = data.shape[0]
        # Repeat the array for plotting.
        xaxis = np.tile(xaxis, plots).reshape(data.shape)
    elif len(data.shape) == 2 and len(xaxis.shape) == 2:
        plots = data.shape[0]
    else:
        plots = 1
    # Now plot the data, each row one by one to insert the label.
    for count in range(plots):
        plt.plot(xaxis[count], data[count], marker = 'x', label=f'{count+1}. young diagram')
    plt.legend()
    if showplot:
        plt.show()


def plot_several_boxplots(data, xaxis, *args, **kwargs):
    """ Takes a two dimensional array (matrix) np.ndarray:data
    and plots every array in a boxplot for each point.
    Input:
        np.ndarray:data, shape(a, b),
        np.ndarray or list:xaxis, with len(xaxis)=a,
        
    """
    # Make a new figure only if the variable nonewfigure is False or
    # does not exist.
    if kwargs.get('nonewfigure'):
        print('No new figure.')
    else:
        plt.figure()
    # In case the opacity has to be set.
    alpha_plot = kwargs.get('alpha', .45)
    # Check, if boxplot is needed:
    # If yes, it is easier to store the plotted scattered data.
    # Since the boxplot version only makes sense, if  one variable is
    # repeated many times and the others are held constant, we just asume
    # that here.
    all_plot_data = []
    # Define a colormap and colors for average and median.
    infernofunc = plt.get_cmap("inferno")
    # For the boxplot thingies.
    plotcolor_number = kwargs.get('plotcolor', 100)
    plotcolor = infernofunc(plotcolor_number)
    # For the average line color.
    plotcolor_number = kwargs.get('averagecolor', 100)
    averagecolor = infernofunc(plotcolor_number)
    # For better visualization, the scattered values can be shifted even
    # thought they should be at the same x-axis point.
    if kwargs.get("shiftby_x"):
        shiftby_x = kwargs["shiftby_x"]
        width_marker = 70
    else:
        shiftby_x = 0
        width_marker = 400
    # To visualize the deviation from a wanted value, introduce the shiftby_y variable.
    shiftby_y = kwargs.get("shiftby_y", 0)
    # Values of the frame potential of one run of the simulation are plotted, all
    # parameters are the same for each simulation.
    for kk in range(len(data)):
        plt.scatter(xaxis+shiftby_x, data[kk]+shiftby_y, s=width_marker, marker="_", linewidth=4,
                    alpha=alpha_plot, color=plotcolor)
    # # Calculate median of each singular value.
    # median = np.median(all_plot_data, axis=0)
    # Calculate average of each singular value
    average = np.average(data, axis=0)
    print(f"averages: {average}")
    # Plot the average with triangles and certain color.
    # Something can be added to the label with kwargs.
    addtolabel = kwargs.get('addtolabel', '')
    if not kwargs.get('noaverage', False):
        plt.scatter(xaxis+shiftby_x, average+shiftby_y, marker=4, s=60, color=averagecolor,
                alpha=.9, label=addtolabel)
        if not kwargs.get('no_dashedline'):
            plt.plot(xaxis+shiftby_x, average+shiftby_y, "--", alpha=.9, color=averagecolor)
    # The yscale could be log for example.
    if kwargs.get("yscale"):
        plt.yscale(kwargs["yscale"])
    if kwargs.get("xscale"):
        plt.xscale(kwargs["xscale"])
    # Define the x-axis and y-axis labels.
    plt.xlabel(kwargs.get('gxlabel'))
    plt.ylabel(kwargs.get('gylabel'))
    plt.legend()
    # And the plot can be saved.
    if kwargs.get("save_plot_in"):
        plt.savefig(kwargs["save_plot_in"], format = "png")
    # For example via ssh showing plots is not possible.
    if kwargs.get("showplot"):
            plt.show()
    if kwargs.get("xscale"):
        plt.xscale(kwargs["xscale"])
    # Define the x-axis and y-axis labels.
    plt.xlabel(kwargs.get('gxlabel'))
    plt.ylabel(kwargs.get('gylabel'))
    plt.legend()
    # And the plot can be saved.
    if kwargs.get("save_plot_in"):
        plt.savefig(kwargs["save_plot_in"], format = "png")
    # For example via ssh showing plots is not possible.
    if kwargs.get("showplot"):
            plt.show()
    if kwargs.get("xscale"):
        plt.xscale(kwargs["xscale"])
    # Define the x-axis and y-axis labels.
    plt.xlabel(kwargs.get('gxlabel'))
    plt.ylabel(kwargs.get('gylabel'))
    plt.legend()
    # And the plot can be saved.
    if kwargs.get("save_plot_in"):
        plt.savefig(kwargs["save_plot_in"], format = "png")
    # For example via ssh showing plots is not possible.
    if kwargs.get("showplot"):
            plt.show()

def plot_singvalues(filename_protocol, dirname, *args, plot_singvalues=5, which_decay=1, **kwargs):
    """ This function can only be used if every file has data constructed with the same set
    of paramters, e.g. to make a boxplot out of it!
    Take the data from the postprocessed_data0.txt file and extract the amount of runs (here
    sequence lengths).
    Checks how many and which filenames there are where singular values are stored.
    Take the data from all of the files, reshape it and make a boxplot out of it for each singular value.

    """
    # Extract the sequences (runs) which were used to build the projector. Hopefully the more
    # the more accurate the projector is, i.e. the dominant singular values are close to one
    # and the others close to zero.
    # Also the amount of decays (also known as amount if irreps or amount of boxes).
    with open(filename_protocol, 'r') as file:
        # Same code as in func:extract_postprocess_data
        amount_decays = int(file.readline()[-2])
        len_sequences = int(file.readline()[-2])
        sequences = np.loadtxt(filename_protocol, skiprows=3)[:len_sequences]
    # Prepare the finding of all filenames where singular values are stored.
    count_filename, filename_not_found = 0, False
    filenames_list = []
    # Find all filenames.
    while not filename_not_found:
        filenamesv = f'{dirname}/postprocessed_data{count_filename}_projector.txt'
        if os.path.isfile(filenamesv):
            filenames_list.append(filenamesv)
        else:
            filename_not_found = True
        count_filename += 1
    # Initiate the array where the singular values are stored. Since the different amount of runs
    # have to yield a smaller variance, and the values will converge, use different colors for different
    # amount of runs.
    all_singvalues = np.zeros((len_sequences, count_filename-1, plot_singvalues))
    # Initiate the paramter to keep track of used filename number.
    count_storage = 0
    # Go through the filenames. Store the singular values, remember the order for plotting.
    # Also, filter out which decay (or irrep) will be seen.
    for filenamesv in filenames_list:
        with open(filenamesv, 'r') as file:
            all_data = np.loadtxt(filenamesv)
            amount_singvalues = len(all_data[0])
            datasv = all_data.reshape(len_sequences, amount_decays, amount_singvalues)
            datasv = datasv[:,which_decay-1,:plot_singvalues]
        # Store the singvalues
        all_singvalues[:,count_storage,:] = datasv
        count_storage += 1
    # Arange the singular values labeling as it will appear at the x-axis.
    singvalues_numbered = np.arange(1, plot_singvalues+1)
    # Chose equally spaced integers to make the coloring nice.
    plotcolor_array = np.linspace(1, 200, len_sequences, dtype=int)
    # Start by making the first boxplot, since here a new figure has to be 
    # deployed. After that not anymore, therefore all data points are in one figure.
    plot_several_boxplots(all_singvalues[0], singvalues_numbered, *args,
        addtolabel= f'{sequences[0]}. N' ,showplot=False, **kwargs)
    # Go through the remaining data points for different sequence lengths.
    for count_sequences in range(1,len_sequences):
        # Here are so many paramter for labeling, plot color and stuff.
        plot_several_boxplots(all_singvalues[count_sequences], singvalues_numbered, *args,
            addtolabel= f'{sequences[count_sequences]}. N' ,showplot=False,
            nonewfigure=True, plotcolor = plotcolor_array[count_sequences],
            averagecolor=plotcolor_array[count_sequences], **kwargs)
    plt.show()



def plot_postprocess_data(filename, *args, **kwargs):
    """
    """
    sequences, data = extract_postprocess_data(filename, *args, **kwargs)
    standard_plot(sequences, data, *args, showplot=False, **kwargs)
    plt.ylabel('survival probability')
    plt.xlabel('sequence length')
    plt.show()


def plot_boxplot(dirname, *args, showplot=True, **kwargs):

    filename_filenames = dirname + 'filenames.txt'
    with open(filename_filenames, 'r') as file:
        filenames_list_with_n = file.readlines()
    filenames_list = []
    for filename in filenames_list_with_n:
        if os.path.isfile(filename[:-1]):
            filenames_list.append(filename[:-1])

    sequences_list, data_list = [], []
    for filename in filenames_list:
        with open(filename, 'r') as file:
            amount_decays = int(file.readline()[-2])
            len_sequences = int(file.readline()[9:-1])
            all_data = np.loadtxt(filename, skiprows=3)
            sequences = all_data[:len_sequences]
            data = all_data[len_sequences:].reshape(amount_decays, len_sequences)
        sequences_list.append(sequences)
        data_list.append(data)
    x_data = sequences_list[0]
    y_data = np.array(data_list)
    plotruns = amount_decays-2
    label = 1
    plot_several_boxplots(y_data[:,0], x_data, showplot=False, **kwargs)
    for count in range(plotruns):
        label += 1
        plot_several_boxplots(y_data[:,count+1], x_data, nonewfigure=True,
                              showplot=False, addtolabel=f'{label}. young diagram', **kwargs)
    if showplot:
        plt.show()


def exp_func(x, a, b, c):
    return a*b**x + c


def plot_rb_protocol_data(dirname, *args, showplot=True, **kwargs):

    font = {'size'   : 15, 'family':'serif','serif':['Times'] }
    matplotlib.rc('font', **font)

    filename_filenames = dirname + 'filenames.txt'
    with open(filename_filenames, 'r') as file:
        filenames_list_with_n = file.readlines()
    filenames_list = []
    for filename in filenames_list_with_n:
        if os.path.isfile(filename[:-1]):
            filenames_list.append(filename[:-1])
    # Define a colormap and colors for average and median.
    infernofunc = plt.get_cmap("inferno")
    # For the boxplot thingies.
    plotcolor_number = kwargs.get('plotcolor', 100)
    plotcolor = infernofunc(plotcolor_number)
    # For the average line color.
    plotcolor_number = kwargs.get('averagecolor', 100)
    averagecolor = infernofunc(plotcolor_number)
    sequences_list, data_list = [], []
    for filename in filenames_list:
        with open(filename, 'r') as file:
            amount_decays = int(file.readline()[-2])
            len_sequences = int(file.readline()[9:-1])
            all_data = np.loadtxt(filename, skiprows=3)
            sequences = all_data[:len_sequences]
            data = all_data[len_sequences:].reshape(amount_decays, len_sequences)
        sequences_list.append(sequences)
        data_list.append(data)
    x_data = sequences_list[0]
    y_data = np.array(data_list)
    averages = np.average(y_data, axis=0)
    deviations = np.tile(averages, 10).reshape(y_data.shape) - y_data
    procentual_deviation_max = np.max(np.absolute(deviations), axis=0)/averages*100
    print('procentual deviation: ', procentual_deviation_max)
    # And the fit. Since the math.log function does not take an array as argument, 
    # the basis of the logarithm has to be changed by hand.
    popt, pcov = curve_fit(exp_func, x_data, averages[0], p0=(1.0, 0.99, 0))
    # Calculate the fitted linear function y values.
    x_data_fitted = np.linspace(x_data[0], x_data[-1], 100)
    fitted_data = exp_func(x_data_fitted, *popt)
    # Plot!
    if not kwargs.get('nonewfigure'):
        plt.figure(figsize=(5.0, 4.6))
    plt.plot(x_data_fitted, fitted_data, '--', color=plotcolor, label='fit')
    plt.scatter(x_data, averages, marker=4, s=60, color=averagecolor,
                alpha=.9, label=kwargs.get('addtolabel'))
    plt.legend()
    addtolabel = kwargs.get('addtolabel')
    print(f'{addtolabel}, fit f={popt[1]}')
    plt.xlabel(kwargs.get('gxlabel'))
    plt.ylabel(kwargs.get('gylabel'))
    if kwargs.get('save_plot'):
        plt.savefig(kwargs.get('save_plot'), format='pdf')
    if showplot:
        plt.tight_layout()
        plt.show()
    
# lot_singvalues('numericalData_rbprotocol/1Simulation/postprocessed_data0.txt', 'numericalData_rbprotocol/1Simulation/',
#               plot_singvalues=9)
plot_rb_protocol_data('numericalData_rbprotocol/26Simulation/', gxlabel='sequences', gylabel='survival probability', 
    addtolabel=r'$\xi_1 = 0.01$', alpha=0.0, plotcolor=60, averagecolor=50, showplot=False)
plot_rb_protocol_data('numericalData_rbprotocol/27Simulation/', gxlabel='sequences', gylabel='survival probability',
    addtolabel=r'$\xi_2 = 0.05$', alpha=0.0, plotcolor=160, averagecolor=150, nonewfigure=True, showplot=True,
    save_plot='numericalData_rbprotocol/uniform_005_001_dim9_fitting.pdf')
    
# plot_postprocess_data('numericalData_rbprotocol/6Simulation/merged_postprocessed_data.txt')