import numpy as np
import time

from multiprocessing import Process, Queue
from itertools import product

import rbprotocol
import structure_functions
import asrb
import pdb
import datetime

# Initiate the dictionary where parameters for the simulation can be set.
params = {}
kernels = 10
# dimensions, number of nodes
dim_list = [9]
# Important for character function
number_particles_list = [1]
# Choose sequence lenghts. Here the parallel work can be used.
final_sequences = np.array([2, 4, 8, 16, 24, 32, 42, 53, 64, 128, 256, 512])
break_up_sequences = np.tile(final_sequences, kernels).reshape(kernels, len(final_sequences))
sequences_list = list(break_up_sequences)
# How many times the same sequence lenght is applied and measured.
runs_list = [1000]
# Set the number of particles and number of node.
# params['number_particles'] = 1
# params['number_nodes'] = dim_list[0]
## Which character function should be used?
# 'character_function_young'
# 'character_function_schur'
params['characterfunction'] = 'character_function_schur'
# Set the depolarizing constant, between 0 and 1.
params['depolarizing_lambda'] = 0.05
#############################################################################
############################## MEASUREMENT ##################################
#############################################################################
## What kind of measurement
# projective_measurement
# basis_measurement
params['basis_measurement'] = True
## Which density matrix
# default 0.
params['number_fock_vector_density_matrix'] = 0
# If a random density matrix is required. This means the above parameter will be
# 'overwritten'.
params['random_density_matrix'] = False
#############################################################################
############################## unitaries ##################################
#############################################################################
## unitary_func
# many_circuits
# many_haar_unitaries
# one_haar_unitary
params['unitary_func'] =  asrb.many_circuits
# params['unitary_func'] =  asrb.many_haar_unitaries
# Set the depth of the circuits.
params['depth'] = 30
## circuit_func
# unitaries_from_hamiltonians
params["circuit_func"] = asrb.unitaries_from_hamiltonians
#############################################################################
######################## If band diagonal is chosen ###########################
#############################################################################
## hamiltonian_func
# draw_1d_noninteracting
# draw_2d_noninteracting
# draw_1d_interacting
# draw_2d_interacting
params["hamiltonian_func"] = asrb.draw_1d_noninteracting
## distributions
# normal_distribution
# uniform_distribution
# repeated_distribution, set distribution=normal_disbtribution
# only_number, set fixed_value=1.
params["diag_func_hop"] = asrb.only_number
params["band_func_hop"] = asrb.uniform_distribution
params["diag_func_int"] = asrb.only_number
## For drawing one entry and copying it to the whole diagonal/band for one matrix.
# random_entry_uniform
# random_entry_normal
# fixed_value
params["random_entry_normal"] = True
params["low"] = -2
params["high"] = 2
# This defines the mean for the normal distribution.
# params["loc"] =  0.
# This defines the deviation of the normal distribution.
# params["scale"] = 1.
#############################################################################
#############################################################################
#############################################################################
# Set the k-design value for calculating the frame potentials.
params["time"] = 1.
# Define how much smaller or bigger the band diagonal entries are than the
# diagonal entries.
params["bandweight"] = 1.
# 'rb_protocol'
# 'character_average'
params['which_simulation'] = 'rb_protocol'
# If 'character_average' chosen, these paramters can be used, too.
# params['spectralgap'] = True
# params['store_projector'] = True

# Make the dictionary into a list to make it iterable.
params_list = [params]
# # Extract the amount of kernenls by getting the sequence_list dimension.
# kernels = len(sequences_list)
# Build the list of filenames where the data will be stored.
dirname = structure_functions.create_dir(pathname='numericalData_rbprotocol')
# Build all the filenames for the postprocessed arrays to store.
filenames_list = []
for count in range(kernels):
    filenames_list.append(f'{dirname}/postprocessed_data{count}.txt')
filenames_list_list = [filenames_list]
# Write all the stuff in the labbook.
structure_functions.write_to_txt('numericalData_rbprotocol/labBook.txt', {'\n' : '\n'})
structure_functions.write_to_txt('numericalData_rbprotocol/labBook.txt', start=datetime.datetime.now())
structure_functions.write_to_txt('numericalData_rbprotocol/labBook.txt', dirname=dirname,
    dim=dim_list[0], number_particles = number_particles_list[0], runs=runs_list[0], **params)

class Worker(Process):
    def __init__(self, queue):
        super(Worker, self).__init__()
        self.queue = queue
    def run(self):
        print('Worker started')
        for data in iter(self.queue.get,None):
            # Hier kannst du festlegen, was er ausgeben soll,
            # sobald ein neues skript gestartet wird. Ich
            # lasse immer die benutzten Parameter anzeigen.
            # Print-Befehle Deines Skriptes werden trotzdem
            # angezeigt (aber bis zu 8 mal gleichzeitig.
            np.random.seed()
            print(data)
            time.sleep(3)
            # Den Stern brauchst du, wenn deine Funktion mehr
            # als einen Parameter enthaelt, andernfalls kannst Du
            # ihn auch weglassen.            
            ##########################
            ## Run the actual code. ##
            ##########################
            rbprotocol.run_kernel_simulation_version2(*data)


request_queue = Queue()
for i in range(kernels):
    Worker(request_queue).start()
# Hier legst du die Parameter fest. Wichtig ist,
# dass sie in derselben Reihenfolge stehen, wie
# in der Funktionsdefinition.
for data in product(dim_list, number_particles_list, sequences_list, runs_list, filenames_list_list, params_list):
    request_queue.put(data)
    time.sleep(3)
# Sentinel objects to allow clean shutdown: 1 per worker.
for i in range(kernels):
    request_queue.put(None)
    time.sleep(3)



with open(f'{dirname}/filenames.txt', 'a') as file:
    for name in filenames_list:
        file.write(name + '\n')
