import scipy
import structure_functions
# from sympy.combinatorics.partitions import Partition
import numpy as np
import scipy.special
import asrb
from copy import deepcopy
from itertools import count, permutations
from itertools import product
import datetime
import os
# from sympy.combinatorics.permutations import Permutation
import pdb

###############################################################################
###################### Functions needed for sequence ##########################
###############################################################################

def depolarizing_channel(rho, *args, **kwargs):
    """ Takes a density matrix np.ndarray:rho and depolarizes with
    depolarized_rho = (1-dep_lambda)rho + dep_lambda*np.eye(dim)/dim where dim is
    rho.shape[0].
    """
    # Extract the dimension of rho.
    dim = float(rho.shape[0])
    # Set the depolarizing constant depolarizing_lambda.
    depolarizing_lambda = kwargs.get('depolarizing_lambda', 0.01) 
    # Calculate the depolarized rho.
    depolarized_rho = (1-depolarizing_lambda)*rho + np.eye(int(dim))*depolarizing_lambda/dim
    return depolarized_rho

def draw_density_matrix(dim, number_particles, *args, **kwargs):
    """ Returns an np.array with shape (int:dim, int:dim)
    """
    # Calculate the number of Fock vectors. 
    number_vectors = int(scipy.special.binom(number_particles+dim-1, number_particles))
    # First check if a random density matrix is needed.
    if kwargs.get('random_density_matrix'):
        # Draw a random number from the unifrom distribution.
        number_fock_vector = np.random.randint(0, high=number_vectors)
    else:
        number_fock_vector = kwargs.get('number_fock_vector_density_matrix', 0)
    # Since the way of generating the fock vectors and generating this
    # basis vectors stays the same alle the time, they are interchangeable to some extend.
    state = np.zeros(number_vectors)
    state[number_fock_vector] = 1
    # Take the outer product to produce the pure state density matrix.
    density_matrix = np.outer(state, state)
    return density_matrix, state

def measurement(rho, *args, **kwargs):
    """ Takes a density matrix as an argument and returns a value.
    Input:
        np.ndarray:rho, of shape (dim, dim) with dim the dimension of rho,
            the density matrix which is measured.
    Optional Input:
        kwargs['projective_measurement'], then also needed kwargs['basis_vector].
    Output:
        float:outcome, the measurement outcome.
    """
    # Check what kind of measurement is set.
    if kwargs.get('projective_measurement'):
        # For the projective measurement, a basis vector has to be given
        # throught kwargs.
        if type(kwargs.get('basis_vector')) == np.ndarray:
            # Get the basis vector.
            project_on = kwargs['basis_vector']
            # Build the measurement matix.
            measurement_matrix = np.outer(project_on, project_on)
        else:
            print('Projective measurement is chosen but no basis vector as np.ndarray given!', 
                  'Abort by not setting measurement_matrix.')
        # Perform the measurement.
        outcome = np.trace(np.dot(measurement_matrix, rho))
        return outcome

def fock_basis_measurement(rho, *args, measure_now=True, return_vectorized=False, **kwargs):
    """ Uses a Fock basis to generate all pure state measurements.
    Then take the density matrix and calculates the trace from rho times all
    measurements. Returns an array for each pure state measurent.
    Input:
        np.ndarray:rho, of shape (dim, dim) with dim the dimension of rho,
            the density matrix which is measured.
    Output:
        np.ndarray, an array where all the projective measurement outcomes of
            the whole basis are stored.
    """
    # TODO many particle measurements?
    # Check if the number of vectors is given in kwargs.
    # If not, calculate the number of Fock vectors. 
    if kwargs.get('number_vectors') != None: 
        number_vectors = kwargs.get('number_vectors')
    else:
        number_vectors = int(scipy.special.binom(
            kwargs.get('number_particles')+kwargs.get('number_nodes')-1,
            kwargs.get('number_particles')))
    # Generate the basis vectors. Each of this vectors in np.ndarray:states corresponds
    # to a fock vector. Since the way of generating the fock vectors and generating this
    # basis vectors stays the same alle the time, they are interchangeable to some extend.
    states = np.diag(np.ones(number_vectors))
    if measure_now:
        # Calculate the outer product of pairs of the same vectors in np.ndarray:states.
        # Yielding the basis measurement.
        basis_measurements = np.einsum('ij,il->ijl', states, states)
        # Take the trace of each basis measurement element and the density matrix given.
        measurement_outcome = np.trace(np.dot(basis_measurements, rho), axis1=1, axis2=2)
        return measurement_outcome
    if return_vectorized:
        # Take the tensor product of every basis vector with itsself. For that np.einsum
        # is useful, but first the shape has to be altered for that.
        states_reshaped = states.reshape(number_vectors, 1, number_vectors)
        # And now the tensor product of each state vector with itsself is taken and reshape since
        # np.einsum is not putting it in the right shape.
        vecotrized_measurements = np.einsum(
            'ijk,ilm->ijklm', states_reshaped, states_reshaped).reshape(number_vectors,-1)
        return vecotrized_measurements

###############################################################################
################ Basis of Matrices of arbitrary dimension. ####################
###############################################################################

def gellmann(j, k, d):
    """Returns a generalized Gell-Mann matrix of dimension d. According to the
    convention in *Bloch Vectors for Qubits* by Bertlmann and Krammer (2008),
    returns :math:`\Lambda^j` for :math:`1\leq j=k\leq d-1`,
    :math:`\Lambda^{kj}_s` for :math:`1\leq k<j\leq d`,
    :math:`\Lambda^{jk}_a` for :math:`1\leq j<k\leq d`, and
    :math:`I` for :math:`j=k=d`.

    :param j: First index for generalized Gell-Mann matrix
    :type j:  positive integer
    :param k: Second index for generalized Gell-Mann matrix
    :type k:  positive integer
    :param d: Dimension of the generalized Gell-Mann matrix
    :type d:  positive integer
    :returns: A genereralized Gell-Mann matrix.
    :rtype:   numpy.array
    Taken from Jonathan Gross. Revision 449580a1
    """

    if j > k:
        gjkd = np.zeros((d, d), dtype=np.complex128)
        gjkd[j - 1][k - 1] = 1
        gjkd[k - 1][j - 1] = 1
    elif k > j:
        gjkd = np.zeros((d, d), dtype=np.complex128)
        gjkd[j - 1][k - 1] = -1.j
        gjkd[k - 1][j - 1] = 1.j
    elif j == k and j < d:
        gjkd = np.sqrt(2/(j*(j + 1)))*np.diag([1 + 0.j if n <= j
                                               else (-j + 0.j if n == (j + 1)
                                                     else 0 + 0.j)
                                               for n in range(1, d + 1)])
    else:
        gjkd = np.diag([1 + 0.j for n in range(1, d + 1)])

    return gjkd

def get_basis(d):
    '''Return a basis of orthogonal Hermitian operators on a Hilbert space of
    dimension d, with the identity element in the last place.
    Taken from Jonathan Gross. Revision 449580a1

    '''
    return [gellmann(j, k, d) for j, k in product(range(1, d + 1), repeat=2)]

###############################################################################
############################ Post-porocessing #################################
###############################################################################

# def flip_operator(amount_sides, dim, initial, flip_to, *args, **kwargs):
#     """ Given the 
#     Input:
#         int:initial,
#         int:flip_to
#     """
#     # Build a flattened matrix filled with zeros with dimension of a flip operator
#     # which flips two sides in a two sides system.
#     flip_basic = np.zeros(dim**4)
#     # The prescription for building a flip of two sides (each with dimension d) is as follows:
#     # row: d*m + n, column d*n + m for all pairs (m,n) with m,n = 0, ..., d-1.
#     # Meaning that first the m and n array have to be initialized.
#     m, n = np.arange(0, dim), np.arange(0, dim)
#     # Then all pairs are put together with meshgrid and some reshaping.
#     combinations_mn = np.array(np.meshgrid(m, n)).T.reshape(-1, 2).T
#     # Use the prescription from above to form the indices for the rows and for the columns.
#     indices_row = dim*combinations_mn[0] + combinations_mn[1]
#     indices_column = dim*combinations_mn[1] + combinations_mn[0]
#     # Get a an array with two array inside: First one is the row indices, second one the columns indices.
#     indices = np.array(np.append(indices_row, indices_column, axis=0)).reshape(2, dim**2)
#     # From the indices to tuples which can be used to index an array, the ravel_multi_index function can be used.
#     flip_indices = np.ravel_multi_index(indices, (dim**2, dim**2))
#     # Use the flip_indices to set entries 1, all other entries stay 0.
#     flip_basic[flip_indices] = 1
#     # Reshape the flattened matrix into a matrix.
#     flip_basic = flip_basic.reshape(dim**2, dim**2)
#     # Build the identity matrix for one side
#     identity = np.eye(dim)
#     # Check if the inital and flipped to sides are next to each other or not.
#     # If not, the flip operator has to be applied several times.
#     sides_to_hop = flip_to - initial
#     # Initiate the final matrix
#     final_matrix_right = np.eye(dim**amount_sides)
#     # If flipping more then one side, they have to be 'stacked', for example
#     # flip_13 = (1 tensor flip_12)(flip_23 tensor 1)(1 tensor flip_12), drawing the
#     # flipping makes it better to understand.
#     final_matrix_left = np.eye(dim**amount_sides)
#     # Iterate through the sides which have to be flipped.
#     # For example if the side 1 and 3 are flipped, firs the 1st and 2nd side and
#     # then the 2nd and 3rd side are flipped.
#     for count in range(0, sides_to_hop, np.sign(sides_to_hop)):
#         # If already flipped, go to the next side.
#         new_initial = initial + count
#         # Overwrite the old matrix_list (or initiate it in the first run).
#         matrix_list = []
#         # Iterate over the number of sides.
#         # For the new_inital side append the flip operator for two sides, else just the identity.
#         for kk in range(1, amount_sides):
#             if kk == new_initial:
#                 matrix_list.append(flip_basic)
#             else:
#                 matrix_list.append(identity)
#         # Start from the last matrix in the list and put them all via the tensorproduct together.
#         tensorproducted_matrix = matrix_list[-1]
#         for kk in range(2, amount_sides):
#             tensorproducted_matrix = np.kron(matrix_list[-kk], tensorproducted_matrix)
#         # Multiply the flip matrices with each other to build the final flip matrix.
#         final_matrix_right = np.dot(tensorproducted_matrix, final_matrix_right)
#         if abs(count) != sides_to_hop-1:
#             # Multiply the tensorproducted_matrix for the 'back stacking' from the right.
#             final_matrix_left = np.dot(final_matrix_left, tensorproducted_matrix)
#     # Take both the left and right final matrix and build the flip operator.
#     final_matrix = final_matrix_left.dot(final_matrix_right)
#     return final_matrix

# def sym_antisym_projector(amount_sides, indices_sides, dim_side, *args, anti=False, **kwargs):
#     """Builds a symmetry or antisymmetry projector according to the given side indices.
#     The dimension int:dim of the single sides is important for the flip operator.
#     A projector consists of an addition of all possible permutations which themself consists of
#     translations (or in this case flips) of two sides.
#     Input:
#         int:amount_sides,
#         np.array or list:indices_sides,
#         int:dim_side,
#         bool:anti, if True the parity of the permutation has to be used for the 
#             for factor = (-1)^{parity}.
#     Output:
#         np.ndarray, the projector with dimension (dim**amount_sides, dim**amount_sides)
#     """
#     # If not all numbers from 0 to max(indices_side) are inside the array, the transposition
#     # function is not working. Hence a workaround is used, where the transposition indices are
#     # indexing the given indices_sides array, and the transpositions are build with an array
#     # going from 0 to len(indices_sides).
#     if len(indices_sides) != max(indices_sides):
#         # For example indices_indices = [1, 3], then new_indices_sides = [1,2].
#         working_indices_sides = np.arange(1, len(indices_sides)+1, 1)
#     else:
#         working_indices_sides = deepcopy(indices_sides)
#     # Generate all possible permutations.
#     all_permutations = permutations(np.array(working_indices_sides))
#     all_permutations = list(all_permutations)
#     # Initiate the permutation matrices list which will be added together in the end.
#     list_perm_matrices = []
#     # Iterate through all permutations and build them via flip operators.
#     for perm in all_permutations:
#         # The sympy package counts from 0, not from 1
#         perm = np.array(perm)-1
#         # Make the permutation into an object the sympy package can deal with.
#         # formatted_perm = Permutation(perm)
#         # Get the flips (or transpositions) which the permutation consists of.
#         # This is now a list filled with tuples for example: [(1,2), (3,4)]
#         # Since the indexing here starts with zero, but in the flip function with 1, make plus one.
#         list_transpositions = formatted_perm.transpositions()
#         # Now loop over the transpositions to build the permutation by multiplying the
#         # flip operators for the transpositions together.
#         list_flips = []
#         # If the list of transpositions is empty, then it is like applying the
#         # identiy matrix.
#         if len(list_transpositions) == 0:
#             list_flips.append(np.eye(dim_side**amount_sides))
#         # If it is not empty, flips are doing the transpositions.
#         for transp in list_transpositions:
#             # Get the current flip. 
#             initial, flip_to = indices_sides[transp[0]], indices_sides[transp[1]]
#             current_flip = flip_operator(amount_sides, dim_side, initial, flip_to)
#             # Append the current flip.
#             list_flips.append(current_flip)
#         # In case there is only one matrix in the list list_flips, nothing has to be done.
#         if len(list_flips) == 1:
#             perm_matrix = list_flips[0]
#         else:
#             # Multiply all elements in the list to get the permutation.
#             perm_matrix = np.linalg.multi_dot(list_flips)
#         # Check for antisymmetrizer. If it is true the parity has to be taken into account.
#         if anti:
#             forfactor = (-1)**(len(list_transpositions))
#         else:
#             forfactor = 1
#         # Add to the list.
#         list_perm_matrices.append(forfactor * perm_matrix)
#     # Take all elements of list_perm_matrices and add them together.
#     projector = np.sum(list_perm_matrices, axis=0)
#     # Finally, normalize the projector by dividing it throug the amount of permutations.
#     projector = projector/float(len(all_permutations))
#     return projector

# def young_n1_m3(list_projectors_irreps, *args, **kwargs):
#     """One particle with three nodes. There is only one irrep
#     There are only two Sigmas which are used for the postprocessing.
#     This one, sigma_2, and the trivial (identity) sigma_1.
#     """
#     # 1. projector, flip with two systems and from 1 to 2.
#     pi_sym_12 = sym_antisym_projector(3, [1,2], 3, *args, **kwargs)
#     # 2. projector, with flip with three systems and from 1 to 3.
#     pi_ant_13 = sym_antisym_projector(3, [1,3], 3, *args, anti=True, **kwargs)
#     # Build the final projector for the not trivial irrep.
#     sigma_2 = np.dot(pi_sym_12, pi_ant_13)
#     # The other is the identity.
#     # sigma_1 = np.eye(3**3)
#     # list_projectors_irreps.append(sigma_1)
#     list_projectors_irreps.append(sigma_2)
#     return list_projectors_irreps

# def young_n1_m3_sum_numbering(list_projectors_irreps, *args, **kwargs):
#     """One particle with three nodes. There is only one irrep
#     There are only two Sigmas which are used for the postprocessing.
#     This one, sigma_2, and the trivial (identity) sigma_1.
#     """
#     # 1.1 projector, flip with two systems and from 1 to 2.
#     pi_sym_12 = sym_antisym_projector(3, [1,2], 3, *args, **kwargs)
#     # 2.1 projector, with flip with three systems and from 1 to 3.
#     pi_ant_13 = sym_antisym_projector(3, [1,3], 3, *args, anti=True, **kwargs)
#     # Build the final projector for the not trivial irrep.
#     sigma_1 = np.dot(pi_sym_12, pi_ant_13)
#     # 1.2 projector
#     pi_sym_13 = sym_antisym_projector(3, [1,3], 3, *args, **kwargs)
#     # 2.2 projector
#     pi_ant_12 = sym_antisym_projector(3, [1,2], 3, *args, anti=True, **kwargs)
#     #
#     sigma_2 = np.dot(pi_sym_13, pi_ant_12)
#     # The other is the identity.
#     # sigma_1 = np.eye(3**3)
#     # list_projectors_irreps.append(sigma_1)
#     list_projectors_irreps.append((sigma_2+sigma_1)/2.)
#     return list_projectors_irreps

# def young_n2_m3(list_projector_irreps, *args, **kwargs):
#     # Start with the one which is already defined via young_n1_m3.
#     list_projector_irreps = young_n1_m3(list_projector_irreps)
#     # Add the remaining. The trivial one is already included.
#     pi_sym_1234 = sym_antisym_projector(6, [1,2,3,4], 6, *args, **kwargs)
#     pi_sym_56 = sym_antisym_projector(6, [5,6], 6, *args, **kwargs)
#     pi_anti_15 = sym_antisym_projector(6, [1,5], 6, *args, anti=True, **kwargs)
#     pi_anti_26 = sym_antisym_projector(6, [2,6], 6, anti=True)
#     # Multiply all the projectors together.
#     sigma_3 = np.dot(pi_sym_1234, np.dot(pi_sym_56, np.dot(pi_anti_15, pi_anti_26)))
#     # Add the last sigma to the list of irrep projectors.
#     list_projector_irreps.append(sigma_3)
#     return list_projector_irreps

# def character_function_young(dim, amount_particles, *args, **kwargs):
#     """ This function calculates, based on the given parameters, the operator consisting
#     of symmetrizers and antisymmetrizers. It returns a list of 
#     these symmetrizers and antisymmetrizers following Young Diagramms.
#     Input:
#         int:dim,
#         int:amount_particles,
#     Output:
#         , a list filled with lists with matrices (anti-)symmetrizes
#     """
#     # # Get the single symmetrizers and antisymmetrizers.
#     # sigma = [P_sym(4,5), P_sym(1,2,3), ...]
#     # Store the projectors onto the different irreps in this list.
#     list_projectors_irreps = []
#     # Build the projectors.
#     # Use symmetrizers and antisymmetrizers.
#     # The calculation for what symmetrizers and antisymmetrizers to take
#     # has to happen analytically.
#     if amount_particles == 1 and dim == 3 and kwargs.get('numbering') == None:
#         # Get the related projector according to the Young diagram.
#         list_projectors_irreps = young_n1_m3(list_projectors_irreps)
#         # Store the number of boxes in the young diagram.
#         # n+n*(m-1).
#         number_boxes = 3
#     elif amount_particles == 2 and dim == 3:
#         # Two particles in three nodes.There are several irreps.
#         list_projectors_irreps = young_n2_m3(list_projectors_irreps)
#         # Store the number of boxes in the young diagram.
#         # n+n*(m-1).
#         number_boxes = 6
#     elif kwargs.get('numbering'):
#         list_projectors_irreps = young_n1_m3_sum_numbering(list_projectors_irreps)
#         number_boxes = 3
#     else:
#         pass
#     # For normailzation reasons, perform an SVD and make the not zero singular values to one.
#     # Go through the list of projectors (which are not neccesarily rank 1!).
#     for count in range(len(list_projectors_irreps)):
#         projector = list_projectors_irreps[count]
#         # Perform the SVD. The second term is the singular values vector, not a matrix.
#         # U*singvaluesmatrix*Vdag = projector
#         U, singularvalues, Vdag = np.linalg.svd(projector)
#         # They all should be the same. So check that all nonzero values are all the same.
#         # First get a bool array [False, False, True, ...] indicating where the elements
#         # if the array are zero.
#         bool_zeros = np.isclose(singularvalues, np.zeros(len(singularvalues)), atol=1e-15)
#         # Invert the bool array with np.invert or just a tiled ~ (which is the same).
#         nonzero_entries = singularvalues[~bool_zeros]
#         # Get the nonzero entry.
#         nonzero = nonzero_entries[0]
#         if not np.allclose(nonzero_entries, np.ones(len(nonzero_entries))*nonzero, rtol=1e-05):
#             print('The young diagramm projector is not a projector. Here the nonzero singularvalues:')
#             print(nonzero_entries)
#         # Even if it is not, proceed with normalizing the projector.
#         list_projectors_irreps[count] = projector/nonzero
#     return list_projectors_irreps, number_boxes

def character_function_schur(unitary, number_boxes, *args, **kwargs):
    """ This function calculates the schur polynomial value of a unitary
    and returns the fraction of the polynomial, see wikipedia 
    """
    # Extract the amount of nodes from the unitary.
    d = unitary.shape[0]
    # The eigenvalues are needed, since unitary can have an eigenvalue Zerlegung.
    eigvales, _ = np.linalg.eig(unitary)
    # Stack them for performing the power of the partitions (amount of boxes) and dimension paramter.
    # The transpose is because the power is acting horizontal, making it differnet than in the 
    # wikipedia article described. Since the determinant is invariant under transposition, this is okay.
    eigvales_matrix = np.tile(eigvales, d).reshape(d,d).T
    # List for storing character values for the case of more than one irrep.
    char_list = []
    # For every Young diagramm:
    # Calculate the nominator of the Jacobi's bialternant formula (wikipedia).
    # The numerator is the determinant of the matrix where the eigenvalues are raised to a power which is dependent
    # on the amount of nodes d and the partition entries (the amount of boxes from the Young diagram).
    for count in range(number_boxes, 0, -1):
        # Just in case the calculation is alternating the original matrix.
        numerator_matrix = deepcopy(eigvales_matrix)
        # Set the partition, lambda = (lambda_{d-1}, lambda_{d-2}, ..., 0), where lambda_{d-1} is the amount of boxes of
        # the young diagram which is in our case always 2*number_boxes for the first young diagram,
        # 2*(number_boxes-1) for the second, and so on (that is why a for loop is needed).
        partitions = np.append(np.append(2*count, np.repeat(count, d-2)), 0)
        # There are additional parameters depending on the number of nodes (dimension).
        numerator_powers = np.arange(0, d)[::-1] + partitions
        # Take the eigvalues in every array to the different powers.
        numerator_matrix = np.power(numerator_matrix, numerator_powers)
        # Calculate the determinant.
        numerator = np.linalg.det(numerator_matrix)
        # The denominator is the determinant of the vandermond matrix of the dimension dependent power.
        denominator = np.linalg.det(np.vander(eigvales))
        # The character is now the fraction.
        character = numerator/denominator
        char_list.append(character)
    return char_list

def fit_exponential(gdata, *args, **kwargs):
    pass

def irreps_projectors(dim, *args, interacting=False, **kwargs):
    """ The projector onto the existing irreps of the representation can
    only be calculated analytically for interacting particles (one particle can also be seen
    as interacting). 
    If so, the equation:
        P_triv = 1/d \sum_{i,j =1}^d \ket {i,i}\bra{j,j},
        where d the fockspace dimension. Then the important projector is this:
        P_rest = Ident - P_triv.
    If more than two particles are non interacting, the projector has to be calculated numerically,
    and stored in a .txt file. All of the existing paths will be stored here depending on th
    dimension and number of particles.
    Input:
        int:dim, the fock_space dimension.
        int:number_nodes, how many actualy nodes there are.
        int:number_particles, well the number of particles.
            Actually only important for the noninteracting case.
        bool:interacting, if that is true the analytic calculation can be used.
            Or if the int:number_particles is one.
    """
    # Extract the number of particles and number of nodes.
    number_particles = kwargs.get('number_particles')
    number_nodes = kwargs.get('number_nodes')
    if interacting or number_particles == 1:
        # First get the vectors \bra{j,j} for j=1, ..., d. Since the measurement function needs
        # a matrix to extract the right shape, just give a zero matrix with the right dimensions.
        vectors_ii = fock_basis_measurement(np.zeros((dim, dim)), measure_now=False, return_vectorized=True, **kwargs)
        # For using the einsum function, reshape them.
        vectors = vectors_ii.reshape((vectors_ii.shape[0],1,vectors_ii.shape[1]))
        # Calculate all needed matrices and take the sum over all of the basis vectors.
        projector_trivial = np.sum(
            np.einsum('ijk,ljm->jilmk', vectors, vectors).reshape(dim**2, dim**2, dim**2), axis=0)/number_nodes
        # Take identity - P_triv to obtain the wanted projector.
        projector = np.eye(dim**2) - projector_trivial
    else:
        # Here all the numerically calculated projectors are stored (or more like their paths).
        # Don't forget to add them! :D 
        pass
    return projector

###############################################################################
################################ Sequence #####################################
###############################################################################

def survival_probability(amount, rho, measure, *args, **kwargs):
    """ Calculate the survival probability with rho as density matrix to begin with,
    the information about the unitaries to be used stored in kwargs with the key
    'unitary_func' and specificed more with other keys and the final measurement
    as func:measure.
    The equation (k=amount): Tr(Lambda(U_^k.... Lambda(U_1\rho U_1*)...U_k*)A), where  Lambda
    is the depolarizing channel.
    Input:
        int:amount,
        np.ndarray:rho,
        func:measure
    Output:
        float: , numerical survival probability.
    """
    # To not alter the original rho, copy it.
    matrix_rho = deepcopy(rho)
    dim = matrix_rho.shape[0]
    # Since the unitaries need actually the number of nodes, if this variable is given take that.
    # If it is not given, take int:dim.
    number_nodes = kwargs.get('number_nodes', dim)
    # Set the function with which the unitary is generated.
    unitary_func = kwargs.get('unitary_func', asrb.many_circuits)
    ## To not run into a memory overflow, check how many times we can draw as many
    # unitaries as possible in one run.
    ## Check int:amount and the dimension. There is a maximal amount of entries a
    # computer can handle,  in this case its approximately 360.000
    max_amount = int(3600000.0/dim**2)
    # How many runs are needed:
    runs = int(float(amount)/max_amount)
    # Store the product of the unitaries.
    product_unitaries = np.eye(dim, dim)
    # Go through the runs.
    for run in range(runs):
        # Draw int:max_amount many unitaries.
        unitaries = unitary_func(max_amount, number_nodes, *args, **kwargs)
        # Loop over range of int:amount. Each time take a unitary and depolarize the
        # density mattrix on which the conjugated action of the unitary was performed.
        for count in range(max_amount):
            # Normalize the unitaries to be SU(2). TODO put the normalization into the asrb.py function
            # with vectorized approach for faster performance.
            # TODO dim or number_nodes?
            unitaries[count] = unitaries[count]/np.linalg.det(unitaries[count])**(1./dim)
            # Take the conjugated action.
            matrix_rho = np.dot(unitaries[count].conj(), np.dot(matrix_rho, unitaries[count]))
            # Depolarize the matrix_rho.
            matrix_rho = depolarizing_channel(matrix_rho, *args, **kwargs)
        # Multiply all unitaries together. This product of unitaries is needed for the character
        # function in the postprocessing schema.
        temporary_product = np.linalg.multi_dot(unitaries)
        product_unitaries = np.dot(temporary_product, product_unitaries)
    # Calculate the remaining unitaries to be drawn:
    remaining = int((float(amount)/max_amount - runs) * max_amount)
    # Draw int:remaining many unitaries.
    unitaries = unitary_func(remaining, number_nodes, *args, **kwargs)
    # Loop over range of int:remaining. Each time take a unitary and depolarize the
    # density matrix on which the conjugated action of the unitary was performed.
    for count in range(remaining):
        # Normalize the unitaries to be SU(2).
        unitaries[count] = unitaries[count]/np.linalg.det(unitaries[count])**(1./dim)
        # Take the conjugated action.
        matrix_rho = np.dot(unitaries[count].T.conj(), np.dot(matrix_rho, unitaries[count]))
        # Depolarize the matrix_rho.
        matrix_rho = depolarizing_channel(matrix_rho, *args, **kwargs)
    # Multiply, again, all unitaries together.
    if len(unitaries) != 1:
        temporary_product = np.linalg.multi_dot(unitaries)
    else:
        temporary_product = unitaries[0]
    product_unitaries = np.dot(temporary_product, product_unitaries)
    # Finally, measure the state with the given measurement func:measure. The variable
    # outcome can be either a float or a np.ndarray, depends on the measurement performed.
    outcome = measure(matrix_rho, *args, **kwargs)
    return outcome, product_unitaries


# def random_analog_bosonic_benchmarking(dim, number_particles, sequences, runs, *args, **kwargs):
#     """ The algorithm works as follows:
#     Require: Sequence lenght int:K, Hamiltonian measure v, repetition number M,
#     particle number int:n.

#     1. Fix psi, which is a Fock state with n particles.
#     2. Fix a measurement {A, 1-A}.
#     (define a list for saving the Qs, list_Qs)
#     3. for k in range(K):
#     4.   for l in range(M):
#             (define a list for saving the drawn unitaries, list_unitaries)
#     5.       for mu in range(k):
#     6.          draw U_l^mu according to v.
#     7.          list_unitaries.append(U_l^mu)
#     8.       q_l^k = Tr(Lambda(U_^k....Lambda(U_1\rho U_1*)...U_k*)A)
#     9.    Q^k = sum(q_l^k)/M
#     10.    list_Qs.append(Q^k)
#     11. list_Qs store K many survival probabilites. 

#     Input:
#         int:dim,
#         int:number_particles,
#         list:sequences, a list filled with integers defining the sequence lengths for the simulation.
#     """
#     # Draw a density matrix, inside kwargs the specifications should be stored.
#     rho, fock_vector = draw_density_matrix(dim, number_particles, *args, **kwargs)
#     # If projective measurement is chosen, put the returned fock_vector into kwargs.
#     if kwargs.get('projective_measurement'):
#         kwargs['basis_vector'] = fock_vector
#     # Define the measurement with which the twirled state is measured, here it is a
#     # function.
#     measurement_A = measurement
#     # Put the number of particles inside the kwargs dictionary. This way the unitary function can use it
#     # without altering all the frame potential code and input of the function (added the number of particles
#     # too late haha)
#     kwargs['number_particles'] = number_particles
#     # The number of irreps depends on the number of particles and if it is the interacting or noninteracting case.
#     number_irreps = number_particles + 0
#     # Initiate an array to store the post processed Q values. Since a 2d array is needed
#     # a list is not suffienct.
#     postprocess_array = np.zeros((number_irreps, len(sequences)), dtype=np.complex64)
#     # Store the qs for every sequence and every run.
#     q_array = np.zeros((len(sequences), runs), dtype=np.complex64)
#     char_array = np.zeros((len(sequences), runs, number_irreps), dtype=np.complex64)
#     # Start the first loop where over the different sequences in list:sequences
#     # the survival probability estimates are calculated.
#     for count in range(len(sequences)):
#         # How many unitaries are needed for this run.
#         sequencelen = sequences[count]
#         # For each run with this sequence length, store the product of all unitaries
#         # for the postprocessing step of taking the character function.
#         list_unitary_products = [] 
#         # Initiate a list for storing the int:runs many survival probabilities.
#         list_qs = []
#         # Since for every int:sequencelen the survival probability has to be
#         # calculated many times, loop over int:runs.
#         for run in range(runs):
#             # Calculate the survival probability. Also, the product of unitaries is returned.
#             q, product_unitaries = survival_probability(int(sequencelen), rho, measurement_A, *args, **kwargs)
#             # Store the product of unitaires (without the depolarizing channel in between) and the q.
#             list_unitary_products.append(product_unitaries)
#             list_qs.append(q)
#             q_array[count, run] = q
#             # Calculate the character values of the (possibly different) irreps.
#             char_list = character_function_schur(product_unitaries, number_particles, *args, **kwargs)
#             # Store the value of the character for further investigation puproses.
#             char_array[count,run] = np.array(char_list)
#             weighted_surv_prob = np.array(char_list)*q
#             # Average the value wich was just calculate int:runs times.
#             postprocess_array[:,count] = postprocess_array[:, count] + weighted_surv_prob
#     # Done! The postprcess_array contains the data which can be plotted.
#     return postprocess_array/float(runs), char_array, q_array, list_unitary_products


def random_analog_bosonic_benchmarking_version2(number_nodes, number_particles, sequences, runs, *args, 
        interacting=False, **kwargs):
    """ The algorithm works as follows:
    Require: Sequence lenght int:K, Hamiltonian measure v, repetition number runs,
    particle number int:n.

    1. Fix psi, which is a Fock state with n particles.
    2. Fix a measurement basis {M1, M2, ..., MD} with D the Fock space dimension.
    3. for k in range(K):
    4.   for l in range(M):
            (define a list for saving the drawn unitaries, list_unitaries)
    5.       for mu in range(k):
    6.          draw U_l^mu according to v.
    7.          list_unitaries.append(U_l^mu)
    8.       q_l^k,i = Tr(Lambda(U_^k....Lambda(U_1\rho U_1*)...U_k*)Mi)
    9.       Filterfunction bla too lazy to write it down
    10.    Q^k,i = sum(q_l^k,i)/runs
    11.    list_Qs.append(Q^k)
    12. list_Qs store K many survival probabilites. 

    Input:
        int:number_nodes, how many lattice points there are
        int:number_particles,
        list:sequences, a list filled with integers defining the sequence lengths for the simulation.
        int:runs, the iterations the average is sampled out
        bool:interacting, if the system is interacting, the filter function and the Hamiltonians looks different.
        Possibly in kwargs:
            basis_measurement: Either True or False.

    """
    # Draw a density matrix, inside kwargs the specifications should be stored.
    rho, fock_vector_0 = draw_density_matrix(number_nodes, number_particles, *args, **kwargs)
    # Calculate the dimension of the fock space. Needed for the basis measurement and
    # vectorization of rho and measurements.
    dim_fockspace = int(scipy.special.binom(number_particles+number_nodes-1, number_particles))
    # Check what measurement is chosen. Right now only the full basis measurement is implemented.
    # This measurement function is used to measure the twirled and developed state at the end of
    # the protocol.
    if kwargs.get('basis_measurement'):
        measurement_A = fock_basis_measurement
    else:
        print('Error, no measurement in kwargs given, only basis_measurement as True allowed!')
    # Put the number of particles inside the kwargs dictionary. This way the unitary function can use it
    # without altering all the frame potential code and input of the function (added the number of particles
    # too late haha)
    kwargs['number_particles'] = number_particles
    # And the number of nodes (or modes), too.
    kwargs['number_nodes'] = number_nodes
    #
    jonas_basis = fock_basis_measurement(rho, measure_now=False, return_vectorized=True, **kwargs)
    # The number of irreps depends on the number of particles
    # and if it is the interacting or noninteracting case.
    # TODO for interacting case this is 2 (or 1)? Check that.
    if interacting:
        number_irreps = 1
    else:
        number_irreps = number_particles + 0
    # Initiate two arrays to store the post processed Q values. Since 2d arrays are needed
    # a list is not suffienct.
    postprocess_unnormalized = np.zeros((number_irreps, len(sequences)), dtype=np.complex64)
    postprocess_normalization = np.zeros((number_irreps, len(sequences)), dtype=np.complex64)
    # Store the qs for every sequence, every run, and every measurement.
    q_array = np.zeros((len(sequences), runs, dim_fockspace), dtype=np.complex64)
    # Same for the filter function parameters, but now the number of irreps goes in, too.
    filter_array = np.zeros((len(sequences), runs, dim_fockspace, number_irreps), dtype=np.complex64)
    # Start the first loop where over the different sequences in list:sequences
    # the survival probability estimates are calculated.
    for count in range(len(sequences)):
        # How many unitaries are needed for this run.
        sequencelen = sequences[count]
        # For each run with this sequence length, store the product of all unitaries
        # for the postprocessing step of taking the character function.
        list_unitary_products = [] 
        # Since for every int:sequencelen the survival probability has to be
        # calculated many times, loop over int:runs.
        for run in range(runs):
            # Calculate the survival probability. Also, the product of unitaries is returned.
            q, product_unitaries = survival_probability(
                int(sequencelen), rho, measurement_A, *args, number_vectors=dim_fockspace, **kwargs)
            # Store the product of unitaires (without the depolarizing channel in between) and the q.
            list_unitary_products.append(product_unitaries)
            q_array[count, run] = q
            # Vectorize the initital state density matrix rho. 
            rhoprime_vec = np.kron(
                product_unitaries, product_unitaries.conj()).dot(
                    np.kron(fock_vector_0.reshape((-1, 1)), fock_vector_0.reshape((-1, 1))))
            # Obtain the vectorized measurements. The variable rho is actually not important,
            # the architecture of the functions require giving an argument here.
            measurements_vec = measurement_A(
                rho, measure_now=False, return_vectorized=True, number_vectors=dim_fockspace)
            projectors = irreps_projectors(dim_fockspace, interacting=interacting, **kwargs)
            # Calculate the filter function. Here for every irrep and basis measurement a value comes out.
            filters = measurements_vec.dot(projectors.dot(rhoprime_vec))
            # Take the filterfunction times the survival probability for each different basis measurement.
            # For that the q array has to be repeated like as much irreps there are.
            repeated_qs = np.repeat(q, filters.shape[1])
            # For normalization puprose, do the same with the trace of
            # measurement matrices with the productUs^dag*rho*productUs.
            rho_prime = product_unitaries.T.conj().dot(rho.dot(product_unitaries))
            repeated_outcomes_noerror = np.repeat(
                measurement_A(rho_prime, *args, **kwargs), filters.shape[1])
            # Now every block of filtervalues is taken times the survival probability value.
            # The resulting array must be a 3-tensor, else the next step does not work.
            # If there is only one irrep, it would be a 2-tensor, so the -1 makes sure that, even it is only
            # one value per array, the shape is right.
            new_shape = (filters.shape[0], -1, filters.shape[-1])
            filtered_qs = (filters.flatten()*repeated_qs).reshape(new_shape)
            # Same with the normalization term.
            filtered_normalization = (filters.flatten()*repeated_outcomes_noerror).reshape(new_shape)
            # Now sum over the different measurement blocks. This is the postprocessed value of survival
            # probabilities, but not yet normalized.
            postprocessed = np.sum(filtered_qs, axis=0)
            # Again same with normlization.
            normalization = np.sum(filtered_normalization, axis=0)
            # And store them all.
            filter_array[count,run] = np.array(filters)
            # Average the values (unnormalized value and normalization factor),
            # wich was just calculate int:runs times.
            postprocess_unnormalized[:,count] = postprocess_unnormalized[:, count] + postprocessed
            postprocess_normalization[:,count] = postprocess_normalization[:, count] + normalization
        # Sometime the simulation breaks down in the middle, that is why it is good to store the
        # values immediatly rather then collecting them and then storing the whole array at once.
        structure_functions.arrays_to_txtfile(
            kwargs.get('filename'),
            np.real(postprocess_unnormalized[:,count]/postprocess_normalization[:,count]))
        print(f'sequencelength: {sequencelen}, ',
              f'survival prob: {postprocess_unnormalized[:,count]/postprocess_normalization[:,count]}')
    # Divide the two averaged values.
    postprocess = postprocess_unnormalized/postprocess_normalization
    # Done! The postprcess_array contains the data which can be plotted.
    return postprocess, filter_array, q_array, list_unitary_products

def character_average(dim, number_particles, sequences, *args, store_projector=False, **kwargs):
    """ This function is for checking if the character function converges to the right
    value if average over enough unitaries.
    The input is a little missleading since this function was developed after the rb_protocol
    implementation.
    list:sequences is now referencing the amount of unitaries used for the average.
    Everything else stays with the same meaning.
    Input:
        int:dim,
        int:number_particles,
        list:sequences, a list filled with integers defining the
            sequence lengths for the simulation.
    """
    # Extract the unitary function and the character function.
    unitary_func = kwargs.get('unitary_func', asrb.many_circuits)
    character_func = kwargs.get('character_func', character_function_schur)
    # The number of irreps depends on the number of particles and if it is the interacting or 
    # noninteracting case. TODO implement the interacting case!
    kwargs['number_particles'] = number_particles + 0
    number_boxes = number_particles + 0
    # Calculate the dimension of the fock space. This is needed to initialize the projector well.
    fock_dim = int(scipy.special.binom(number_particles + dim - 1, number_particles))
    # Check if the spectral gap should be calculated and stored.
    spectralgap = kwargs.get('spectralgap', False)
    if spectralgap:
        # Here the character projector is stored. The character value times the unitary
        # averaged over multiple unitaries should yield the projector onto the irrep
        # corrseponding to the calculated character.
        projector = np.zeros((number_boxes, fock_dim**2, fock_dim**2), dtype=np.complex64)
        # Get the filename where the singular values will be stored.
        filenamesv = kwargs.get('filenamesv')
    # Here the sum of the character values will be stored.
    char_array = np.zeros((number_boxes, len(sequences)), dtype=np.complex64)
    # Since we need the dimensions of the irreps, store them in an array.
    # We need the l_array going from number of particles to one.
    l_array = np.arange(1, number_particles+1)[::-1]
    # Calculate the binomial coefficient dim+l-2 over l
    dimension_irreps = (dim+2*l_array-1)/(dim-1)*scipy.special.binom(dim+l_array-2, l_array)**2
    # Start the first loop where over the different sequences in list:sequences
    # the survival probability estimates are calculated.
    for sequence_count in range(len(sequences)):
        # How many unitaries are needed for this run.
        sequencelen = sequences[sequence_count]
        # Since for every int:sequencelen the character average has to be calculated with this amount of
        # unitaries, first draw this many unitaries and hope the memory is not crashing haha.
        unitaries = unitary_func(sequencelen, dim, **kwargs)
        # Sum over the character function of int:sequencelen. 
        for unitary_count in range(sequencelen):
            # Normalize the unitary by hand!
            unitaries[unitary_count] = unitaries[unitary_count] \
                                            /np.linalg.det(unitaries[unitary_count])**(1./dim)
            # With the unitary matrix calculate the character function.
            characters = character_func(unitaries[unitary_count], number_particles, **kwargs)
            # And store the sum of the characters for each sequence. 
            char_array[:,sequence_count] += characters
            # If the chosen unitary is the Haar random one, and if the particles are non interacting and more than two,
            # the unitary has to first be brought into the symmetrical subspace. 
            U_tilde = asrb.change_to_particlebasis(
                np.kron(unitaries[unitary_count], unitaries[unitary_count]), dim, **kwargs)
            if spectralgap:
                # Take each character times the corresponding projecter.
                projector = projector + np.einsum('k,ij->kij',
                        np.conj(characters), np.kron(U_tilde, U_tilde.conj()))
            tocheck1 = np.sum(characters)
            tocheck2 = np.trace(np.kron(U_tilde, U_tilde.conj()))
        # For an average divide through the number of unitaries used to
        # calculate the total character value.
        char_array[:,sequence_count] = char_array[:,sequence_count]/float(sequencelen)
        # Same for the projector.
        if spectralgap:
            # Take the dimension times the projector. Since there could be several projectors
            # onto several irrpes, there are different dimensions for different irreps (and their projector).
            for count_boxes in range(projector.shape[0]):
                projector[count_boxes] = dimension_irreps[count_boxes]*projector[count_boxes]/float(sequencelen)
            # Carry out the singular value decomposition.
            _, sing_values, _ = np.linalg.svd(projector)
            # Carry out the eigenvalue decomposition
            eigvalues, _ = np.linalg.eig(projector)
            structure_functions.arrays_to_txtfile(filenamesv, sing_values)
            structure_functions.arrays_to_txtfile(filenamesv, eigvalues)
            # Check if the projector should be stored.
            if store_projector:
                if sequencelen == np.amax(sequences):
                    for count_irreps in range(len(dimension_irreps)):
                        structure_functions.arrays_to_txtfile(
                                f'{filenamesv[:-4]}_{sequencelen}_runs.txt', projector[count_irreps])  
            # Reset the projector, not storing every single one helps having enough memory.
            projector = np.zeros((number_boxes, fock_dim**2, fock_dim**2), dtype=np.complex64)
    # Done! The char_array contains the data which can be plotted.
    return char_array

def character_average_riskyversion(dim, number_particles, sequences, *args, store_projector=False, **kwargs):
    """ This function is for checking if the character function converges to the right
    value if average over enough unitaries.
    The input is a little missleading since this function was developed after the rb_protocol
    implementation.
    list:sequences is now referencing the amount of unitaries used for the average.
    Everything else stays with the same meaning.
    Input:
        int:dim,
        int:number_particles,
        list:sequences, a list filled with integers defining the
            sequence lengths for the simulation.
    """
    # Extract the unitary function and the character function.
    unitary_func = kwargs.get('unitary_func', asrb.many_circuits)
    character_func = kwargs.get('character_func', character_function_schur)
    # The number of irreps depends on the number of particles and if it is the interacting or 
    # noninteracting case. TODO implement the interacting case!
    kwargs['number_particles'] = number_particles + 0
    number_boxes = number_particles + 0
    # Calculate the dimension of the fock space. This is needed to initialize the projector well.
    fock_dim = int(scipy.special.binom(number_particles + dim - 1, number_particles))
    # Check if the spectral gap should be calculated and stored.
    spectralgap = kwargs.get('spectralgap', False)
    if spectralgap:
        # Here the character projector is stored. The character value times the unitary
        # averaged over multiple unitaries should yield the projector onto the irrep
        # corrseponding to the calculated character.
        projector = np.zeros((number_boxes, fock_dim**2, fock_dim**2), dtype=np.complex64)
        # Get the filename where the singular values will be stored.
        filenamesv = kwargs.get('filenamesv')
    # Here the sum of the character values will be stored.
    char_array = np.zeros((number_boxes, len(sequences)), dtype=np.complex64)
    # Since we need the dimensions of the irreps, store them in an array.
    # We need the l_array going from number of particles to one.
    l_array = np.arange(1, number_particles+1)[::-1]
    # Calculate the binomial coefficient dim+l-2 over l
    dimension_irreps = (dim+2*l_array-1)/(dim-1)*scipy.special.binom(dim+l_array-2, l_array)**2
    # Start the first loop where over the different sequences in list:sequences
    # the survival probability estimates are calculated.
    for sequence_count in range(len(sequences)):
        # How many unitaries are needed for this run.
        sequencelen = sequences[sequence_count]
        # Since for every int:sequencelen the character average has to be calculated with this amount of
        # unitaries, first draw this many unitaries and hope the memory is not crashing haha.
        unitaries = unitary_func(sequencelen, dim, **kwargs)
        # Sum over the character function of int:sequencelen. 
        for unitary_count in range(sequencelen):
            # Normalize the unitary by hand!
            unitaries[unitary_count] = unitaries[unitary_count] \
                                            /np.linalg.det(unitaries[unitary_count])**(1./dim)
            # With the unitary matrix calculate the character function.
            characters = character_func(unitaries[unitary_count], number_particles, **kwargs)
            # And store the sum of the characters for each sequence. 
            char_array[:,sequence_count] += characters
            # If the chosen unitary is the Haar random one, and if the particles are non interacting and more than two,
            # the unitary has to first be brought into the symmetrical subspace. For that first extracte the
            # Hamiltonian from the unitary, change the basis into the Fock basis of the Hamiltonian and make the
            # unitary out of the changes Hamiltonian again.
            # Since U = exp(-itH), assume t=1, then H = -i*log(U).
            eigvalues, U_eig = np.linalg.eig(unitaries[unitary_count])
            # Take the logarithm of the singular values and put everything back together.
            H = -1j*U_eig.dot(np.diag(np.log(eigvalues)).dot(U_eig.conj().T))
            # Perform the change of basis. For that the Hamiltonian has to be flatten and indices are added, which
            # are actually just all numbers from 0 to dim*dim-1.
            # Since the function returns 3-tensor, make the first (unnecessary) layer vanish.
            H_tilde = asrb.change_to_fockbasis(H.flatten(), np.arange(0, dim*dim), 1, dim, **kwargs)[0]
            # Build the new unitary out of the changes Hamiltonian, for that a singular value decomposition is needed.
            eigvalues, U = np.linalg.eig(H_tilde)
            # Exponentiate the singular values and put everything back together.
            U_tilde = U.dot(np.diag(np.exp(eigvalues*1j)).dot(U.conj().T))
            if spectralgap:
                # Take each character times the corresponding projecter.
                projector = projector + np.einsum('k,ij->kij',
                        np.conj(characters), np.kron(U_tilde, U_tilde.conj()))
            to_check = np.trace(np.kron(unitaries[unitary_count], unitaries[unitary_count].conj()))-1
        # For an average divide through the number of unitaries used to
        # calculate the total character value.
        char_array[:,sequence_count] = char_array[:,sequence_count]/float(sequencelen)
        # Same for the projector.
        if spectralgap:
            # Take the dimension times the projector. Since there could be several projectors
            # onto several irrpes, there are different dimensions for different irreps (and their projector).
            for count_boxes in range(projector.shape[0]):
                projector[count_boxes] = dimension_irreps[count_boxes]*projector[count_boxes]/float(sequencelen)
            # Carry out the singular value decomposition.
            _, sing_values, _ = np.linalg.svd(projector)
            # Carry out the eigenvalue decomposition
            eigvalues, _ = np.linalg.eig(projector)
            structure_functions.arrays_to_txtfile(filenamesv, sing_values)
            structure_functions.arrays_to_txtfile(filenamesv, eigvalues)
            # Check if the projector should be stored.
            if store_projector:
                if sequencelen == np.amax(sequences):
                    for count_irreps in range(len(dimension_irreps)):
                        structure_functions.arrays_to_txtfile(
                                f'{filenamesv[:-4]}_{sequencelen}_runs.txt', projector[count_irreps])  
            # Reset the projector, not storing every single one helps having enough memory.
            projector = np.zeros((number_boxes, fock_dim**2, fock_dim**2), dtype=np.complex64)
    # Done! The char_array contains the data which can be plotted.
    return char_array

# def build_projector_haar_complicated(runs, *args, **kwargs):
#     """ Build a projector onto the not trivial irrep for 3 nodes and 1 particle.
#     Input:
#         int:runs, over how many haar matrices the projector is averaged.
#     """
#     # Take the easiest case for a lattice with 3 modes and one particle in it.
#     dim = 3
#     number_particles = 1
#     # Extract the needed function for the character calculation.
#     characterfunction = kwargs.get('characterfunction')
#     # Young diagramms where implemented first. For that the projector and number of
#     # boxes is needed.
#     if characterfunction == 'character_function_young':
#         # Get the young matrix.
#         young_matrices_irreps, number_boxes = character_function_young(dim,
#             number_particles, *args, numbering=None, **kwargs)
#         sigma = young_matrices_irreps[0]
#         # Averaged projector onto irrep initialization.
#     irrep_projector = np.zeros((dim**2, dim**2))
#     for run in range(runs):
#         # Draw 1 haar unitary.
#         unitary = asrb.one_haar_unitary(dim)
#         # Fpr Young diagramm option, the unitary has to be taken to a tensor product.
#         if characterfunction == character_function_young:
#             # Calculate the tensor (kron) product of product_unitaires int:number_boxes times, since
#             # this matrix is projected.
#             tensored_product_unitaries = deepcopy(unitary)
#             for _ in range(number_boxes-1):
#                 tensored_product_unitaries = np.kron(tensored_product_unitaries, unitary)
#             # Calculate the filter paramteter. 
#             character_value = np.trace(np.dot(sigma, tensored_product_unitaries))
#         # For the schur polynomial option, one value ist calculated.
#         if characterfunction == 'character_function_schur':
#             character_value = character_function_schur(unitary, *args, **kwargs)
#         # Calculate the projector, average over different haar random matrices.
#         # filter_function_matrix = character_value * liouville_representation(dim, adjoint_action, {'acting_matrix':unitary})
#         filter_function_matrix = np.conj(character_value) * unitary # np.kron(unitary.conj(), unitary)
#         # Average them.
#         irrep_projector = irrep_projector + filter_function_matrix
#     return irrep_projector/runs

def build_projector_haar(runs, dim, particle_number, *args, **kwargs):
    """ Build a projector onto the not trivial irrep for 3 nodes and 1 particle.
    Input:
        int:runs, over how many haar matrices the projector is averaged.
    """
    # Initiate the matrix.
    irrep_projector = np.zeros((dim**2, dim**2), dtype=np.complex64)
    # Iterate over int:run and draw a new haar unitary each run.
    for run in range(runs):
        # Draw the unitaryies with the given unitary function.
        unitary_func = kwargs.get('unitary_func')
        unitary = unitary_func(1, dim, *args, **kwargs)[0]
        # 
        unitary = unitary/np.linalg.det(unitary)**(1./dim)
        # For the schur polynomial option, one value ist calculated.
        character_value = character_function_schur(unitary, *args, **kwargs)
        # Calculate the projector, average over different haar random matrices.
        # filter_function_matrix = character_value * liouville_representation(dim, adjoint_action, {'acting_matrix':unitary})
        filter_function_matrix = np.kron(unitary.conj(), unitary) * character_value
        # Average them.
        irrep_projector = irrep_projector + filter_function_matrix
    return irrep_projector*8/float(runs)


def adjoint_action(matrix_to_act_on, *args, **kwargs):
    """ Takes a matrix where the adjoint action M *..* M^\dag is acted on with a given acting_matrix.
    """
    # Extract the matrix which is acting adjoint.
    acting_matrix = kwargs.get('acting_matrix')
    # Calculate the adjoint action.
    return acting_matrix.dot(matrix_to_act_on.dot(acting_matrix.T.conj()))

def liouville_representation(dim, action, dict_for_action, *args, **kwargs):
    """ This function take the action of a map (for example the adjoint action) and
    rewrites is into one matrix.
    Input:
        int:dim, the dimension of the input matrices. The choice of basis depends on that.
        func:action, or channel, takes as an argument a basis element and returns a matrix.
    """
    # Get the basis matrices via the gell-mann function. This is a list
    basis = get_basis(dim)
    # Initialize the final matrix representation which has dimension d**2 x d**2.
    final_representation = np.zeros((dim**2, dim**2), dtype=np.complex64)
    # Go through the matrix elements i,j of the final representation and calculate the element.
    for i, j in product(range(0, dim**2), repeat=2):
        # i and j are the indices for the final_representation matrix, calculate
        # final_representation_{i,j} = Tr(basis_i.dot(action(basis_j))).
        final_representation[i, j] = np.trace(basis[i].dot(action(basis[j], **dict_for_action)))
    # Done! 
    return final_representation

###############################################################################
############################### Simulation ####################################
###############################################################################

# def run_kernel_simulation(dim, number_particles, sequences, runs, filenames_list, params):
#     """This function calls the random_analog_bosonic_benchmarking function. Since in the worker where several kernels can
#     be used a **kwargs argumant cannot be used, this is the workaround.
#      Input:
#         str:unitary_func,
#         list/np.ndarray:dim_list,
#         list/np.ndarray:N_list
#         list/np.ndarray:design_list,
#         list/np.ndarray:depth_list,
#         dict:params_dictionary
#     """
#     # Check which file is alreday written.
#     found_file, count = False, 0
#     while not found_file:
#         filename = filenames_list[count]
#         if not os.path.isfile(filename):
#             found_file = True
#         count += 1

    
#     # There are three simulation possibilities; simulate the whole protocol or calculate the average of the
#     # character function over int:sequences unitaries. The parameter to check that is stored in dict:params.
#     # It is either 'rb_protocol' or 'character_average'.
#     which_simulation = params.get('which_simulation', 'rb_protocol')
#     if which_simulation == 'rb_protocol':
#         postprocess_array, char_array, q_array, list_unitary_products = \
#             random_analog_bosonic_benchmarking(dim, number_particles, sequences, runs, **params)
#     elif which_simulation == 'character_average':
#         filenamesv = filename[:-4] + '_projector.txt'
#         postprocess_array = character_average(dim, number_particles, sequences, filenamesv=filenamesv, **params)
#     else:
#         print('Parameter which_simulation was given a wrong assignement! Either rb_protocol or character_average.')

#     with open(filename, 'x') as file:
#         file.write(f'# number_particles={postprocess_array.shape[0]}\n')
#         file.write(f'# length={postprocess_array.shape[1]}\n')
#         file.write(f'# keep in mind that the imaginary part is discarded.\n')
#     postprocess_array = np.real(postprocess_array)
#     structure_functions.arrays_to_txtfile(filename, sequences)
#     structure_functions.arrays_to_txtfile(filename, *postprocess_array)
#     structure_functions.write_labbook('numericalData_rbprotocol/labBook.txt',done=str(datetime.datetime.now()))


def run_kernel_simulation_version2(dim, number_particles, sequences, runs, filenames_list, params):
    """This function calls the random_analog_bosonic_benchmarking function. Since in the worker where several kernels can
    be used a **kwargs argumant cannot be used, this is the workaround.
     Input:
        str:unitary_func,
        list/np.ndarray:dim_list,
        list/np.ndarray:N_list
        list/np.ndarray:design_list,
        list/np.ndarray:depth_list,
        dict:params_dictionary
    Return: None
    """
    # Check which file is alreday written.
    found_file, count = False, 0
    while not found_file:
        filename = filenames_list[count]
        if not os.path.isfile(filename):
            found_file = True
        count += 1
    # Write it so the other kernels 'know' that this file is already taken.
    with open(filename, 'x') as file:
        file.write(f'# number_particles={number_particles}\n')
        file.write(f'# length={sequences.shape[0]}\n')
        file.write(f'# keep in mind that the imaginary part is discarded.\n')
    structure_functions.arrays_to_txtfile(filename, sequences)
    # To make the kernels different random, take the count variable and random seed
    for _ in range(count):
        np.random.seed()
    # There are two simulation possibilities; simulate the whole protocol 
    # character function over int:sequences unitaries. The parameter to check 
    # that is stored in dict:params. It is either 'rb_protocol' or 'character_average'.
    which_simulation = params.get('which_simulation', 'rb_protocol')
    if which_simulation == 'rb_protocol':
        _, _, _, _ = random_analog_bosonic_benchmarking_version2(
            dim, number_particles, sequences, runs, filename=filename, **params)
    elif which_simulation == 'character_average':
        filenamesv = filename[:-4] + '_projector.txt'
        _ = character_average(
            dim, number_particles, sequences, filenamesv=filenamesv, **params)
    else:
        print('Parameter which_simulation wrong! Either rb_protocol or character_average.')

    # Store in the labbook when the simulation was completet with a timestamp and the filename.
    structure_functions.write_labbook(
        'numericalData_rbprotocol/labBook.txt',done=f'filename, {datetime.datetime.now()}')



def merge_postprocessed_data(filenames, dirname, *args, **kwargs):
    """ For making parallel work easier, the different sequences can be split up
    and run simultaneously. For plotting and better storing, merge them into one .txt file.
    """
    # Initiate the lists with the sequence entries and the data.
    # The sequences don't have to be in the right order, but the order of the date
    # must stay the same as of the sequences. 
    sequences_list, data_list = [], []
    for filename in filenames:
        with open(filename, 'r') as file:
            amount_decays = int(file.readline()[-2])
            len_sequences = int(file.readline()[-2])
            all_data = np.loadtxt(filename, skiprows=3)
            sequences = all_data[:len_sequences]
            data = all_data[len_sequences:].reshape(amount_decays, len_sequences)
        sequences_list.append(sequences)
        data_list.append(data)
    # Put together the arrays in data_list, not interrupting the order and the shape.
    data_array = np.stack(np.array(data_list), axis=1)
    sequences_array = np.array(sequences_list).flatten()
    # Store the merged data.
    with open(dirname+'/merged_postprocessed_data.txt', 'x') as newfile:
        newfile.write(f'# number_particles={data_array.shape[0]}\n')
        newfile.write(f'# length={data_array.shape[1]}\n')
        newfile.write(f'# keep in mind that the imaginary part is discarded.\n')
    structure_functions.arrays_to_txtfile(dirname+'/merged_postprocessed_data.txt', sequences_array)
    structure_functions.arrays_to_txtfile(dirname+'/merged_postprocessed_data.txt', *data_array)
