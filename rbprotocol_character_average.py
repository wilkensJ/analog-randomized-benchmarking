import numpy as np
import time

from multiprocessing import Process, Queue
from itertools import product

import rbprotocol
import structure_functions
import asrb
import pdb
import datetime

# Initiate the dictionary where parameters for the simulation can be set.
params = {}
# dimensions, number of nodes
dim_list = [3]
# Important for character function
number_particles_list = [2]
# Not necesarry but the function wants it.
runs_list = [1]
# Choose sequence lenghts. Here the parallel work can be used.
exponents = np.arange(1,10)
exponents = np.array([10])
# final_sequences = np.array([4, 16, 64, 256, 1024, 4096, 16384])
final_sequences = 4**exponents
break_up_sequences = np.tile(final_sequences, 1).reshape(1, len(final_sequences))
sequences_list = list(break_up_sequences)
## Which character function should be used?
# 'character_function_young'
# 'character_function_schur'
params['characterfunction'] = 'character_function_schur'
#############################################################################
############################## unitaries ##################################
#############################################################################
## unitary_func
# many_circuits
# many_haar_unitaries
# one_haar_unitary
# params['unitary_func'] =  asrb.many_circuits
params['unitary_func'] =  asrb.many_haar_unitaries
# Set the depth of the circuits.
params['depth'] = 1
## circuit_func
# unitaries_from_hamiltonians
# --params["circuit_func"] = asrb.unitaries_from_hamiltonians
#############################################################################
######################## If band diagonal is chosen ###########################
#############################################################################
## hamiltonian_func
# draw_1d_noninteracting
# draw_2d_noninteracting
# draw_1d_interacting
# draw_2d_interacting
# --params["hamiltonian_func"] = asrb.draw_1d_noninteracting
# And with that the varable interacting is defined, too.
params["interacting"] = False
## distributions
# normal_distribution
# uniform_distribution
# repeated_distribution, set distribution=normal_disbtribution
# only_number, set fixed_value=1.
# --params["diag_func_hop"] = asrb.only_number
# --params["band_func_hop"] = asrb.normal_distribution
# --params["diag_func_int"] = asrb.only_number
## For drawing one entry and copying it to the whole diagonal/band for one matrix.
# random_entry_uniform
# random_entry_normal
# fixed_value
# --params["random_entry_normal"] = True
# params["low"] = -2
# params["high"] = 2
# This defines the mean for the normal distribution.
# --params["loc"] =  0.
# This defines the deviation of the normal distribution.
# --params["scale"] = 1.
#############################################################################
#############################################################################
#############################################################################
# Set the k-design value for calculating the frame potentials.
# --params["time"] = 1.
# Define how much smaller or bigger the band diagonal entries are than the
# diagonal entries.
# --params["bandweight"] = 1.
params['which_simulation'] = 'character_average'
params['spectralgap'] = True
params['store_projector'] = True
# Make the dictionary into a list to make it iterable.
params_list = [params]
# Extract the amount of kernenls by getting the sequence_list dimension.
kernels = len(sequences_list)
# Build the list of filenames where the data will be stored.
dirname = structure_functions.create_dir(pathname='numericalData_rbprotocol')
# Build all the filenames for the postprocessed arrays to store.
filenames_list = []
for count in range(kernels):
    filenames_list.append(f'{dirname}/postprocessed_data{count}.txt')
filenames_list_list = [filenames_list]
# Write all the stuff in the labbook.
structure_functions.write_to_txt('numericalData_rbprotocol/labBook.txt', {'\n' : '\n'})
structure_functions.write_to_txt('numericalData_rbprotocol/labBook.txt', start=datetime.datetime.now())
structure_functions.write_to_txt('numericalData_rbprotocol/labBook.txt', dirname=dirname,
    dim=dim_list[0], number_particles = number_particles_list[0], runs=runs_list[0], **params)

class Worker(Process):
    def __init__(self, queue):
        super(Worker, self).__init__()
        self.queue = queue
    def run(self):
        print('Worker started')
        for data in iter(self.queue.get,None):
            # Hier kannst du festlegen, was er ausgeben soll,
            # sobald ein neues skript gestartet wird. Ich
            # lasse immer die benutzten Parameter anzeigen.
            # Print-Befehle Deines Skriptes werden trotzdem
            # angezeigt (aber bis zu 8 mal gleichzeitig.
            np.random.seed()
            print(data)
            # Den Stern brauchst du, wenn deine Funktion mehr
            # als einen Parameter enthaelt, andernfalls kannst Du
            # ihn auch weglassen.
            time.sleep(3)
            ##########################
            ## Run the actual code. ##
            ##########################
            rbprotocol.run_kernel_simulation(*data)


request_queue = Queue()
for i in range(kernels):
    Worker(request_queue).start()
# Hier legst du die Parameter fest. Wichtig ist,
# dass sie in derselben Reihenfolge stehen, wie
# in der Funktionsdefinition.
for data in product(dim_list, number_particles_list, sequences_list, runs_list, filenames_list_list, params_list):
    request_queue.put(data)
# Sentinel objects to allow clean shutdown: 1 per worker.
for i in range(kernels):
    request_queue.put(None)



with open(f'{dirname}/filenames.txt', 'a') as file:
    for name in filenames_list:
        file.write(name + '\n')
